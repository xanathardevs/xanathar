#!/usr/bin/env bash

if [ $(whoami) != 'root' ]; then
    echo "You must be root!";
    exit;
fi;

x=$1;

if [ -z $1 ]; then
    echo "Using default install dir. If you want to use a custom directory, run ./install.sh <dir>";
    x="/usr/lib/xanathar";
fi;

echo "Install dir: "${x};

echo -n "Is this correct? (type 'y' or 'n', and then [ENTER]) ";

read correct;

if [ ${correct} == 'n' ]; then
    exit 1;
fi;

echo "Making directory "${x};
mkdir -p ${x};
echo "Copying files to "${x};
cp -R xanathar/*.py ${x};
cp xanathar.sh ${x};
mv ${x}/xanathar.sh ${x}/xanathar;
chmod +x ${x}/xanathar;
mkdir ${x}/stdlib;
cp xanathar/stdlib/*.so ${x}/stdlib;
cp xanathar/stdlib/module_registry ${x}/stdlib;
mkdir ${x}/grammar;
cp xanathar/grammar/* ${x}/grammar;
echo "Installed!";
echo "Please add "${x}' to your $PATH';

