#!/usr/bin/env bash
find -name "*.py" -or -name "*.xan" -or -name "*.xl" -or -name "*.c" -or -name "*.cpp" | sed -e "/venv/d" | xargs wc -l
