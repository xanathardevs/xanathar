; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca i8
  store i8 2, i8* %".6"
  %".8" = load i8, i8* %".6"
  %".9" = zext i8 %".8" to i1024
  %".10" = call i1024 @"a"(i1024 %".9")
  %".11" = trunc i1024 %".10" to i8
  store i8 %".11", i8* %".6"
  %".13" = load i8, i8* %".6"
  %".14" = icmp eq i8 %".13", 2
  br i1 %".14", label %"entry.if", label %"entry.else"
entry.if:
  %".16" = alloca [8 x i8]
  store [8 x i8] [i8 36, i8 120, i8 32, i8 105, i8 115, i8 32, i8 50, i8 0], [8 x i8]* %".16"
  %".18" = getelementptr [8 x i8], [8 x i8]* %".16", i32 0
  %".19" = call i32 (i8*, ...) @"printf"(i8* %".4", [8 x i8]* %".18")
  br label %"entry.endif"
entry.else:
  %".21" = alloca [12 x i8]
  store [12 x i8] [i8 36, i8 120, i8 32, i8 105, i8 115, i8 32, i8 110, i8 111, i8 116, i8 32, i8 50, i8 0], [12 x i8]* %".21"
  %".23" = getelementptr [12 x i8], [12 x i8]* %".21", i32 0
  %".24" = call i32 (i8*, ...) @"printf"(i8* %".4", [12 x i8]* %".23")
  %".25" = alloca [9 x i8]
  store [9 x i8] [i8 36, i8 120, i8 32, i8 105, i8 115, i8 32, i8 37, i8 105, i8 0], [9 x i8]* %".25"
  %".27" = getelementptr [9 x i8], [9 x i8]* %".25", i32 0
  %".28" = getelementptr [9 x i8], [9 x i8]* %".27", i8 0
  %".29" = bitcast [9 x i8]* %".28" to i8*
  %".30" = load i8, i8* %".6"
  %".31" = call i32 (i8*, ...) @"printf"(i8* %".29", i8 %".30")
  %".32" = alloca [1 x i8]
  store [1 x i8] [i8 0], [1 x i8]* %".32"
  %".34" = getelementptr [1 x i8], [1 x i8]* %".32", i32 0
  %".35" = call i32 (i8*, ...) @"printf"(i8* %".4", [1 x i8]* %".34")
  br label %"entry.endif"
entry.endif:
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
define i1024 @"a"(i1024 %".1") 
{
entry:
  %".3" = alloca i1024
  store i1024 %".1", i1024* %".3"
  %".5" = load i1024, i1024* %".3"
  %".6" = zext i8 2 to i1024
  %".7" = add i1024 %".5", %".6"
  ret i1024 %".7"
}
