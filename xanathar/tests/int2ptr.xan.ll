; ModuleID = "tests/int2ptr.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %"called_malloc" = call i8* @"malloc"(i32 40)
  %".6" = alloca i8*
  store i8* %"called_malloc", i8** %".6"
  %"geped_from_y" = getelementptr i8*, i8** %".6", i32 0
  %".8" = bitcast i8** %"geped_from_y" to i32*
  store i32 0, i32* %".8"
  %"geped_from_y.1" = getelementptr i8*, i8** %".6", i32 1
  %".10" = bitcast i8** %"geped_from_y.1" to i32*
  store i32 1, i32* %".10"
  %"geped_from_y.2" = getelementptr i8*, i8** %".6", i32 2
  %".12" = bitcast i8** %"geped_from_y.2" to i32*
  store i32 2, i32* %".12"
  %"geped_from_y.3" = getelementptr i8*, i8** %".6", i32 3
  %".14" = bitcast i8** %"geped_from_y.3" to i32*
  store i32 3, i32* %".14"
  %"geped_from_y.4" = getelementptr i8*, i8** %".6", i32 4
  %".16" = bitcast i8** %"geped_from_y.4" to i32*
  store i32 4, i32* %".16"
  %"geped_from_y.5" = getelementptr i8*, i8** %".6", i32 5
  %".18" = bitcast i8** %"geped_from_y.5" to i32*
  store i32 5, i32* %".18"
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
declare external void @"nop"() 

declare external i32* @"range"(i32 %".1", i32 %".2") 

declare external i8* @"malloc"(i32 %".1") 

declare external void @"free"(...) 

declare external i32 @"cmp"(i32 %".1", i32 %".2") 

declare external void @"inc"(i32* %".1") 

declare external void @"dec"(i32* %".1") 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }