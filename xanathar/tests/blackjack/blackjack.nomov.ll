; ModuleID = "tests/blackjack/blackjack.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"Stringifyable_vtable" = type {[3 x i8] ()*}
%"Stringifyable" = type {}
%"Card" = type {i2, i32}
%"Card_vtable" = type {[3 x i8] (%"Card"*)*}
define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = call i32 @"time"(i32 0)
  call void @"srand"(i32 %".8")
  %".10" = alloca i32
  %".11" = alloca i32
  %".12" = alloca i64
  %".13" = zext i32 0 to i64
  store i64 %".13", i64* %".12"
  %".15" = alloca i64
  %".16" = zext i32 0 to i64
  store i64 %".16", i64* %".15"
  %".18" = mul i32 52, 8
  %".19" = call i8* @"malloc"(i32 %".18")
  %".20" = bitcast i8* %".19" to %"Card"*
  %".21" = alloca %"Card"*
  store %"Card"* %".20", %"Card"** %".21"
  store i32 0, i32* %".10"
  br label %"forloop-0"
forloop-0:
  store i32 1, i32* %".11"
  br label %"forloop-1"
endfor-0:
  %".50" = mul i32 21, 8
  %".51" = call i8* @"malloc"(i32 %".50")
  %".52" = bitcast i8* %".51" to %"Card"*
  %".53" = alloca %"Card"*
  store %"Card"* %".52", %"Card"** %".53"
  %".55" = alloca i32
  store i32 0, i32* %".55"
  %".57" = mul i32 21, 8
  %".58" = call i8* @"malloc"(i32 %".57")
  %".59" = bitcast i8* %".58" to %"Card"*
  %".60" = alloca %"Card"*
  store %"Card"* %".59", %"Card"** %".60"
  %".62" = alloca i32
  store i32 0, i32* %".62"
  %".64" = load %"Card"*, %"Card"** %".21"
  call void (...) @"shuffle"(%"Card"* %".64", i32 52, i32 8)
  %".66" = load %"Card"*, %"Card"** %".21"
  %".67" = load i64, i64* %".15"
  %".68" = getelementptr %"Card", %"Card"* %".66", i64 %".67"
  %".69" = getelementptr %"Card"*, %"Card"** %".53", i32 0
  store %"Card"* %".68", %"Card"** %".69"
  %".71" = load i64, i64* %".15"
  %".72" = zext i32 1 to i64
  %".73" = add i64 %".71", %".72"
  store i64 %".73", i64* %".15"
  %".75" = load %"Card"*, %"Card"** %".21"
  %".76" = load i64, i64* %".15"
  %".77" = getelementptr %"Card", %"Card"* %".75", i64 %".76"
  %".78" = getelementptr %"Card"*, %"Card"** %".53", i32 1
  store %"Card"* %".77", %"Card"** %".78"
  %".80" = load i64, i64* %".15"
  %".81" = zext i32 1 to i64
  %".82" = add i64 %".80", %".81"
  store i64 %".82", i64* %".15"
  %".84" = load %"Card"*, %"Card"** %".21"
  %".85" = load i64, i64* %".15"
  %".86" = getelementptr %"Card", %"Card"* %".84", i64 %".85"
  %".87" = getelementptr %"Card"*, %"Card"** %".60", i32 0
  store %"Card"* %".86", %"Card"** %".87"
  %".89" = load i64, i64* %".15"
  %".90" = zext i32 1 to i64
  %".91" = add i64 %".89", %".90"
  store i64 %".91", i64* %".15"
  %".93" = load %"Card"*, %"Card"** %".21"
  %".94" = load i64, i64* %".15"
  %".95" = getelementptr %"Card", %"Card"* %".93", i64 %".94"
  %".96" = getelementptr %"Card"*, %"Card"** %".60", i32 1
  store %"Card"* %".95", %"Card"** %".96"
  %".98" = load i64, i64* %".15"
  %".99" = zext i32 1 to i64
  %".100" = add i64 %".98", %".99"
  store i64 %".100", i64* %".15"
  store i32 2, i32* %".55"
  store i32 2, i32* %".62"
  %".104" = load %"Card"*, %"Card"** %".53"
  %".105" = load i32, i32* %".55"
  %".106" = call i32 @"get_value"(%"Card"* %".104", i32 %".105")
  %".107" = alloca i32
  store i32 %".106", i32* %".107"
  %".109" = load %"Card"*, %"Card"** %".60"
  %".110" = load i32, i32* %".62"
  %".111" = call i32 @"get_value"(%"Card"* %".109", i32 %".110")
  %".112" = alloca i32
  store i32 %".111", i32* %".112"
  br label %"entry.whileloop0"
forloop-1:
  %".27" = load i32, i32* %".10"
  %".28" = trunc i32 %".27" to i2
  %".29" = load i32, i32* %".11"
  %".30" = load %"Card"*, %"Card"** %".21"
  %".31" = load i64, i64* %".12"
  %".32" = getelementptr %"Card", %"Card"* %".30", i64 %".31"
  call void @"Card__init_hook"(%"Card"* %".32", i2 %".28", i32 %".29")
  %".34" = load i64, i64* %".12"
  %".35" = zext i32 1 to i64
  %".36" = add i64 %".34", %".35"
  store i64 %".36", i64* %".12"
  %".38" = load i32, i32* %".11"
  %".39" = add i32 %".38", 1
  store i32 %".39", i32* %".11"
  %".41" = load i32, i32* %".11"
  %".42" = icmp sle i32 %".41", 13
  br i1 %".42", label %"forloop-1", label %"endfor-1"
endfor-1:
  %".44" = load i32, i32* %".10"
  %".45" = add i32 %".44", 1
  store i32 %".45", i32* %".10"
  %".47" = load i32, i32* %".10"
  %".48" = icmp slt i32 %".47", 4
  br i1 %".48", label %"forloop-0", label %"endfor-0"
entry.whileloop0:
  %".115" = load i32, i32* %".112"
  %".116" = icmp sge i32 %".115", 17
  br i1 %".116", label %"entry.whileloop0.if", label %"entry.whileloop0.else"
entry.endwhile0:
  %".151" = alloca i32
  store i32 1, i32* %".151"
  %".153" = alloca i32
  store i32 1, i32* %".153"
  %".155" = load i32, i32* %".107"
  %".156" = icmp sgt i32 %".155", 21
  br i1 %".156", label %"entry.endwhile0.if", label %"entry.endwhile0.else"
entry.whileloop0.if:
  br label %"entry.endwhile0"
entry.whileloop0.else:
  br label %"entry.whileloop0.endif"
entry.whileloop0.endif:
  %".120" = alloca [9 x i8]
  store [9 x i8] [i8 37, i8 120, i8 32, i8 64, i8 32, i8 37, i8 112, i8 10, i8 0], [9 x i8]* %".120"
  %".122" = getelementptr [9 x i8], [9 x i8]* %".120", i32 0
  %".123" = getelementptr [9 x i8], [9 x i8]* %".122", i32 0
  %".124" = bitcast [9 x i8]* %".123" to i8*
  %".125" = load i32, i32* %".55"
  %".126" = call i32 (i8*, ...) @"printf"(i8* %".124", i32 %".125", i32* %".55")
  %".127" = alloca [9 x i8]
  store [9 x i8] [i8 37, i8 120, i8 32, i8 64, i8 32, i8 37, i8 112, i8 10, i8 0], [9 x i8]* %".127"
  %".129" = getelementptr [9 x i8], [9 x i8]* %".127", i32 0
  %".130" = getelementptr [9 x i8], [9 x i8]* %".129", i32 0
  %".131" = bitcast [9 x i8]* %".130" to i8*
  %".132" = load i32, i32* %".55"
  %".133" = call i32 (i8*, ...) @"printf"(i8* %".131", i32 %".132", i32* %".55")
  %".134" = load i32, i32* %".62"
  %".135" = add i32 %".134", 1
  store i32 %".135", i32* %".62"
  %".137" = load i64, i64* %".15"
  %".138" = zext i32 1 to i64
  %".139" = add i64 %".137", %".138"
  store i64 %".139", i64* %".15"
  %".141" = load %"Card"*, %"Card"** %".60"
  %".142" = load i32, i32* %".62"
  %".143" = call i32 @"get_value"(%"Card"* %".141", i32 %".142")
  store i32 %".143", i32* %".112"
  %".145" = load %"Card"*, %"Card"** %".60"
  %".146" = load i32, i32* %".62"
  call void @"print_hand"(%"Card"* %".145", i32 %".146")
  %".148" = load i32, i32* %".112"
  %".149" = icmp slt i32 %".148", 17
  br i1 %".149", label %"entry.whileloop0", label %"entry.endwhile0"
entry.endwhile0.if:
  store i32 0, i32* %".151"
  br label %"entry.endwhile0.endif"
entry.endwhile0.else:
  br label %"entry.endwhile0.endif"
entry.endwhile0.endif:
  %".161" = load i32, i32* %".112"
  %".162" = icmp sgt i32 %".161", 21
  br i1 %".162", label %"entry.endwhile0.endif.if", label %"entry.endwhile0.endif.else"
entry.endwhile0.endif.if:
  store i32 0, i32* %".153"
  br label %"entry.endwhile0.endif.endif"
entry.endwhile0.endif.else:
  br label %"entry.endwhile0.endif.endif"
entry.endwhile0.endif.endif:
  %".167" = load i32, i32* %".107"
  %".168" = load i32, i32* %".151"
  %".169" = mul i32 %".167", %".168"
  %".170" = alloca i32
  store i32 %".169", i32* %".170"
  %".172" = load i32, i32* %".112"
  %".173" = load i32, i32* %".153"
  %".174" = mul i32 %".172", %".173"
  %".175" = alloca i32
  store i32 %".174", i32* %".175"
  %".177" = load i32, i32* %".175"
  %".178" = load i32, i32* %".170"
  %".179" = call i32 @"cmp"(i32 %".177", i32 %".178")
  switch i32 %".179", label %"default-match-0" [i32 -1, label %"switchstmt-0-0" i32 1, label %"switchstmt-0-1"]
default-match-0:
  br label %"end-match-0"
end-match-0:
  %".196" = load %"Card"*, %"Card"** %".21"
  call void (...) @"free"(%"Card"* %".196")
  ret void
switchstmt-0-0:
  %".182" = alloca [10 x i8]
  store [10 x i8] [i8 89, i8 111, i8 117, i8 32, i8 119, i8 105, i8 110, i8 33, i8 10, i8 0], [10 x i8]* %".182"
  %".184" = getelementptr [10 x i8], [10 x i8]* %".182", i32 0
  %".185" = getelementptr [10 x i8], [10 x i8]* %".184", i32 0
  %".186" = bitcast [10 x i8]* %".185" to i8*
  %".187" = call i32 (i8*, ...) @"printf"(i8* %".186")
  br label %"end-match-0"
switchstmt-0-1:
  %".189" = alloca [11 x i8]
  store [11 x i8] [i8 89, i8 111, i8 117, i8 32, i8 108, i8 111, i8 115, i8 101, i8 33, i8 10, i8 0], [11 x i8]* %".189"
  %".191" = getelementptr [11 x i8], [11 x i8]* %".189", i32 0
  %".192" = getelementptr [11 x i8], [11 x i8]* %".191", i32 0
  %".193" = bitcast [11 x i8]* %".192" to i8*
  %".194" = call i32 (i8*, ...) @"printf"(i8* %".193")
  br label %"end-match-0"
}

declare i32 @"printf"(i8* %".1", ...) 

declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

declare i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

declare void @"nop"() 

declare i32* @"range"(i32 %".1", i32 %".2") 

declare i8* @"malloc"(i32 %".1") 

declare void @"free"(...) 

declare i32 @"cmp"(i32 %".1", i32 %".2") 

declare void @"inc"(i32* %".1") 

declare void @"dec"(i32* %".1") 

declare void @"srand"(i32 %".1") 

declare i32 @"rand"() 

declare void @"shuffle"(...) 

declare i32 @"time"(i32 %".1") 

declare i8* @"ctime"(i32* %".1") 

define i8 @"suit_to_char"(i2 %".1") 
{
entry:
  %".3" = alloca i2
  store i2 %".1", i2* %".3"
  %".5" = load i2, i2* %".3"
  %".6" = zext i2 %".5" to i32
  %".7" = alloca i32
  store i32 %".6", i32* %".7"
  %".9" = load i32, i32* %".7"
  %".10" = icmp eq i32 %".9", 0
  br i1 %".10", label %"entry.if", label %"entry.else"
entry.if:
  %".12" = alloca [2 x i8]
  store [2 x i8] [i8 67, i8 0], [2 x i8]* %".12"
  %".14" = getelementptr [2 x i8], [2 x i8]* %".12", i32 0
  %".15" = getelementptr [2 x i8], [2 x i8]* %".14", i32 0
  %".16" = bitcast [2 x i8]* %".15" to i8*
  %".17" = load i8, i8* %".16"
  ret i8 %".17"
entry.else:
  %".19" = load i32, i32* %".7"
  %".20" = icmp eq i32 %".19", 1
  br i1 %".20", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %".59" = alloca [2 x i8]
  store [2 x i8] [i8 69, i8 0], [2 x i8]* %".59"
  %".61" = getelementptr [2 x i8], [2 x i8]* %".59", i32 0
  %".62" = getelementptr [2 x i8], [2 x i8]* %".61", i32 0
  %".63" = bitcast [2 x i8]* %".62" to i8*
  %".64" = load i8, i8* %".63"
  ret i8 %".64"
entry.else.if:
  %".22" = alloca [2 x i8]
  store [2 x i8] [i8 83, i8 0], [2 x i8]* %".22"
  %".24" = getelementptr [2 x i8], [2 x i8]* %".22", i32 0
  %".25" = getelementptr [2 x i8], [2 x i8]* %".24", i32 0
  %".26" = bitcast [2 x i8]* %".25" to i8*
  %".27" = load i8, i8* %".26"
  ret i8 %".27"
entry.else.else:
  %".29" = load i32, i32* %".7"
  %".30" = icmp eq i32 %".29", 2
  br i1 %".30", label %"entry.else.else.if", label %"entry.else.else.else"
entry.else.endif:
  br label %"entry.endif"
entry.else.else.if:
  %".32" = alloca [2 x i8]
  store [2 x i8] [i8 68, i8 0], [2 x i8]* %".32"
  %".34" = getelementptr [2 x i8], [2 x i8]* %".32", i32 0
  %".35" = getelementptr [2 x i8], [2 x i8]* %".34", i32 0
  %".36" = bitcast [2 x i8]* %".35" to i8*
  %".37" = load i8, i8* %".36"
  ret i8 %".37"
entry.else.else.else:
  %".39" = load i32, i32* %".7"
  %".40" = icmp eq i32 %".39", 3
  br i1 %".40", label %"entry.else.else.else.if", label %"entry.else.else.else.else"
entry.else.else.endif:
  br label %"entry.else.endif"
entry.else.else.else.if:
  %".42" = alloca [2 x i8]
  store [2 x i8] [i8 72, i8 0], [2 x i8]* %".42"
  %".44" = getelementptr [2 x i8], [2 x i8]* %".42", i32 0
  %".45" = getelementptr [2 x i8], [2 x i8]* %".44", i32 0
  %".46" = bitcast [2 x i8]* %".45" to i8*
  %".47" = load i8, i8* %".46"
  ret i8 %".47"
entry.else.else.else.else:
  %".49" = alloca [2 x i8]
  store [2 x i8] [i8 69, i8 0], [2 x i8]* %".49"
  %".51" = getelementptr [2 x i8], [2 x i8]* %".49", i32 0
  %".52" = getelementptr [2 x i8], [2 x i8]* %".51", i32 0
  %".53" = bitcast [2 x i8]* %".52" to i8*
  %".54" = load i8, i8* %".53"
  ret i8 %".54"
entry.else.else.else.endif:
  unreachable
}

define i8 @"num_to_char"(i32 %".1") 
{
entry:
  %".3" = alloca i32
  store i32 %".1", i32* %".3"
  %".5" = load i32, i32* %".3"
  %".6" = icmp eq i32 %".5", 10
  br i1 %".6", label %"entry.if", label %"entry.else"
entry.if:
  %".8" = alloca [2 x i8]
  store [2 x i8] [i8 84, i8 0], [2 x i8]* %".8"
  %".10" = getelementptr [2 x i8], [2 x i8]* %".8", i32 0
  %".11" = getelementptr [2 x i8], [2 x i8]* %".10", i32 0
  %".12" = bitcast [2 x i8]* %".11" to i8*
  %".13" = load i8, i8* %".12"
  ret i8 %".13"
entry.else:
  %".15" = load i32, i32* %".3"
  %".16" = icmp eq i32 %".15", 11
  br i1 %".16", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %".60" = alloca [1 x i8]
  %".61" = bitcast [1 x i8]* %".60" to i8*
  %".62" = alloca [3 x i8]
  store [3 x i8] [i8 37, i8 105, i8 0], [3 x i8]* %".62"
  %".64" = getelementptr [3 x i8], [3 x i8]* %".62", i32 0
  %".65" = getelementptr [3 x i8], [3 x i8]* %".64", i32 0
  %".66" = bitcast [3 x i8]* %".65" to i8*
  %".67" = load i32, i32* %".3"
  %".68" = call i32 (i8*, i8*, ...) @"sprintf"(i8* %".61", i8* %".66", i32 %".67")
  %".69" = bitcast [1 x i8]* %".60" to i8*
  %".70" = load i8, i8* %".69"
  ret i8 %".70"
entry.else.if:
  %".18" = alloca [2 x i8]
  store [2 x i8] [i8 74, i8 0], [2 x i8]* %".18"
  %".20" = getelementptr [2 x i8], [2 x i8]* %".18", i32 0
  %".21" = getelementptr [2 x i8], [2 x i8]* %".20", i32 0
  %".22" = bitcast [2 x i8]* %".21" to i8*
  %".23" = load i8, i8* %".22"
  ret i8 %".23"
entry.else.else:
  %".25" = load i32, i32* %".3"
  %".26" = icmp eq i32 %".25", 12
  br i1 %".26", label %"entry.else.else.if", label %"entry.else.else.else"
entry.else.endif:
  br label %"entry.endif"
entry.else.else.if:
  %".28" = alloca [2 x i8]
  store [2 x i8] [i8 81, i8 0], [2 x i8]* %".28"
  %".30" = getelementptr [2 x i8], [2 x i8]* %".28", i32 0
  %".31" = getelementptr [2 x i8], [2 x i8]* %".30", i32 0
  %".32" = bitcast [2 x i8]* %".31" to i8*
  %".33" = load i8, i8* %".32"
  ret i8 %".33"
entry.else.else.else:
  %".35" = load i32, i32* %".3"
  %".36" = icmp eq i32 %".35", 13
  br i1 %".36", label %"entry.else.else.else.if", label %"entry.else.else.else.else"
entry.else.else.endif:
  br label %"entry.else.endif"
entry.else.else.else.if:
  %".38" = alloca [2 x i8]
  store [2 x i8] [i8 75, i8 0], [2 x i8]* %".38"
  %".40" = getelementptr [2 x i8], [2 x i8]* %".38", i32 0
  %".41" = getelementptr [2 x i8], [2 x i8]* %".40", i32 0
  %".42" = bitcast [2 x i8]* %".41" to i8*
  %".43" = load i8, i8* %".42"
  ret i8 %".43"
entry.else.else.else.else:
  %".45" = load i32, i32* %".3"
  %".46" = icmp eq i32 %".45", 1
  br i1 %".46", label %"entry.else.else.else.else.if", label %"entry.else.else.else.else.else"
entry.else.else.else.endif:
  br label %"entry.else.else.endif"
entry.else.else.else.else.if:
  %".48" = alloca [2 x i8]
  store [2 x i8] [i8 65, i8 0], [2 x i8]* %".48"
  %".50" = getelementptr [2 x i8], [2 x i8]* %".48", i32 0
  %".51" = getelementptr [2 x i8], [2 x i8]* %".50", i32 0
  %".52" = bitcast [2 x i8]* %".51" to i8*
  %".53" = load i8, i8* %".52"
  ret i8 %".53"
entry.else.else.else.else.else:
  br label %"entry.else.else.else.else.endif"
entry.else.else.else.else.endif:
  br label %"entry.else.else.else.endif"
}

define void @"Card__init_hook"(%"Card"* %".1", i2 %".2", i32 %".3") 
{
entry:
  %".5" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".5"
  %".7" = alloca i2
  store i2 %".2", i2* %".7"
  %".9" = alloca i32
  store i32 %".3", i32* %".9"
  %".11" = load i2, i2* %".7"
  %".12" = load %"Card"*, %"Card"** %".5"
  %".13" = getelementptr %"Card", %"Card"* %".12", i32 0, i32 0
  store i2 %".11", i2* %".13"
  %".15" = load i32, i32* %".9"
  %".16" = load %"Card"*, %"Card"** %".5"
  %".17" = getelementptr %"Card", %"Card"* %".16", i32 0, i32 1
  store i32 %".15", i32* %".17"
  ret void
}

define [3 x i8] @"Card_tostring"(%"Card"* %".1") 
{
entry:
  %".3" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".3"
  %".5" = alloca [3 x i8]
  %".6" = load [3 x i8], [3 x i8]* %".5"
  ret [3 x i8] %".6"
}

define void @"Card_print"(%"Card"* %".1") 
{
entry:
  %".3" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".3"
  %".5" = alloca i8
  %".6" = alloca i8
  %".7" = load %"Card"*, %"Card"** %".3"
  %".8" = getelementptr %"Card", %"Card"* %".7", i32 0, i32 1
  %".9" = load i32, i32* %".8"
  %".10" = call i8 @"num_to_char"(i32 %".9")
  store i8 %".10", i8* %".5"
  %".12" = load %"Card"*, %"Card"** %".3"
  %".13" = getelementptr %"Card", %"Card"* %".12", i32 0, i32 0
  %".14" = load i2, i2* %".13"
  %".15" = call i8 @"suit_to_char"(i2 %".14")
  store i8 %".15", i8* %".6"
  %".17" = alloca [5 x i8]
  store [5 x i8] [i8 37, i8 99, i8 37, i8 99, i8 0], [5 x i8]* %".17"
  %".19" = getelementptr [5 x i8], [5 x i8]* %".17", i32 0
  %".20" = getelementptr [5 x i8], [5 x i8]* %".19", i32 0
  %".21" = bitcast [5 x i8]* %".20" to i8*
  %".22" = load i8, i8* %".5"
  %".23" = load i8, i8* %".6"
  %".24" = call i32 (i8*, ...) @"printf"(i8* %".21", i8 %".22", i8 %".23")
  ret void
}

@"Card_vtable_value" = internal constant %"Card_vtable" {[3 x i8] (%"Card"*)* @"Card_tostring"}
define i32 @"rem_extra_aces"(i32 %".1", i32 %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".6"
  %".9" = alloca i32
  store i32 %".8", i32* %".9"
  %".11" = load i32, i32* %".4"
  %".12" = alloca i32
  store i32 %".11", i32* %".12"
  br label %"entry.whileloop0"
entry.whileloop0:
  %".15" = load i32, i32* %".12"
  %".16" = sub i32 %".15", 1
  store i32 %".16", i32* %".12"
  %".18" = load i32, i32* %".9"
  %".19" = sub i32 %".18", 10
  store i32 %".19", i32* %".9"
  %".21" = load i32, i32* %".12"
  %".22" = icmp sgt i32 %".21", 0
  %".23" = load i32, i32* %".9"
  %".24" = icmp sgt i32 %".23", 21
  %".25" = and i1 %".22", %".24"
  br i1 %".25", label %"entry.whileloop0", label %"entry.endwhile0"
entry.endwhile0:
  %".27" = load i32, i32* %".12"
  %".28" = icmp eq i32 %".27", -1
  br i1 %".28", label %"entry.endwhile0.if", label %"entry.endwhile0.else"
entry.endwhile0.if:
  %".30" = load i32, i32* %".9"
  %".31" = add i32 %".30", 10
  ret i32 %".31"
entry.endwhile0.else:
  %".33" = load i32, i32* %".9"
  ret i32 %".33"
entry.endwhile0.endif:
  unreachable
}

define i32 @"get_value"(%"Card"* %".1", i32 %".2") 
{
entry:
  %".4" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = alloca i64
  %".9" = alloca %"Card"*
  %".10" = alloca i32
  store i32 0, i32* %".10"
  %".12" = alloca i32
  store i32 0, i32* %".12"
  %".14" = zext i32 0 to i64
  store i64 %".14", i64* %".8"
  br label %"forloop-0"
forloop-0:
  %".17" = load %"Card"*, %"Card"** %".4"
  %".18" = load i64, i64* %".8"
  %".19" = getelementptr %"Card", %"Card"* %".17", i64 %".18"
  store %"Card"* %".19", %"Card"** %".9"
  %".21" = load %"Card"*, %"Card"** %".9"
  %".22" = getelementptr %"Card", %"Card"* %".21", i32 0, i32 1
  %".23" = load i32, i32* %".22"
  %".24" = icmp sgt i32 %".23", 10
  br i1 %".24", label %"forloop-0.if", label %"forloop-0.else"
endfor-0:
  %".71" = load i32, i32* %".10"
  ret i32 %".71"
forloop-0.if:
  %".26" = load i32, i32* %".10"
  %".27" = add i32 %".26", 10
  store i32 %".27", i32* %".10"
  br label %"forloop-0.endif"
forloop-0.else:
  %".30" = load %"Card"*, %"Card"** %".9"
  %".31" = getelementptr %"Card", %"Card"* %".30", i32 0, i32 1
  %".32" = load i32, i32* %".31"
  %".33" = icmp eq i32 %".32", 1
  br i1 %".33", label %"forloop-0.else.if", label %"forloop-0.else.else"
forloop-0.endif:
  %".52" = load %"Card"*, %"Card"** %".9"
  %".53" = getelementptr %"Card", %"Card"* %".52", i32 0, i32 1
  %".54" = load i32, i32* %".53"
  %".55" = icmp eq i32 %".54", 1
  br i1 %".55", label %"forloop-0.endif.if", label %"forloop-0.endif.else"
forloop-0.else.if:
  %".35" = load i32, i32* %".10"
  %".36" = icmp sle i32 %".35", 21
  br i1 %".36", label %"forloop-0.else.if.if", label %"forloop-0.else.if.else"
forloop-0.else.else:
  br label %"forloop-0.else.endif"
forloop-0.else.endif:
  %".45" = load i32, i32* %".10"
  %".46" = load %"Card"*, %"Card"** %".9"
  %".47" = getelementptr %"Card", %"Card"* %".46", i32 0, i32 1
  %".48" = load i32, i32* %".47"
  %".49" = add i32 %".45", %".48"
  store i32 %".49", i32* %".10"
  br label %"forloop-0.endif"
forloop-0.else.if.if:
  %".38" = load i32, i32* %".10"
  %".39" = add i32 %".38", 10
  store i32 %".39", i32* %".10"
  br label %"forloop-0.else.if.endif"
forloop-0.else.if.else:
  br label %"forloop-0.else.if.endif"
forloop-0.else.if.endif:
  br label %"forloop-0.else.endif"
forloop-0.endif.if:
  %".57" = load i32, i32* %".12"
  %".58" = add i32 %".57", 1
  store i32 %".58", i32* %".12"
  br label %"forloop-0.endif.endif"
forloop-0.endif.else:
  br label %"forloop-0.endif.endif"
forloop-0.endif.endif:
  %".62" = load i64, i64* %".8"
  %".63" = zext i32 1 to i64
  %".64" = add i64 %".62", %".63"
  store i64 %".64", i64* %".8"
  %".66" = load i64, i64* %".8"
  %".67" = load i32, i32* %".6"
  %".68" = zext i32 %".67" to i64
  %".69" = icmp slt i64 %".66", %".68"
  br i1 %".69", label %"forloop-0", label %"endfor-0"
}

define void @"print_hand"(%"Card"* %".1", i32 %".2") 
{
entry:
  %".4" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = alloca i64
  %".9" = alloca %"Card"*
  %".10" = zext i32 0 to i64
  store i64 %".10", i64* %".8"
  br label %"forloop-0"
forloop-0:
  %".13" = load %"Card"*, %"Card"** %".4"
  %".14" = load i64, i64* %".8"
  %".15" = getelementptr %"Card", %"Card"* %".13", i64 %".14"
  call void @"Card_print"(%"Card"* %".15")
  %".17" = alloca [2 x i8]
  store [2 x i8] [i8 32, i8 0], [2 x i8]* %".17"
  %".19" = getelementptr [2 x i8], [2 x i8]* %".17", i32 0
  %".20" = getelementptr [2 x i8], [2 x i8]* %".19", i32 0
  %".21" = bitcast [2 x i8]* %".20" to i8*
  %".22" = call i32 (i8*, ...) @"printf"(i8* %".21")
  %".23" = load i64, i64* %".8"
  %".24" = zext i32 1 to i64
  %".25" = add i64 %".23", %".24"
  store i64 %".25", i64* %".8"
  %".27" = load i64, i64* %".8"
  %".28" = load i32, i32* %".6"
  %".29" = zext i32 %".28" to i64
  %".30" = icmp slt i64 %".27", %".29"
  br i1 %".30", label %"forloop-0", label %"endfor-0"
endfor-0:
  ret void
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }