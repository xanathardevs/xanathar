; ModuleID = "tests/blackjack/blackjack.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"Stringifyable_vtable" = type {[3 x i8] ()*}
%"Stringifyable" = type {}
%"Card" = type {i2, i32}
%"Card_vtable" = type {[3 x i8] (%"Card"*)*}
define void @"main"(i32 %".1", i8** %".2") !dbg !10
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %"called_time" = call i32 @"time"(i32 0), !dbg !11
  call void @"srand"(i32 %"called_time"), !dbg !12
  %"$USED_CARDS" = alloca i64
  call void @"llvm.dbg.declare"(metadata i64* %"$USED_CARDS", metadata !86, metadata !22), !dbg !87
  %".7" = zext i32 0 to i64
  call void @"llvm.dbg.value"(metadata i64 %".7", metadata !86, metadata !22), !dbg !88
  store i64 %".7", i64* %"$USED_CARDS"
  %".10" = mul i32 52, 8
  %"called_malloc" = call i8* @"malloc"(i32 %".10"), !dbg !89
  %".11" = bitcast i8* %"called_malloc" to %"Card"*
  %".12" = alloca %"Card"*
  store %"Card"* %".11", %"Card"** %".12"
  %"loaded_$deck" = load %"Card"*, %"Card"** %".12"
  call void @"init_deck"(%"Card"* %"loaded_$deck"), !dbg !110
  %".14" = mul i32 21, 8
  %"called_malloc.1" = call i8* @"malloc"(i32 %".14"), !dbg !111
  %".15" = bitcast i8* %"called_malloc.1" to %"Card"*
  %".16" = alloca %"Card"*
  store %"Card"* %".15", %"Card"** %".16"
  %".18" = alloca i32
  store i32 0, i32* %".18"
  %".20" = mul i32 21, 8
  %"called_malloc.2" = call i8* @"malloc"(i32 %".20"), !dbg !112
  %".21" = bitcast i8* %"called_malloc.2" to %"Card"*
  %".22" = alloca %"Card"*
  store %"Card"* %".21", %"Card"** %".22"
  %".24" = alloca i32
  store i32 0, i32* %".24"
  %".26" = getelementptr [10 x i8], [10 x i8]* @"strf", i32 0
  %".27" = getelementptr [10 x i8], [10 x i8]* %".26", i32 0
  %".28" = bitcast [10 x i8]* %".27" to i8*
  %"loaded_$deck.1" = load %"Card"*, %"Card"** %".12"
  %"loaded_$myhand" = load %"Card"*, %"Card"** %".16"
  %"loaded_$ophand" = load %"Card"*, %"Card"** %".22"
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".28", %"Card"* %"loaded_$deck.1", %"Card"* %"loaded_$myhand", %"Card"* %"loaded_$ophand"), !dbg !113
  %"loaded_$deck.2" = load %"Card"*, %"Card"** %".12"
  call void (...) @"shuffle"(%"Card"* %"loaded_$deck.2", i32 52, i32 8), !dbg !114
  %"loaded_$deck.3" = load %"Card"*, %"Card"** %".12"
  %"loaded_$USED_CARDS" = load i64, i64* %"$USED_CARDS"
  %".29" = getelementptr %"Card", %"Card"* %"loaded_$deck.3", i64 %"loaded_$USED_CARDS"
  %"geped_from_myhand" = getelementptr %"Card"*, %"Card"** %".16", i32 0
  store %"Card"* %".29", %"Card"** %"geped_from_myhand"
  %"loaded_$USED_CARDS.1" = load i64, i64* %"$USED_CARDS"
  %".31" = zext i32 1 to i64
  %".32" = add i64 %"loaded_$USED_CARDS.1", %".31"
  call void @"llvm.dbg.value"(metadata i64 %".32", metadata !86, metadata !22), !dbg !115
  store i64 %".32", i64* %"$USED_CARDS"
  %"loaded_$deck.4" = load %"Card"*, %"Card"** %".12"
  %"loaded_$USED_CARDS.2" = load i64, i64* %"$USED_CARDS"
  %".35" = getelementptr %"Card", %"Card"* %"loaded_$deck.4", i64 %"loaded_$USED_CARDS.2"
  %"geped_from_myhand.1" = getelementptr %"Card"*, %"Card"** %".16", i32 1
  store %"Card"* %".35", %"Card"** %"geped_from_myhand.1"
  %"loaded_$USED_CARDS.3" = load i64, i64* %"$USED_CARDS"
  %".37" = zext i32 1 to i64
  %".38" = add i64 %"loaded_$USED_CARDS.3", %".37"
  call void @"llvm.dbg.value"(metadata i64 %".38", metadata !86, metadata !22), !dbg !116
  store i64 %".38", i64* %"$USED_CARDS"
  %"loaded_$deck.5" = load %"Card"*, %"Card"** %".12"
  %"loaded_$USED_CARDS.4" = load i64, i64* %"$USED_CARDS"
  %".41" = getelementptr %"Card", %"Card"* %"loaded_$deck.5", i64 %"loaded_$USED_CARDS.4"
  %"geped_from_ophand" = getelementptr %"Card"*, %"Card"** %".22", i32 0
  store %"Card"* %".41", %"Card"** %"geped_from_ophand"
  %"loaded_$ophand.1" = load %"Card"*, %"Card"** %".22"
  %".43" = getelementptr %"Card", %"Card"* %"loaded_$ophand.1", i32 0
  %".44" = alloca %"Card"*
  store %"Card"* %".43", %"Card"** %".44"
  %"loaded_$USED_CARDS.5" = load i64, i64* %"$USED_CARDS"
  %".46" = zext i32 1 to i64
  %".47" = add i64 %"loaded_$USED_CARDS.5", %".46"
  call void @"llvm.dbg.value"(metadata i64 %".47", metadata !86, metadata !22), !dbg !117
  store i64 %".47", i64* %"$USED_CARDS"
  %"loaded_$deck.6" = load %"Card"*, %"Card"** %".12"
  %"loaded_$USED_CARDS.6" = load i64, i64* %"$USED_CARDS"
  %".50" = getelementptr %"Card", %"Card"* %"loaded_$deck.6", i64 %"loaded_$USED_CARDS.6"
  %"geped_from_ophand.1" = getelementptr %"Card"*, %"Card"** %".22", i32 1
  store %"Card"* %".50", %"Card"** %"geped_from_ophand.1"
  %"loaded_$USED_CARDS.7" = load i64, i64* %"$USED_CARDS"
  %".52" = zext i32 1 to i64
  %".53" = add i64 %"loaded_$USED_CARDS.7", %".52"
  call void @"llvm.dbg.value"(metadata i64 %".53", metadata !86, metadata !22), !dbg !118
  store i64 %".53", i64* %"$USED_CARDS"
  store i32 2, i32* %".18"
  store i32 2, i32* %".24"
  %"loaded_$myhand.1" = load %"Card"*, %"Card"** %".16"
  %"loaded_$cards_i_have" = load i32, i32* %".18"
  %"called_get_value" = call i32 @"get_value"(%"Card"* %"loaded_$myhand.1", i32 %"loaded_$cards_i_have"), !dbg !119
  %".58" = alloca i32
  store i32 %"called_get_value", i32* %".58"
  %"loaded_$ophand.2" = load %"Card"*, %"Card"** %".22"
  %"loaded_$cards_op_has" = load i32, i32* %".24"
  %"called_get_value.1" = call i32 @"get_value"(%"Card"* %"loaded_$ophand.2", i32 %"loaded_$cards_op_has"), !dbg !120
  %".60" = alloca i32
  store i32 %"called_get_value.1", i32* %".60"
  %".62" = getelementptr [21 x i8], [21 x i8]* @"str6e", i32 0
  %".63" = getelementptr [21 x i8], [21 x i8]* %".62", i32 0
  %".64" = bitcast [21 x i8]* %".63" to i8*
  %"loaded_$showcard" = load %"Card"*, %"Card"** %".44"
  %".65" = getelementptr %"Card", %"Card"* %"loaded_$showcard", i32 0, i32 1
  %".66" = load i32, i32* %".65"
  %"called_num_to_char" = call i8 @"num_to_char"(i32 %".66"), !dbg !121
  %"loaded_$showcard.1" = load %"Card"*, %"Card"** %".44"
  %".67" = getelementptr %"Card", %"Card"* %"loaded_$showcard.1", i32 0, i32 0
  %".68" = load i2, i2* %".67"
  %"called_suit_to_char" = call i8 @"suit_to_char"(i2 %".68"), !dbg !122
  %".69" = getelementptr [5 x i8], [5 x i8]* @"str1d", i32 0
  %".70" = getelementptr [5 x i8], [5 x i8]* %".69", i32 0
  %".71" = bitcast [5 x i8]* %".70" to i8*
  %"called_printf.1" = call i32 (i8*, ...) @"printf"(i8* %".64", i8 %"called_num_to_char", i8 %"called_suit_to_char", i8* %".71"), !dbg !123
  %".72" = alloca i32
  store i32 0, i32* %".72"
  %"$x" = alloca i8*
  call void @"llvm.dbg.declare"(metadata i8** %"$x", metadata !126, metadata !22), !dbg !127
  %"$y" = alloca i8
  call void @"llvm.dbg.declare"(metadata i8* %"$y", metadata !129, metadata !22), !dbg !130
  call void @"llvm.dbg.value"(metadata i8* %"$y", metadata !126, metadata !22), !dbg !131
  store i8* %"$y", i8** %"$x"
  br label %"entry.whileloop0"
entry.whileloop0:
  %".79" = getelementptr [11 x i8], [11 x i8]* @"str91", i32 0
  %".80" = getelementptr [11 x i8], [11 x i8]* %".79", i32 0
  %".81" = bitcast [11 x i8]* %".80" to i8*
  %"called_printf.2" = call i32 (i8*, ...) @"printf"(i8* %".81"), !dbg !132
  %"loaded_$myhand.2" = load %"Card"*, %"Card"** %".16"
  %"loaded_$cards_i_have.1" = load i32, i32* %".18"
  call void @"print_hand"(%"Card"* %"loaded_$myhand.2", i32 %"loaded_$cards_i_have.1"), !dbg !133
  %".82" = getelementptr [28 x i8], [28 x i8]* @"str33", i32 0
  %".83" = getelementptr [28 x i8], [28 x i8]* %".82", i32 0
  %".84" = bitcast [28 x i8]* %".83" to i8*
  %"called_printf.3" = call i32 (i8*, ...) @"printf"(i8* %".84"), !dbg !134
  %".85" = trunc i32 0 to i8
  call void @"llvm.dbg.value"(metadata i8 %".85", metadata !129, metadata !22), !dbg !135
  store i8 %".85", i8* %"$y"
  %"called_get" = call i8* @"get"(i8* %"$y", i32 10), !dbg !136
  %"loaded_$y" = load i8, i8* %"$y"
  %".88" = getelementptr [2 x i8], [2 x i8]* @"str59", i32 0
  %".89" = getelementptr [2 x i8], [2 x i8]* %".88", i32 0
  %".90" = bitcast [2 x i8]* %".89" to i8*
  %"derefed_from_.90" = load i8, i8* %".90"
  %".91" = icmp eq i8 %"loaded_$y", %"derefed_from_.90"
  br i1 %".91", label %"entry.whileloop0.if", label %"entry.whileloop0.else"
entry.endwhile0:
  br label %"entry.whileloop0.1"
entry.whileloop0.if:
  %"loaded_$deck.7" = load %"Card"*, %"Card"** %".12"
  %"loaded_$USED_CARDS.8" = load i64, i64* %"$USED_CARDS"
  %".93" = getelementptr %"Card", %"Card"* %"loaded_$deck.7", i64 %"loaded_$USED_CARDS.8"
  %"loaded_$cards_i_have.2" = load i32, i32* %".18"
  %"geped_from_myhand.2" = getelementptr %"Card"*, %"Card"** %".16", i32 %"loaded_$cards_i_have.2"
  store %"Card"* %".93", %"Card"** %"geped_from_myhand.2"
  %"loaded_$myhand.3" = load %"Card"*, %"Card"** %".16"
  %"loaded_$cards_i_have.3" = load i32, i32* %".18"
  %".95" = getelementptr %"Card", %"Card"* %"loaded_$myhand.3", i32 %"loaded_$cards_i_have.3"
  %".96" = alloca %"Card"*
  store %"Card"* %".95", %"Card"** %".96"
  %".98" = getelementptr [15 x i8], [15 x i8]* @"strcf", i32 0
  %".99" = getelementptr [15 x i8], [15 x i8]* %".98", i32 0
  %".100" = bitcast [15 x i8]* %".99" to i8*
  %"loaded_$nc" = load %"Card"*, %"Card"** %".96"
  %".101" = getelementptr %"Card", %"Card"* %"loaded_$nc", i32 0, i32 1
  %".102" = load i32, i32* %".101"
  %"called_num_to_char.1" = call i8 @"num_to_char"(i32 %".102"), !dbg !137
  %"loaded_$nc.1" = load %"Card"*, %"Card"** %".96"
  %".103" = getelementptr %"Card", %"Card"* %"loaded_$nc.1", i32 0, i32 0
  %".104" = load i2, i2* %".103"
  %"called_suit_to_char.1" = call i8 @"suit_to_char"(i2 %".104"), !dbg !138
  %"called_printf.4" = call i32 (i8*, ...) @"printf"(i8* %".100", i8 %"called_num_to_char.1", i8 %"called_suit_to_char.1"), !dbg !139
  %"loaded_$cards_i_have.4" = load i32, i32* %".18"
  %".105" = add i32 %"loaded_$cards_i_have.4", 1
  store i32 %".105", i32* %".18"
  %"loaded_$USED_CARDS.9" = load i64, i64* %"$USED_CARDS"
  %".107" = zext i32 1 to i64
  %".108" = add i64 %"loaded_$USED_CARDS.9", %".107"
  call void @"llvm.dbg.value"(metadata i64 %".108", metadata !86, metadata !22), !dbg !140
  store i64 %".108", i64* %"$USED_CARDS"
  br label %"entry.whileloop0.endif"
entry.whileloop0.else:
  br label %"entry.endwhile0"
entry.whileloop0.endif:
  %".113" = trunc i32 1 to i1
  br i1 %".113", label %"entry.whileloop0", label %"entry.endwhile0"
entry.whileloop0.1:
  %"loaded_$ov" = load i32, i32* %".60"
  %"icmp_>=" = icmp sge i32 %"loaded_$ov", 17
  br i1 %"icmp_>=", label %"entry.whileloop0.1.if", label %"entry.whileloop0.1.else"
entry.endwhile0.1:
  %"loaded_$myhand.4" = load %"Card"*, %"Card"** %".16"
  %"loaded_$cards_i_have.5" = load i32, i32* %".18"
  %"called_get_value.3" = call i32 @"get_value"(%"Card"* %"loaded_$myhand.4", i32 %"loaded_$cards_i_have.5"), !dbg !150
  store i32 %"called_get_value.3", i32* %".58"
  %"loaded_$mv" = load i32, i32* %".58"
  %"loaded_$ov.2" = load i32, i32* %".60"
  call void @"compare"(i32 %"loaded_$mv", i32 %"loaded_$ov.2"), !dbg !151
  %".130" = getelementptr [12 x i8], [12 x i8]* @"str8", i32 0
  %".131" = getelementptr [12 x i8], [12 x i8]* %".130", i32 0
  %".132" = bitcast [12 x i8]* %".131" to i8*
  %"called_printf.5" = call i32 (i8*, ...) @"printf"(i8* %".132"), !dbg !152
  %"loaded_$myhand.5" = load %"Card"*, %"Card"** %".16"
  %"loaded_$cards_i_have.6" = load i32, i32* %".18"
  call void @"print_hand"(%"Card"* %"loaded_$myhand.5", i32 %"loaded_$cards_i_have.6"), !dbg !153
  %".133" = getelementptr [19 x i8], [19 x i8]* @"strb7", i32 0
  %".134" = getelementptr [19 x i8], [19 x i8]* %".133", i32 0
  %".135" = bitcast [19 x i8]* %".134" to i8*
  %"called_printf.6" = call i32 (i8*, ...) @"printf"(i8* %".135"), !dbg !154
  %"loaded_$ophand.4" = load %"Card"*, %"Card"** %".22"
  %"loaded_$cards_op_has.4" = load i32, i32* %".24"
  call void @"print_hand"(%"Card"* %"loaded_$ophand.4", i32 %"loaded_$cards_op_has.4"), !dbg !155
  %".136" = getelementptr [2 x i8], [2 x i8]* @"str8c", i32 0
  %".137" = getelementptr [2 x i8], [2 x i8]* %".136", i32 0
  %".138" = bitcast [2 x i8]* %".137" to i8*
  %"called_printf.7" = call i32 (i8*, ...) @"printf"(i8* %".138"), !dbg !156
  %".139" = getelementptr [20 x i8], [20 x i8]* @"strb3", i32 0
  %".140" = getelementptr [20 x i8], [20 x i8]* %".139", i32 0
  %".141" = bitcast [20 x i8]* %".140" to i8*
  %"loaded_$deck.9" = load %"Card"*, %"Card"** %".12"
  %"called_printf.8" = call i32 (i8*, ...) @"printf"(i8* %".141", %"Card"* %"loaded_$deck.9"), !dbg !157
  %"loaded_$deck.10" = load %"Card"*, %"Card"** %".12"
  call void (...) @"free"(%"Card"* %"loaded_$deck.10"), !dbg !158
  %".142" = getelementptr [13 x i8], [13 x i8]* @"str3c", i32 0
  %".143" = getelementptr [13 x i8], [13 x i8]* %".142", i32 0
  %".144" = bitcast [13 x i8]* %".143" to i8*
  %"called_printf.9" = call i32 (i8*, ...) @"printf"(i8* %".144"), !dbg !159
  %".145" = getelementptr [22 x i8], [22 x i8]* @"str0", i32 0
  %".146" = getelementptr [22 x i8], [22 x i8]* %".145", i32 0
  %".147" = bitcast [22 x i8]* %".146" to i8*
  %"loaded_$myhand.6" = load %"Card"*, %"Card"** %".16"
  %"called_printf.10" = call i32 (i8*, ...) @"printf"(i8* %".147", %"Card"* %"loaded_$myhand.6"), !dbg !160
  %"loaded_$myhand.7" = load %"Card"*, %"Card"** %".16"
  call void (...) @"free"(%"Card"* %"loaded_$myhand.7"), !dbg !161
  %".148" = getelementptr [15 x i8], [15 x i8]* @"strc7", i32 0
  %".149" = getelementptr [15 x i8], [15 x i8]* %".148", i32 0
  %".150" = bitcast [15 x i8]* %".149" to i8*
  %"called_printf.11" = call i32 (i8*, ...) @"printf"(i8* %".150"), !dbg !162
  %".151" = getelementptr [22 x i8], [22 x i8]* @"str3e", i32 0
  %".152" = getelementptr [22 x i8], [22 x i8]* %".151", i32 0
  %".153" = bitcast [22 x i8]* %".152" to i8*
  %"loaded_$ophand.5" = load %"Card"*, %"Card"** %".22"
  %"called_printf.12" = call i32 (i8*, ...) @"printf"(i8* %".153", %"Card"* %"loaded_$ophand.5"), !dbg !163
  %"loaded_$ophand.6" = load %"Card"*, %"Card"** %".22"
  call void (...) @"free"(%"Card"* %"loaded_$ophand.6"), !dbg !164
  %".154" = getelementptr [15 x i8], [15 x i8]* @"str7", i32 0
  %".155" = getelementptr [15 x i8], [15 x i8]* %".154", i32 0
  %".156" = bitcast [15 x i8]* %".155" to i8*
  %"called_printf.13" = call i32 (i8*, ...) @"printf"(i8* %".156"), !dbg !165
  ret void
entry.whileloop0.1.if:
  br label %"entry.endwhile0.1"
entry.whileloop0.1.else:
  br label %"entry.whileloop0.1.endif"
entry.whileloop0.1.endif:
  %"loaded_$deck.8" = load %"Card"*, %"Card"** %".12"
  %"loaded_$USED_CARDS.10" = load i64, i64* %"$USED_CARDS"
  %".119" = getelementptr %"Card", %"Card"* %"loaded_$deck.8", i64 %"loaded_$USED_CARDS.10"
  %"loaded_$cards_op_has.1" = load i32, i32* %".24"
  %"geped_from_ophand.2" = getelementptr %"Card"*, %"Card"** %".22", i32 %"loaded_$cards_op_has.1"
  store %"Card"* %".119", %"Card"** %"geped_from_ophand.2"
  %"loaded_$cards_op_has.2" = load i32, i32* %".24"
  %".121" = add i32 %"loaded_$cards_op_has.2", 1
  store i32 %".121", i32* %".24"
  %"loaded_$USED_CARDS.11" = load i64, i64* %"$USED_CARDS"
  %".123" = zext i32 1 to i64
  %".124" = add i64 %"loaded_$USED_CARDS.11", %".123"
  call void @"llvm.dbg.value"(metadata i64 %".124", metadata !86, metadata !22), !dbg !141
  store i64 %".124", i64* %"$USED_CARDS"
  %"loaded_$ophand.3" = load %"Card"*, %"Card"** %".22"
  %"loaded_$cards_op_has.3" = load i32, i32* %".24"
  %"called_get_value.2" = call i32 @"get_value"(%"Card"* %"loaded_$ophand.3", i32 %"loaded_$cards_op_has.3"), !dbg !142
  store i32 %"called_get_value.2", i32* %".60"
  %"loaded_$ov.1" = load i32, i32* %".60"
  %"icmp_<" = icmp slt i32 %"loaded_$ov.1", 17
  br i1 %"icmp_<", label %"entry.whileloop0.1", label %"entry.endwhile0.1"
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"llvm.dbg.declare"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

declare void @"llvm.dbg.value"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
declare external i8* @"get"(i8* %".1", i32 %".2") 

declare external i32 @"get_int"() 

declare external i32 @"atoi"(i8* %".1") 

declare external i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

declare external void @"nop"() 

declare external i32* @"range"(i32 %".1", i32 %".2") 

declare external i8* @"malloc"(i32 %".1") 

declare external void @"free"(...) 

declare external i32 @"cmp"(i32 %".1", i32 %".2") 

declare external void @"inc"(i32* %".1") 

declare external void @"dec"(i32* %".1") 

declare external void @"srand"(i32 %".1") 

declare external i32 @"rand"() 

declare external void @"shuffle"(...) 

declare external i32 @"time"(i32 %".1") 

declare external i8* @"ctime"(i32* %".1") 

define i8 @"suit_to_char"(i2 %".1") noinline!dbg !16
{
entry:
  %"$suit" = alloca i2
  store i2 %".1", i2* %"$suit"
  %"loaded_$suit" = load i2, i2* %"$suit"
  %".4" = zext i2 %"loaded_$suit" to i32
  %".5" = alloca i32
  store i32 %".4", i32* %".5"
  %"loaded_$z" = load i32, i32* %".5"
  %".7" = icmp eq i32 %"loaded_$z", 0
  br i1 %".7", label %"entry.if", label %"entry.else"
entry.if:
  %".9" = getelementptr [2 x i8], [2 x i8]* @"str", i32 0
  %".10" = getelementptr [2 x i8], [2 x i8]* %".9", i32 0
  %".11" = bitcast [2 x i8]* %".10" to i8*
  %"derefed_from_.11" = load i8, i8* %".11"
  ret i8 %"derefed_from_.11"
entry.else:
  %"loaded_$z.1" = load i32, i32* %".5"
  %".13" = icmp eq i32 %"loaded_$z.1", 1
  br i1 %".13", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %".38" = getelementptr [2 x i8], [2 x i8]* @"str9", i32 0
  %".39" = getelementptr [2 x i8], [2 x i8]* %".38", i32 0
  %".40" = bitcast [2 x i8]* %".39" to i8*
  %"derefed_from_.40" = load i8, i8* %".40"
  ret i8 %"derefed_from_.40"
entry.else.if:
  %".15" = getelementptr [2 x i8], [2 x i8]* @"str6", i32 0
  %".16" = getelementptr [2 x i8], [2 x i8]* %".15", i32 0
  %".17" = bitcast [2 x i8]* %".16" to i8*
  %"derefed_from_.17" = load i8, i8* %".17"
  ret i8 %"derefed_from_.17"
entry.else.else:
  %"loaded_$z.2" = load i32, i32* %".5"
  %".19" = icmp eq i32 %"loaded_$z.2", 2
  br i1 %".19", label %"entry.else.else.if", label %"entry.else.else.else"
entry.else.endif:
  br label %"entry.endif"
entry.else.else.if:
  %".21" = getelementptr [2 x i8], [2 x i8]* @"str67", i32 0
  %".22" = getelementptr [2 x i8], [2 x i8]* %".21", i32 0
  %".23" = bitcast [2 x i8]* %".22" to i8*
  %"derefed_from_.23" = load i8, i8* %".23"
  ret i8 %"derefed_from_.23"
entry.else.else.else:
  %"loaded_$z.3" = load i32, i32* %".5"
  %".25" = icmp eq i32 %"loaded_$z.3", 3
  br i1 %".25", label %"entry.else.else.else.if", label %"entry.else.else.else.else"
entry.else.else.endif:
  br label %"entry.else.endif"
entry.else.else.else.if:
  %".27" = getelementptr [2 x i8], [2 x i8]* @"str3", i32 0
  %".28" = getelementptr [2 x i8], [2 x i8]* %".27", i32 0
  %".29" = bitcast [2 x i8]* %".28" to i8*
  %"derefed_from_.29" = load i8, i8* %".29"
  ret i8 %"derefed_from_.29"
entry.else.else.else.else:
  %".31" = getelementptr [2 x i8], [2 x i8]* @"str5", i32 0
  %".32" = getelementptr [2 x i8], [2 x i8]* %".31", i32 0
  %".33" = bitcast [2 x i8]* %".32" to i8*
  %"derefed_from_.33" = load i8, i8* %".33"
  ret i8 %"derefed_from_.33"
entry.else.else.else.endif:
  unreachable
}

@"str" = internal global [2 x i8] [i8 67, i8 0]
@"str6" = internal global [2 x i8] [i8 83, i8 0]
@"str67" = internal global [2 x i8] [i8 68, i8 0]
@"str3" = internal global [2 x i8] [i8 72, i8 0]
@"str5" = internal global [2 x i8] [i8 69, i8 0]
@"str9" = internal global [2 x i8] [i8 69, i8 0]
define i8 @"num_to_char"(i32 %".1") noinline!dbg !20
{
entry:
  %"$num" = alloca i32
  store i32 %".1", i32* %"$num"
  %"loaded_$num" = load i32, i32* %"$num"
  %".4" = icmp eq i32 %"loaded_$num", 10
  br i1 %".4", label %"entry.if", label %"entry.else"
entry.if:
  %".6" = getelementptr [2 x i8], [2 x i8]* @"strd", i32 0
  %".7" = getelementptr [2 x i8], [2 x i8]* %".6", i32 0
  %".8" = bitcast [2 x i8]* %".7" to i8*
  %"derefed_from_.8" = load i8, i8* %".8"
  ret i8 %"derefed_from_.8"
entry.else:
  %"loaded_$num.1" = load i32, i32* %"$num"
  %".10" = icmp eq i32 %"loaded_$num.1", 11
  br i1 %".10", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %"$_x" = alloca [1 x i8]
  call void @"llvm.dbg.declare"(metadata [1 x i8]* %"$_x", metadata !21, metadata !22), !dbg !23
  %".40" = bitcast [1 x i8]* %"$_x" to i8*
  %".41" = getelementptr [3 x i8], [3 x i8]* @"str1", i32 0
  %".42" = getelementptr [3 x i8], [3 x i8]* %".41", i32 0
  %".43" = bitcast [3 x i8]* %".42" to i8*
  %"loaded_$num.5" = load i32, i32* %"$num"
  %"called_sprintf" = call i32 (i8*, i8*, ...) @"sprintf"(i8* %".40", i8* %".43", i32 %"loaded_$num.5"), !dbg !24
  %".44" = bitcast [1 x i8]* %"$_x" to i8*
  %"derefed_from_.44" = load i8, i8* %".44"
  ret i8 %"derefed_from_.44"
entry.else.if:
  %".12" = getelementptr [2 x i8], [2 x i8]* @"strda", i32 0
  %".13" = getelementptr [2 x i8], [2 x i8]* %".12", i32 0
  %".14" = bitcast [2 x i8]* %".13" to i8*
  %"derefed_from_.14" = load i8, i8* %".14"
  ret i8 %"derefed_from_.14"
entry.else.else:
  %"loaded_$num.2" = load i32, i32* %"$num"
  %".16" = icmp eq i32 %"loaded_$num.2", 12
  br i1 %".16", label %"entry.else.else.if", label %"entry.else.else.else"
entry.else.endif:
  br label %"entry.endif"
entry.else.else.if:
  %".18" = getelementptr [2 x i8], [2 x i8]* @"strdd", i32 0
  %".19" = getelementptr [2 x i8], [2 x i8]* %".18", i32 0
  %".20" = bitcast [2 x i8]* %".19" to i8*
  %"derefed_from_.20" = load i8, i8* %".20"
  ret i8 %"derefed_from_.20"
entry.else.else.else:
  %"loaded_$num.3" = load i32, i32* %"$num"
  %".22" = icmp eq i32 %"loaded_$num.3", 13
  br i1 %".22", label %"entry.else.else.else.if", label %"entry.else.else.else.else"
entry.else.else.endif:
  br label %"entry.else.endif"
entry.else.else.else.if:
  %".24" = getelementptr [2 x i8], [2 x i8]* @"strc", i32 0
  %".25" = getelementptr [2 x i8], [2 x i8]* %".24", i32 0
  %".26" = bitcast [2 x i8]* %".25" to i8*
  %"derefed_from_.26" = load i8, i8* %".26"
  ret i8 %"derefed_from_.26"
entry.else.else.else.else:
  %"loaded_$num.4" = load i32, i32* %"$num"
  %".28" = icmp eq i32 %"loaded_$num.4", 1
  br i1 %".28", label %"entry.else.else.else.else.if", label %"entry.else.else.else.else.else"
entry.else.else.else.endif:
  br label %"entry.else.else.endif"
entry.else.else.else.else.if:
  %".30" = getelementptr [2 x i8], [2 x i8]* @"strb", i32 0
  %".31" = getelementptr [2 x i8], [2 x i8]* %".30", i32 0
  %".32" = bitcast [2 x i8]* %".31" to i8*
  %"derefed_from_.32" = load i8, i8* %".32"
  ret i8 %"derefed_from_.32"
entry.else.else.else.else.else:
  br label %"entry.else.else.else.else.endif"
entry.else.else.else.else.endif:
  br label %"entry.else.else.else.endif"
}

@"strd" = internal global [2 x i8] [i8 84, i8 0]
@"strda" = internal global [2 x i8] [i8 74, i8 0]
@"strdd" = internal global [2 x i8] [i8 81, i8 0]
@"strc" = internal global [2 x i8] [i8 75, i8 0]
@"strb" = internal global [2 x i8] [i8 65, i8 0]
@"str1" = internal global [3 x i8] [i8 37, i8 105, i8 0]
define i32 @"Card__init_hook"(%"Card"* %".1", i2 %".2", i32 %".3") noinline!dbg !28
{
entry:
  %"$self" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %"$self"
  %"$s" = alloca i2
  store i2 %".2", i2* %"$s"
  %"$card" = alloca i32
  store i32 %".3", i32* %"$card"
  %"loaded_$self" = load %"Card"*, %"Card"** %"$self"
  %".8" = getelementptr %"Card", %"Card"* %"loaded_$self", i32 0, i32 0
  %"loaded_$s" = load i2, i2* %"$s"
  store i2 %"loaded_$s", i2* %".8"
  %"loaded_$self.1" = load %"Card"*, %"Card"** %"$self"
  %".10" = getelementptr %"Card", %"Card"* %"loaded_$self.1", i32 0, i32 1
  %"loaded_$card" = load i32, i32* %"$card"
  store i32 %"loaded_$card", i32* %".10"
  ret i32 0
}

define [3 x i8] @"Card_tostring"(%"Card"* %".1") 
{
entry:
  %".3" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".3"
  %"$_x" = alloca [3 x i8]
  call void @"llvm.dbg.declare"(metadata [3 x i8]* %"$_x", metadata !29, metadata !22), !dbg !30
  %"loaded_$_x" = load [3 x i8], [3 x i8]* %"$_x"
  ret [3 x i8] %"loaded_$_x"
}

define void @"Card_print"(%"Card"* %".1") noinline!dbg !34
{
entry:
  %"$self" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %"$self"
  %"$n" = alloca i8
  call void @"llvm.dbg.declare"(metadata i8* %"$n", metadata !36, metadata !22), !dbg !37
  %"$s" = alloca i8
  call void @"llvm.dbg.declare"(metadata i8* %"$s", metadata !39, metadata !22), !dbg !40
  %"loaded_$self" = load %"Card"*, %"Card"** %"$self"
  %".6" = getelementptr %"Card", %"Card"* %"loaded_$self", i32 0, i32 1
  %".7" = load i32, i32* %".6"
  %"called_num_to_char" = call i8 @"num_to_char"(i32 %".7"), !dbg !41
  call void @"llvm.dbg.value"(metadata i8 %"called_num_to_char", metadata !36, metadata !22), !dbg !42
  store i8 %"called_num_to_char", i8* %"$n"
  %"loaded_$self.1" = load %"Card"*, %"Card"** %"$self"
  %".10" = getelementptr %"Card", %"Card"* %"loaded_$self.1", i32 0, i32 0
  %".11" = load i2, i2* %".10"
  %"called_suit_to_char" = call i8 @"suit_to_char"(i2 %".11"), !dbg !43
  call void @"llvm.dbg.value"(metadata i8 %"called_suit_to_char", metadata !39, metadata !22), !dbg !44
  store i8 %"called_suit_to_char", i8* %"$s"
  %".14" = getelementptr [5 x i8], [5 x i8]* @"str4", i32 0
  %".15" = getelementptr [5 x i8], [5 x i8]* %".14", i32 0
  %".16" = bitcast [5 x i8]* %".15" to i8*
  %"loaded_$n" = load i8, i8* %"$n"
  %"loaded_$s" = load i8, i8* %"$s"
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".16", i8 %"loaded_$n", i8 %"loaded_$s"), !dbg !45
  ret void
}

@"str4" = internal global [5 x i8] [i8 37, i8 99, i8 37, i8 99, i8 0]
@"Card_vtable_value" = internal constant %"Card_vtable" {[3 x i8] (%"Card"*)* @"Card_tostring"}
define i32 @"rem_extra_aces"(i32 %".1", i32 %".2") noinline!dbg !49
{
entry:
  %"$aces" = alloca i32
  store i32 %".1", i32* %"$aces"
  %"$value" = alloca i32
  store i32 %".2", i32* %"$value"
  %"loaded_$value" = load i32, i32* %"$value"
  %".6" = alloca i32
  store i32 %"loaded_$value", i32* %".6"
  %"loaded_$aces" = load i32, i32* %"$aces"
  %".8" = alloca i32
  store i32 %"loaded_$aces", i32* %".8"
  br label %"entry.whileloop0"
entry.whileloop0:
  %"loaded_$a" = load i32, i32* %".8"
  %".11" = sub i32 %"loaded_$a", 1
  store i32 %".11", i32* %".8"
  %"loaded_$v" = load i32, i32* %".6"
  %".13" = sub i32 %"loaded_$v", 10
  store i32 %".13", i32* %".6"
  %"loaded_$a.1" = load i32, i32* %".8"
  %"icmp_>" = icmp sgt i32 %"loaded_$a.1", 0
  %"loaded_$v.1" = load i32, i32* %".6"
  %"icmp_>.1" = icmp sgt i32 %"loaded_$v.1", 21
  %".15" = and i1 %"icmp_>", %"icmp_>.1"
  br i1 %".15", label %"entry.whileloop0", label %"entry.endwhile0"
entry.endwhile0:
  %"loaded_$a.2" = load i32, i32* %".8"
  %".17" = icmp eq i32 %"loaded_$a.2", -1
  br i1 %".17", label %"entry.endwhile0.if", label %"entry.endwhile0.else"
entry.endwhile0.if:
  %"loaded_$v.2" = load i32, i32* %".6"
  %".19" = add i32 %"loaded_$v.2", 10
  ret i32 %".19"
entry.endwhile0.else:
  %"loaded_$v.3" = load i32, i32* %".6"
  ret i32 %"loaded_$v.3"
entry.endwhile0.endif:
  unreachable
}

define i32 @"get_value"(%"Card"* %".1", i32 %".2") noinline!dbg !53
{
entry:
  %"$h" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %"$h"
  %"$size" = alloca i32
  store i32 %".2", i32* %"$size"
  %"$it" = alloca i64
  call void @"llvm.dbg.declare"(metadata i64* %"$it", metadata !55, metadata !22), !dbg !56
  %"$ptr" = alloca %"Card"*
  call void @"llvm.dbg.declare"(metadata %"Card"** %"$ptr", metadata !62, metadata !22), !dbg !63
  %".8" = alloca i32
  store i32 0, i32* %".8"
  %".10" = alloca i32
  store i32 0, i32* %".10"
  %".12" = zext i32 0 to i64
  call void @"llvm.dbg.value"(metadata i64 %".12", metadata !55, metadata !22), !dbg !64
  store i64 %".12", i64* %"$it"
  br label %"forloop-0"
forloop-0:
  %"loaded_$h" = load %"Card"*, %"Card"** %"$h"
  %"loaded_$it" = load i64, i64* %"$it"
  %".16" = getelementptr %"Card", %"Card"* %"loaded_$h", i64 %"loaded_$it"
  call void @"llvm.dbg.value"(metadata %"Card"* %".16", metadata !62, metadata !22), !dbg !65
  store %"Card"* %".16", %"Card"** %"$ptr"
  %"loaded_$ptr" = load %"Card"*, %"Card"** %"$ptr"
  %".19" = getelementptr %"Card", %"Card"* %"loaded_$ptr", i32 0, i32 1
  %".20" = load i32, i32* %".19"
  %"icmp_>" = icmp sgt i32 %".20", 10
  br i1 %"icmp_>", label %"forloop-0.if", label %"forloop-0.else"
endfor-0:
  %"loaded_$value.4" = load i32, i32* %".8"
  ret i32 %"loaded_$value.4"
forloop-0.if:
  %"loaded_$value" = load i32, i32* %".8"
  %".22" = add i32 %"loaded_$value", 10
  store i32 %".22", i32* %".8"
  br label %"forloop-0.endif"
forloop-0.else:
  %"loaded_$ptr.1" = load %"Card"*, %"Card"** %"$ptr"
  %".25" = getelementptr %"Card", %"Card"* %"loaded_$ptr.1", i32 0, i32 1
  %".26" = load i32, i32* %".25"
  %".27" = icmp eq i32 %".26", 1
  br i1 %".27", label %"forloop-0.else.if", label %"forloop-0.else.else"
forloop-0.endif:
  %"loaded_$ptr.3" = load %"Card"*, %"Card"** %"$ptr"
  %".41" = getelementptr %"Card", %"Card"* %"loaded_$ptr.3", i32 0, i32 1
  %".42" = load i32, i32* %".41"
  %".43" = icmp eq i32 %".42", 1
  br i1 %".43", label %"forloop-0.endif.if", label %"forloop-0.endif.else"
forloop-0.else.if:
  %"loaded_$value.1" = load i32, i32* %".8"
  %"icmp_<=" = icmp sle i32 %"loaded_$value.1", 21
  br i1 %"icmp_<=", label %"forloop-0.else.if.if", label %"forloop-0.else.if.else"
forloop-0.else.else:
  br label %"forloop-0.else.endif"
forloop-0.else.endif:
  %"loaded_$value.3" = load i32, i32* %".8"
  %"loaded_$ptr.2" = load %"Card"*, %"Card"** %"$ptr"
  %".36" = getelementptr %"Card", %"Card"* %"loaded_$ptr.2", i32 0, i32 1
  %".37" = load i32, i32* %".36"
  %".38" = add i32 %"loaded_$value.3", %".37"
  store i32 %".38", i32* %".8"
  br label %"forloop-0.endif"
forloop-0.else.if.if:
  %"loaded_$value.2" = load i32, i32* %".8"
  %".30" = add i32 %"loaded_$value.2", 10
  store i32 %".30", i32* %".8"
  br label %"forloop-0.else.if.endif"
forloop-0.else.if.else:
  br label %"forloop-0.else.if.endif"
forloop-0.else.if.endif:
  br label %"forloop-0.else.endif"
forloop-0.endif.if:
  %"loaded_$has_ace" = load i32, i32* %".10"
  %".45" = add i32 %"loaded_$has_ace", 1
  store i32 %".45", i32* %".10"
  br label %"forloop-0.endif.endif"
forloop-0.endif.else:
  br label %"forloop-0.endif.endif"
forloop-0.endif.endif:
  %"loaded_$it.1" = load i64, i64* %"$it"
  %".49" = zext i32 1 to i64
  %".50" = add i64 %"loaded_$it.1", %".49"
  call void @"llvm.dbg.value"(metadata i64 %".50", metadata !55, metadata !22), !dbg !66
  store i64 %".50", i64* %"$it"
  %"loaded_$it.2" = load i64, i64* %"$it"
  %"loaded_$size" = load i32, i32* %"$size"
  %".53" = zext i32 %"loaded_$size" to i64
  %"icmp_<" = icmp slt i64 %"loaded_$it.2", %".53"
  br i1 %"icmp_<", label %"forloop-0", label %"endfor-0"
}

define void @"print_hand"(%"Card"* %".1", i32 %".2") noinline!dbg !70
{
entry:
  %"$h" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %"$h"
  %"$size" = alloca i32
  store i32 %".2", i32* %"$size"
  %"$it" = alloca i64
  call void @"llvm.dbg.declare"(metadata i64* %"$it", metadata !72, metadata !22), !dbg !73
  %"$ptr" = alloca %"Card"*
  call void @"llvm.dbg.declare"(metadata %"Card"** %"$ptr", metadata !79, metadata !22), !dbg !80
  %".8" = zext i32 0 to i64
  call void @"llvm.dbg.value"(metadata i64 %".8", metadata !72, metadata !22), !dbg !81
  store i64 %".8", i64* %"$it"
  br label %"forloop-0"
forloop-0:
  %"loaded_$h" = load %"Card"*, %"Card"** %"$h"
  %"loaded_$it" = load i64, i64* %"$it"
  %".12" = getelementptr %"Card", %"Card"* %"loaded_$h", i64 %"loaded_$it"
  call void @"Card_print"(%"Card"* %".12"), !dbg !82
  %".13" = getelementptr [2 x i8], [2 x i8]* @"stre", i32 0
  %".14" = getelementptr [2 x i8], [2 x i8]* %".13", i32 0
  %".15" = bitcast [2 x i8]* %".14" to i8*
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".15"), !dbg !83
  %"loaded_$it.1" = load i64, i64* %"$it"
  %".16" = zext i32 1 to i64
  %".17" = add i64 %"loaded_$it.1", %".16"
  call void @"llvm.dbg.value"(metadata i64 %".17", metadata !72, metadata !22), !dbg !84
  store i64 %".17", i64* %"$it"
  %"loaded_$it.2" = load i64, i64* %"$it"
  %"loaded_$size" = load i32, i32* %"$size"
  %".20" = zext i32 %"loaded_$size" to i64
  %"icmp_<" = icmp slt i64 %"loaded_$it.2", %".20"
  br i1 %"icmp_<", label %"forloop-0", label %"endfor-0"
endfor-0:
  ret void
}

@"stre" = internal global [2 x i8] [i8 32, i8 0]
define void @"init_deck"(%"Card"* %".1") noinline!dbg !93
{
entry:
  %"$d" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %"$d"
  %"$i" = alloca i32
  call void @"llvm.dbg.declare"(metadata i32* %"$i", metadata !95, metadata !22), !dbg !96
  %"$j" = alloca i32
  call void @"llvm.dbg.declare"(metadata i32* %"$j", metadata !98, metadata !22), !dbg !99
  %"$k" = alloca i64
  call void @"llvm.dbg.declare"(metadata i64* %"$k", metadata !101, metadata !22), !dbg !102
  %".7" = zext i32 0 to i64
  call void @"llvm.dbg.value"(metadata i64 %".7", metadata !101, metadata !22), !dbg !103
  store i64 %".7", i64* %"$k"
  call void @"llvm.dbg.value"(metadata i32 0, metadata !95, metadata !22), !dbg !104
  store i32 0, i32* %"$i"
  br label %"forloop-0"
forloop-0:
  call void @"llvm.dbg.value"(metadata i32 1, metadata !98, metadata !22), !dbg !105
  store i32 1, i32* %"$j"
  br label %"forloop-1"
endfor-0:
  ret void
forloop-1:
  %"loaded_$i" = load i32, i32* %"$i"
  %".16" = trunc i32 %"loaded_$i" to i2
  %"loaded_$j" = load i32, i32* %"$j"
  %"loaded_$d" = load %"Card"*, %"Card"** %"$d"
  %"loaded_$k" = load i64, i64* %"$k"
  %".17" = getelementptr %"Card", %"Card"* %"loaded_$d", i64 %"loaded_$k"
  %"called_Card__init_hook" = call i32 @"Card__init_hook"(%"Card"* %".17", i2 %".16", i32 %"loaded_$j"), !dbg !106
  %"loaded_$k.1" = load i64, i64* %"$k"
  %".18" = zext i32 1 to i64
  %".19" = add i64 %"loaded_$k.1", %".18"
  call void @"llvm.dbg.value"(metadata i64 %".19", metadata !101, metadata !22), !dbg !107
  store i64 %".19", i64* %"$k"
  %"loaded_$j.1" = load i32, i32* %"$j"
  %".22" = add i32 %"loaded_$j.1", 1
  call void @"llvm.dbg.value"(metadata i32 %".22", metadata !98, metadata !22), !dbg !108
  store i32 %".22", i32* %"$j"
  %"loaded_$j.2" = load i32, i32* %"$j"
  %"icmp_<=" = icmp sle i32 %"loaded_$j.2", 13
  br i1 %"icmp_<=", label %"forloop-1", label %"endfor-1"
endfor-1:
  %"loaded_$i.1" = load i32, i32* %"$i"
  %".26" = add i32 %"loaded_$i.1", 1
  call void @"llvm.dbg.value"(metadata i32 %".26", metadata !95, metadata !22), !dbg !109
  store i32 %".26", i32* %"$i"
  %"loaded_$i.2" = load i32, i32* %"$i"
  %"icmp_<" = icmp slt i32 %"loaded_$i.2", 4
  br i1 %"icmp_<", label %"forloop-0", label %"endfor-0"
}

@"strf" = internal global [10 x i8] [i8 37, i8 112, i8 32, i8 37, i8 112, i8 32, i8 37, i8 112, i8 10, i8 0]
@"str6e" = internal global [21 x i8] [i8 79, i8 112, i8 112, i8 111, i8 110, i8 101, i8 110, i8 116, i8 32, i8 104, i8 97, i8 115, i8 32, i8 37, i8 99, i8 37, i8 99, i8 37, i8 115, i8 10, i8 0]
@"str1d" = internal global [5 x i8] [i8 240, i8 159, i8 130, i8 160, i8 0]
@"str91" = internal global [11 x i8] [i8 89, i8 111, i8 117, i8 32, i8 104, i8 97, i8 118, i8 101, i8 58, i8 32, i8 0]
@"str33" = internal global [28 x i8] [i8 10, i8 68, i8 111, i8 32, i8 121, i8 111, i8 117, i8 32, i8 119, i8 97, i8 110, i8 116, i8 32, i8 97, i8 32, i8 99, i8 97, i8 114, i8 100, i8 63, i8 32, i8 40, i8 121, i8 47, i8 110, i8 41, i8 32, i8 0]
@"str59" = internal global [2 x i8] [i8 121, i8 0]
@"strcf" = internal global [15 x i8] [i8 89, i8 111, i8 117, i8 32, i8 103, i8 111, i8 116, i8 32, i8 37, i8 99, i8 37, i8 99, i8 33, i8 10, i8 0]
define void @"compare"(i32 %".1", i32 %".2") noinline!dbg !146
{
entry:
  %"$m" = alloca i32
  store i32 %".1", i32* %"$m"
  %"$o" = alloca i32
  store i32 %".2", i32* %"$o"
  %".6" = alloca i32
  store i32 1, i32* %".6"
  %".8" = alloca i32
  store i32 1, i32* %".8"
  %"loaded_$m" = load i32, i32* %"$m"
  %"icmp_>" = icmp sgt i32 %"loaded_$m", 21
  br i1 %"icmp_>", label %"entry.if", label %"entry.else"
entry.if:
  store i32 0, i32* %".6"
  br label %"entry.endif"
entry.else:
  br label %"entry.endif"
entry.endif:
  %"loaded_$o" = load i32, i32* %"$o"
  %"icmp_>.1" = icmp sgt i32 %"loaded_$o", 21
  br i1 %"icmp_>.1", label %"entry.endif.if", label %"entry.endif.else"
entry.endif.if:
  store i32 0, i32* %".8"
  br label %"entry.endif.endif"
entry.endif.else:
  br label %"entry.endif.endif"
entry.endif.endif:
  %"loaded_$m.1" = load i32, i32* %"$m"
  %"loaded_$mb" = load i32, i32* %".6"
  %".18" = mul i32 %"loaded_$m.1", %"loaded_$mb"
  %".19" = alloca i32
  store i32 %".18", i32* %".19"
  %"loaded_$o.1" = load i32, i32* %"$o"
  %"loaded_$ob" = load i32, i32* %".8"
  %".21" = mul i32 %"loaded_$o.1", %"loaded_$ob"
  %".22" = alloca i32
  store i32 %".21", i32* %".22"
  %"loaded_$calc_opp" = load i32, i32* %".22"
  %"loaded_$calc_me" = load i32, i32* %".19"
  %"called_cmp" = call i32 @"cmp"(i32 %"loaded_$calc_opp", i32 %"loaded_$calc_me"), !dbg !147
  switch i32 %"called_cmp", label %"default-match-0" [i32 -1, label %"switchstmt-0-0" i32 1, label %"switchstmt-0-1"]
default-match-0:
  br label %"end-match-0"
end-match-0:
  ret void
switchstmt-0-0:
  %".26" = getelementptr [10 x i8], [10 x i8]* @"stra", i32 0
  %".27" = getelementptr [10 x i8], [10 x i8]* %".26", i32 0
  %".28" = bitcast [10 x i8]* %".27" to i8*
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".28"), !dbg !148
  br label %"end-match-0"
switchstmt-0-1:
  %".30" = getelementptr [11 x i8], [11 x i8]* @"str3b", i32 0
  %".31" = getelementptr [11 x i8], [11 x i8]* %".30", i32 0
  %".32" = bitcast [11 x i8]* %".31" to i8*
  %"called_printf.1" = call i32 (i8*, ...) @"printf"(i8* %".32"), !dbg !149
  br label %"end-match-0"
}

@"stra" = internal global [10 x i8] [i8 89, i8 111, i8 117, i8 32, i8 119, i8 105, i8 110, i8 33, i8 10, i8 0]
@"str3b" = internal global [11 x i8] [i8 89, i8 111, i8 117, i8 32, i8 108, i8 111, i8 115, i8 101, i8 33, i8 10, i8 0]
@"str8" = internal global [12 x i8] [i8 89, i8 111, i8 117, i8 114, i8 32, i8 104, i8 97, i8 110, i8 100, i8 58, i8 32, i8 0]
@"strb7" = internal global [19 x i8] [i8 10, i8 79, i8 112, i8 112, i8 111, i8 110, i8 101, i8 110, i8 116, i8 39, i8 115, i8 32, i8 104, i8 97, i8 110, i8 100, i8 58, i8 32, i8 0]
@"str8c" = internal global [2 x i8] [i8 10, i8 0]
@"strb3" = internal global [20 x i8] [i8 70, i8 114, i8 101, i8 101, i8 105, i8 110, i8 103, i8 32, i8 36, i8 100, i8 101, i8 99, i8 107, i8 32, i8 40, i8 37, i8 112, i8 41, i8 10, i8 0]
@"str3c" = internal global [13 x i8] [i8 70, i8 114, i8 101, i8 101, i8 100, i8 32, i8 36, i8 100, i8 101, i8 99, i8 107, i8 10, i8 0]
@"str0" = internal global [22 x i8] [i8 70, i8 114, i8 101, i8 101, i8 105, i8 110, i8 103, i8 32, i8 36, i8 109, i8 121, i8 104, i8 97, i8 110, i8 100, i8 32, i8 40, i8 37, i8 112, i8 41, i8 10, i8 0]
@"strc7" = internal global [15 x i8] [i8 70, i8 114, i8 101, i8 101, i8 100, i8 32, i8 36, i8 109, i8 121, i8 104, i8 97, i8 110, i8 100, i8 10, i8 0]
@"str3e" = internal global [22 x i8] [i8 70, i8 114, i8 101, i8 101, i8 105, i8 110, i8 103, i8 32, i8 36, i8 111, i8 112, i8 104, i8 97, i8 110, i8 100, i8 32, i8 40, i8 37, i8 112, i8 41, i8 10, i8 0]
@"str7" = internal global [15 x i8] [i8 70, i8 114, i8 101, i8 101, i8 100, i8 32, i8 36, i8 111, i8 112, i8 104, i8 97, i8 110, i8 100, i8 10, i8 0]
!llvm.dbg.cu = !{ !2 }
!llvm.module.flags = !{ !3, !4, !5 }
!llvm.ident = !{ !6 }
!0 = !DIFile(directory: "/home/proc-daemon/Dropbox/Xanathar/xanathar/tests/blackjack", filename: "blackjack.xan")
!1 = !{  }
!2 = distinct !DICompileUnit(emissionKind: FullDebug, enums: !1, file: !0, isOptimized: false, language: DW_LANG_Python, producer: "Xanathar v. a0.0.1", runtimeVersion: 0)
!3 = !{ i32 2, !"Dwarf Version", i32 4 }
!4 = !{ i32 2, !"Debug Info Version", i32 3 }
!5 = !{ i32 1, !"wchar_size", i32 4 }
!6 = !{ !"Xanathar a0.0.1" }
!7 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!8 = !{ !7 }
!9 = !DISubroutineType(types: !8)
!10 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !9, unit: !2)
!11 = !DILocation(column: 7, line: 7, scope: !10)
!12 = !DILocation(column: 1, line: 7, scope: !10)
!13 = !DIBasicType(encoding: DW_ATE_signed, name: "i8", size: 8)
!14 = !{ !13 }
!15 = !DISubroutineType(types: !14)
!16 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "suit_to_char", scope: !0, scopeLine: 1, type: !15, unit: !2)
!17 = !DIBasicType(encoding: DW_ATE_signed, name: "i8", size: 8)
!18 = !{ !17 }
!19 = !DISubroutineType(types: !18)
!20 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "num_to_char", scope: !0, scopeLine: 1, type: !19, unit: !2)
!21 = !DILocalVariable(file: !0, line: 1, name: "_x", scope: !20, type: null)
!22 = !DIExpression()
!23 = !DILocation(column: 5, line: 64, scope: !20)
!24 = !DILocation(column: 5, line: 65, scope: !20)
!25 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!26 = !{ !25 }
!27 = !DISubroutineType(types: !26)
!28 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "Card__init_hook", scope: !0, scopeLine: 1, type: !27, unit: !2)
!29 = !DILocalVariable(file: !0, line: 1, name: "_x", scope: !10, type: null)
!30 = !DILocation(column: 9, line: 79, scope: !10)
!31 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!32 = !{ !31 }
!33 = !DISubroutineType(types: !32)
!34 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "Card_print", scope: !0, scopeLine: 1, type: !33, unit: !2)
!35 = !DIBasicType(encoding: DW_ATE_signed, name: "i8", size: 8)
!36 = !DILocalVariable(file: !0, line: 1, name: "n", scope: !34, type: !35)
!37 = !DILocation(column: 9, line: 90, scope: !34)
!38 = !DIBasicType(encoding: DW_ATE_signed, name: "i8", size: 8)
!39 = !DILocalVariable(file: !0, line: 1, name: "s", scope: !34, type: !38)
!40 = !DILocation(column: 9, line: 91, scope: !34)
!41 = !DILocation(column: 14, line: 92, scope: !34)
!42 = !DILocation(column: 9, line: 92, scope: !34)
!43 = !DILocation(column: 14, line: 93, scope: !34)
!44 = !DILocation(column: 9, line: 93, scope: !34)
!45 = !DILocation(column: 9, line: 94, scope: !34)
!46 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!47 = !{ !46 }
!48 = !DISubroutineType(types: !47)
!49 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "rem_extra_aces", scope: !0, scopeLine: 1, type: !48, unit: !2)
!50 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!51 = !{ !50 }
!52 = !DISubroutineType(types: !51)
!53 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "get_value", scope: !0, scopeLine: 1, type: !52, unit: !2)
!54 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!55 = !DILocalVariable(file: !0, line: 1, name: "it", scope: !53, type: !54)
!56 = !DILocation(column: 5, line: 116, scope: !53)
!57 = !DIBasicType(encoding: DW_ATE_signed, name: "i2", size: 8)
!58 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!59 = !{ !57, !58 }
!60 = !DICompositeType(elements: !59, file: !0, line: 1, size: 64, tag: DW_TAG_structure_type)
!61 = !DIDerivedType(baseType: !60, size: 64, tag: DW_TAG_pointer_type)
!62 = !DILocalVariable(file: !0, line: 1, name: "ptr", scope: !53, type: !61)
!63 = !DILocation(column: 5, line: 117, scope: !53)
!64 = !DILocation(column: 9, line: 122, scope: !53)
!65 = !DILocation(column: 9, line: 123, scope: !53)
!66 = !DILocation(column: 53, line: 122, scope: !53)
!67 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!68 = !{ !67 }
!69 = !DISubroutineType(types: !68)
!70 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "print_hand", scope: !0, scopeLine: 1, type: !69, unit: !2)
!71 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!72 = !DILocalVariable(file: !0, line: 1, name: "it", scope: !70, type: !71)
!73 = !DILocation(column: 5, line: 144, scope: !70)
!74 = !DIBasicType(encoding: DW_ATE_signed, name: "i2", size: 8)
!75 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!76 = !{ !74, !75 }
!77 = !DICompositeType(elements: !76, file: !0, line: 1, size: 64, tag: DW_TAG_structure_type)
!78 = !DIDerivedType(baseType: !77, size: 64, tag: DW_TAG_pointer_type)
!79 = !DILocalVariable(file: !0, line: 1, name: "ptr", scope: !70, type: !78)
!80 = !DILocation(column: 5, line: 145, scope: !70)
!81 = !DILocation(column: 9, line: 147, scope: !70)
!82 = !DILocation(column: 9, line: 148, scope: !70)
!83 = !DILocation(column: 9, line: 149, scope: !70)
!84 = !DILocation(column: 53, line: 147, scope: !70)
!85 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!86 = !DILocalVariable(file: !0, line: 1, name: "USED_CARDS", scope: !10, type: !85)
!87 = !DILocation(column: 1, line: 155, scope: !10)
!88 = !DILocation(column: 1, line: 156, scope: !10)
!89 = !DILocation(column: 14, line: 158, scope: !10)
!90 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!91 = !{ !90 }
!92 = !DISubroutineType(types: !91)
!93 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "init_deck", scope: !0, scopeLine: 1, type: !92, unit: !2)
!94 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!95 = !DILocalVariable(file: !0, line: 1, name: "i", scope: !93, type: !94)
!96 = !DILocation(column: 5, line: 161, scope: !93)
!97 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!98 = !DILocalVariable(file: !0, line: 1, name: "j", scope: !93, type: !97)
!99 = !DILocation(column: 5, line: 162, scope: !93)
!100 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!101 = !DILocalVariable(file: !0, line: 1, name: "k", scope: !93, type: !100)
!102 = !DILocation(column: 5, line: 163, scope: !93)
!103 = !DILocation(column: 5, line: 164, scope: !93)
!104 = !DILocation(column: 9, line: 166, scope: !93)
!105 = !DILocation(column: 13, line: 167, scope: !93)
!106 = !DILocation(column: 13, line: 168, scope: !93)
!107 = !DILocation(column: 13, line: 169, scope: !93)
!108 = !DILocation(column: 31, line: 167, scope: !93)
!109 = !DILocation(column: 25, line: 166, scope: !93)
!110 = !DILocation(column: 1, line: 176, scope: !10)
!111 = !DILocation(column: 16, line: 180, scope: !10)
!112 = !DILocation(column: 16, line: 182, scope: !10)
!113 = !DILocation(column: 1, line: 185, scope: !10)
!114 = !DILocation(column: 1, line: 188, scope: !10)
!115 = !DILocation(column: 1, line: 193, scope: !10)
!116 = !DILocation(column: 1, line: 195, scope: !10)
!117 = !DILocation(column: 1, line: 198, scope: !10)
!118 = !DILocation(column: 1, line: 200, scope: !10)
!119 = !DILocation(column: 11, line: 205, scope: !10)
!120 = !DILocation(column: 11, line: 206, scope: !10)
!121 = !DILocation(column: 38, line: 211, scope: !10)
!122 = !DILocation(column: 69, line: 211, scope: !10)
!123 = !DILocation(column: 1, line: 211, scope: !10)
!124 = !DIBasicType(encoding: DW_ATE_signed, name: "i8", size: 8)
!125 = !DIDerivedType(baseType: !124, size: 64, tag: DW_TAG_pointer_type)
!126 = !DILocalVariable(file: !0, line: 1, name: "x", scope: !10, type: !125)
!127 = !DILocation(column: 1, line: 213, scope: !10)
!128 = !DIBasicType(encoding: DW_ATE_signed, name: "i8", size: 8)
!129 = !DILocalVariable(file: !0, line: 1, name: "y", scope: !10, type: !128)
!130 = !DILocation(column: 1, line: 214, scope: !10)
!131 = !DILocation(column: 1, line: 215, scope: !10)
!132 = !DILocation(column: 5, line: 218, scope: !10)
!133 = !DILocation(column: 5, line: 219, scope: !10)
!134 = !DILocation(column: 5, line: 220, scope: !10)
!135 = !DILocation(column: 5, line: 221, scope: !10)
!136 = !DILocation(column: 5, line: 223, scope: !10)
!137 = !DILocation(column: 40, line: 227, scope: !10)
!138 = !DILocation(column: 65, line: 227, scope: !10)
!139 = !DILocation(column: 9, line: 227, scope: !10)
!140 = !DILocation(column: 9, line: 230, scope: !10)
!141 = !DILocation(column: 5, line: 257, scope: !10)
!142 = !DILocation(column: 11, line: 258, scope: !10)
!143 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!144 = !{ !143 }
!145 = !DISubroutineType(types: !144)
!146 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "compare", scope: !0, scopeLine: 1, type: !145, unit: !2)
!147 = !DILocation(column: 12, line: 279, scope: !146)
!148 = !DILocation(column: 15, line: 280, scope: !146)
!149 = !DILocation(column: 14, line: 281, scope: !146)
!150 = !DILocation(column: 7, line: 288, scope: !10)
!151 = !DILocation(column: 1, line: 290, scope: !10)
!152 = !DILocation(column: 1, line: 292, scope: !10)
!153 = !DILocation(column: 1, line: 293, scope: !10)
!154 = !DILocation(column: 1, line: 294, scope: !10)
!155 = !DILocation(column: 1, line: 295, scope: !10)
!156 = !DILocation(column: 1, line: 296, scope: !10)
!157 = !DILocation(column: 1, line: 298, scope: !10)
!158 = !DILocation(column: 1, line: 299, scope: !10)
!159 = !DILocation(column: 1, line: 300, scope: !10)
!160 = !DILocation(column: 1, line: 301, scope: !10)
!161 = !DILocation(column: 1, line: 302, scope: !10)
!162 = !DILocation(column: 1, line: 303, scope: !10)
!163 = !DILocation(column: 1, line: 304, scope: !10)
!164 = !DILocation(column: 1, line: 305, scope: !10)
!165 = !DILocation(column: 1, line: 306, scope: !10)