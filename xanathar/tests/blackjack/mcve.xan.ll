; ModuleID = "tests/blackjack/mcve.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"Card" = type {i2, i64}
%"Card_vtable" = type {}
define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i64
  store i64 0, i64* %".8"
  %".10" = trunc i64 52 to i32
  %".11" = mul i32 %".10", 12
  %".12" = call i8* @"malloc"(i32 %".11")
  %".13" = bitcast i8* %".12" to %"Card"*
  %".14" = alloca %"Card"*
  store %"Card"* %".13", %"Card"** %".14"
  %".16" = alloca [34 x i8]
  store [34 x i8] [i8 100, i8 101, i8 99, i8 107, i8 32, i8 112, i8 111, i8 105, i8 110, i8 116, i8 101, i8 114, i8 32, i8 37, i8 112, i8 32, i8 40, i8 112, i8 116, i8 114, i8 32, i8 116, i8 111, i8 32, i8 112, i8 116, i8 114, i8 32, i8 37, i8 112, i8 41, i8 10, i8 10, i8 0], [34 x i8]* %".16"
  %".18" = getelementptr [34 x i8], [34 x i8]* %".16", i32 0
  %".19" = getelementptr [34 x i8], [34 x i8]* %".18", i64 0
  %".20" = bitcast [34 x i8]* %".19" to i8*
  %".21" = load %"Card"*, %"Card"** %".14"
  %".22" = call i32 (i8*, ...) @"printf"(i8* %".20", %"Card"* %".21", %"Card"** %".14")
  br label %"entry.whileloop0"
entry.whileloop0:
  %".24" = alloca [21 x i8]
  store [21 x i8] [i8 98, i8 101, i8 102, i8 111, i8 114, i8 101, i8 32, i8 36, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 32, i8 64, i8 32, i8 37, i8 112, i8 10, i8 0], [21 x i8]* %".24"
  %".26" = getelementptr [21 x i8], [21 x i8]* %".24", i32 0
  %".27" = getelementptr [21 x i8], [21 x i8]* %".26", i64 0
  %".28" = bitcast [21 x i8]* %".27" to i8*
  %".29" = load i64, i64* %".8"
  %".30" = call i32 (i8*, ...) @"printf"(i8* %".28", i64 %".29", i64* %".8")
  %".31" = alloca [41 x i8]
  store [41 x i8] [i8 98, i8 101, i8 102, i8 111, i8 114, i8 101, i8 32, i8 100, i8 101, i8 99, i8 107, i8 32, i8 112, i8 111, i8 105, i8 110, i8 116, i8 101, i8 114, i8 32, i8 37, i8 112, i8 32, i8 40, i8 112, i8 116, i8 114, i8 32, i8 116, i8 111, i8 32, i8 112, i8 116, i8 114, i8 32, i8 37, i8 112, i8 41, i8 10, i8 10, i8 0], [41 x i8]* %".31"
  %".33" = getelementptr [41 x i8], [41 x i8]* %".31", i32 0
  %".34" = getelementptr [41 x i8], [41 x i8]* %".33", i64 0
  %".35" = bitcast [41 x i8]* %".34" to i8*
  %".36" = load %"Card"*, %"Card"** %".14"
  %".37" = call i32 (i8*, ...) @"printf"(i8* %".35", %"Card"* %".36", %"Card"** %".14")
  %".38" = trunc i64 2 to i2
  %".39" = load %"Card"*, %"Card"** %".14"
  %".40" = load i64, i64* %".8"
  %".41" = getelementptr %"Card", %"Card"* %".39", i64 %".40"
  call void @"Card__init_hook"(%"Card"* %".41", i2 %".38", i64 3)
  %".43" = alloca [20 x i8]
  store [20 x i8] [i8 97, i8 102, i8 116, i8 101, i8 114, i8 32, i8 36, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 32, i8 64, i8 32, i8 37, i8 112, i8 10, i8 0], [20 x i8]* %".43"
  %".45" = getelementptr [20 x i8], [20 x i8]* %".43", i32 0
  %".46" = getelementptr [20 x i8], [20 x i8]* %".45", i64 0
  %".47" = bitcast [20 x i8]* %".46" to i8*
  %".48" = load i64, i64* %".8"
  %".49" = call i32 (i8*, ...) @"printf"(i8* %".47", i64 %".48", i64* %".8")
  %".50" = load i64, i64* %".8"
  %".51" = add i64 %".50", 1
  store i64 %".51", i64* %".8"
  %".53" = alloca [41 x i8]
  store [41 x i8] [i8 97, i8 102, i8 116, i8 101, i8 114, i8 32, i8 100, i8 101, i8 99, i8 107, i8 32, i8 112, i8 111, i8 105, i8 110, i8 116, i8 101, i8 114, i8 32, i8 37, i8 112, i8 32, i8 40, i8 112, i8 116, i8 114, i8 32, i8 116, i8 111, i8 32, i8 112, i8 116, i8 114, i8 32, i8 37, i8 112, i8 41, i8 10, i8 10, i8 10, i8 0], [41 x i8]* %".53"
  %".55" = getelementptr [41 x i8], [41 x i8]* %".53", i32 0
  %".56" = getelementptr [41 x i8], [41 x i8]* %".55", i64 0
  %".57" = bitcast [41 x i8]* %".56" to i8*
  %".58" = load %"Card"*, %"Card"** %".14"
  %".59" = call i32 (i8*, ...) @"printf"(i8* %".57", %"Card"* %".58", %"Card"** %".14")
  %".60" = load i64, i64* %".8"
  %".61" = icmp slt i64 %".60", 10
  br i1 %".61", label %"entry.whileloop0", label %"entry.endwhile0"
entry.endwhile0:
  %".63" = load %"Card"*, %"Card"** %".14"
  call void (...) @"free"(%"Card"* %".63")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"nop"() 

declare i32* @"range"(i32 %".1", i32 %".2") 

declare i8* @"malloc"(i32 %".1") 

declare void @"free"(...) 

declare i32 @"cmp"(i32 %".1", i32 %".2") 

declare void @"inc"(i32* %".1") 

declare void @"dec"(i32* %".1") 

define void @"Card__init_hook"(%"Card"* %".1", i2 %".2", i64 %".3") 
{
entry:
  %".5" = alloca %"Card"*
  store %"Card"* %".1", %"Card"** %".5"
  %".7" = alloca i2
  store i2 %".2", i2* %".7"
  %".9" = alloca i64
  store i64 %".3", i64* %".9"
  %".11" = load i2, i2* %".7"
  %".12" = load %"Card"*, %"Card"** %".5"
  %".13" = getelementptr %"Card", %"Card"* %".12", i32 0, i32 0
  store i2 %".11", i2* %".13"
  %".15" = load i64, i64* %".9"
  %".16" = load %"Card"*, %"Card"** %".5"
  %".17" = getelementptr %"Card", %"Card"* %".16", i32 0, i32 1
  store i64 %".15", i64* %".17"
  ret void
}

@"Card_vtable_value" = internal constant %"Card_vtable" {}
!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }