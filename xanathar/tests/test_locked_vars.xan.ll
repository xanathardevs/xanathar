; ModuleID = "tests/test_locked_vars.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i32
  store i32 8, i32* %".8"
  %".10" = alloca [5 x i8]
  store [5 x i8] [i8 48, i8 37, i8 110, i8 10, i8 0], [5 x i8]* %".10"
  %".12" = getelementptr [5 x i8], [5 x i8]* %".10", i32 0
  %".13" = getelementptr [5 x i8], [5 x i8]* %".12", i32 0
  %".14" = bitcast [5 x i8]* %".13" to i8*
  %".15" = call i32 (i8*, ...) @"printf"(i8* %".14", i32* %".8")
  %".16" = alloca [21 x i8]
  store [21 x i8] [i8 108, i8 111, i8 99, i8 107, i8 101, i8 100, i8 32, i8 118, i8 97, i8 114, i8 32, i8 36, i8 120, i8 32, i8 105, i8 115, i8 32, i8 37, i8 105, i8 10, i8 0], [21 x i8]* %".16"
  %".18" = getelementptr [21 x i8], [21 x i8]* %".16", i32 0
  %".19" = getelementptr [21 x i8], [21 x i8]* %".18", i32 0
  %".20" = bitcast [21 x i8]* %".19" to i8*
  %".21" = load i32, i32* %".8"
  %".22" = call i32 (i8*, ...) @"printf"(i8* %".20", i32 %".21")
  store i32 9, i32* %".8"
  %".24" = alloca [5 x i8]
  store [5 x i8] [i8 48, i8 37, i8 110, i8 10, i8 0], [5 x i8]* %".24"
  %".26" = getelementptr [5 x i8], [5 x i8]* %".24", i32 0
  %".27" = getelementptr [5 x i8], [5 x i8]* %".26", i32 0
  %".28" = bitcast [5 x i8]* %".27" to i8*
  %".29" = call i32 (i8*, ...) @"printf"(i8* %".28", i32* %".8")
  %".30" = alloca i32
  store i32 32, i32* %".30"
  %".32" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 105, i8 10, i8 0], [4 x i8]* %".32"
  %".34" = getelementptr [4 x i8], [4 x i8]* %".32", i32 0
  %".35" = getelementptr [4 x i8], [4 x i8]* %".34", i32 0
  %".36" = bitcast [4 x i8]* %".35" to i8*
  %".37" = load i32, i32* %".30"
  %".38" = call i32 (i8*, ...) @"printf"(i8* %".36", i32 %".37")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }