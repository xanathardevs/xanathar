; ModuleID = "tests/ctfs/echoooooo.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") !dbg !10
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %".6" = alloca [29 x i8]
  store [29 x i8] [i8 112, i8 99, i8 116, i8 102, i8 123, i8 101, i8 99, i8 104, i8 111, i8 111, i8 111, i8 111, i8 111, i8 111, i8 111, i8 111, i8 45, i8 99, i8 98, i8 51, i8 57, i8 99, i8 57, i8 51, i8 48, i8 52, i8 102, i8 125, i8 0], [29 x i8]* %".6"
  %".8" = getelementptr [29 x i8], [29 x i8]* %".6", i32 0
  %".9" = getelementptr [29 x i8], [29 x i8]* %".8", i32 0
  %".10" = bitcast [29 x i8]* %".9" to i8*
  %".11" = alloca i8*
  store i8* %".10", i8** %".11"
  %".13" = mul i32 20, 1
  %"called_malloc" = call i8* @"malloc"(i32 %".13"), !dbg !11
  %".14" = alloca i8*
  store i8* %"called_malloc", i8** %".14"
  %"loaded_$x" = load i8*, i8** %".14"
  %"called_get" = call i8* @"get"(i8* %"loaded_$x", i32 20), !dbg !12
  %"loaded_$x.1" = load i8*, i8** %".14"
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %"loaded_$x.1"), !dbg !13
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"llvm.dbg.declare"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

declare void @"llvm.dbg.value"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
declare external i8* @"get"(i8* %".1", i32 %".2") 

declare external i32 @"get_int"() 

declare external i32 @"atoi"(i8* %".1") 

declare external i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

declare external void @"nop"() 

declare external i32* @"range"(i32 %".1", i32 %".2") 

declare external i8* @"malloc"(i32 %".1") 

declare external void @"free"(...) 

declare external i32 @"cmp"(i32 %".1", i32 %".2") 

declare external void @"inc"(i32* %".1") 

declare external void @"dec"(i32* %".1") 

!llvm.dbg.cu = !{ !2 }
!llvm.module.flags = !{ !3, !4, !5 }
!llvm.ident = !{ !6 }
!gen-by = !{ !14 }
!0 = !DIFile(directory: "/home/proc-daemon/Dropbox/Xanathar/xanathar/tests/ctfs", filename: "echoooooo.xan")
!1 = !{  }
!2 = distinct !DICompileUnit(emissionKind: FullDebug, enums: !1, file: !0, isOptimized: false, language: DW_LANG_Python, producer: "Xanathar v. a0.0.1", runtimeVersion: 0)
!3 = !{ i32 2, !"Dwarf Version", i32 4 }
!4 = !{ i32 2, !"Debug Info Version", i32 3 }
!5 = !{ i32 1, !"wchar_size", i32 4 }
!6 = !{ !"Xanathar a0.0.1" }
!7 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!8 = !{ !7 }
!9 = !DISubroutineType(types: !8)
!10 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !9, unit: !2, variables: !1)
!11 = !DILocation(column: 11, line: 6, scope: !10)
!12 = !DILocation(column: 1, line: 7, scope: !10)
!13 = !DILocation(column: 1, line: 9, scope: !10)
!14 = !{ !"Xanathar" }