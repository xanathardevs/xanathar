; ModuleID = "tests/test_classes_advanced.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"Arithmetic" = type {}
%"Foo" = type {i32}
%"AddSub" = type {}
define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i32
  %".9" = call i32 @"get_int"()
  store i32 %".9", i32* %".8"
  %".11" = alloca i32
  %".12" = call i32 @"get_int"()
  store i32 %".12", i32* %".11"
  %".14" = alloca %"Foo"
  %".15" = load i32, i32* %".8"
  call void @"Foo__init_hook"(%"Foo"* %".14", i32 %".15")
  call void @"Foo_print"(%"Foo"* %".14")
  %".18" = alloca [14 x i8]
  store [14 x i8] [i8 37, i8 105, i8 32, i8 43, i8 32, i8 37, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 10, i8 0], [14 x i8]* %".18"
  %".20" = getelementptr [14 x i8], [14 x i8]* %".18", i32 0
  %".21" = getelementptr [14 x i8], [14 x i8]* %".20", i32 0
  %".22" = bitcast [14 x i8]* %".21" to i8*
  %".23" = load i32, i32* %".8"
  %".24" = load i32, i32* %".11"
  %".25" = load i32, i32* %".8"
  %".26" = load i32, i32* %".11"
  %".27" = call i32 @"Arithmetic_add"(i32 %".25", i32 %".26")
  %".28" = call i32 (i8*, ...) @"printf"(i8* %".22", i32 %".23", i32 %".24", i32 %".27")
  %".29" = alloca [14 x i8]
  store [14 x i8] [i8 37, i8 105, i8 32, i8 45, i8 32, i8 37, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 10, i8 0], [14 x i8]* %".29"
  %".31" = getelementptr [14 x i8], [14 x i8]* %".29", i32 0
  %".32" = getelementptr [14 x i8], [14 x i8]* %".31", i32 0
  %".33" = bitcast [14 x i8]* %".32" to i8*
  %".34" = load i32, i32* %".8"
  %".35" = load i32, i32* %".11"
  %".36" = load i32, i32* %".8"
  %".37" = load i32, i32* %".11"
  %".38" = call i32 @"Arithmetic_sub"(i32 %".36", i32 %".37")
  %".39" = call i32 (i8*, ...) @"printf"(i8* %".33", i32 %".34", i32 %".35", i32 %".38")
  %".40" = alloca [14 x i8]
  store [14 x i8] [i8 37, i8 105, i8 32, i8 42, i8 32, i8 37, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 10, i8 0], [14 x i8]* %".40"
  %".42" = getelementptr [14 x i8], [14 x i8]* %".40", i32 0
  %".43" = getelementptr [14 x i8], [14 x i8]* %".42", i32 0
  %".44" = bitcast [14 x i8]* %".43" to i8*
  %".45" = load i32, i32* %".8"
  %".46" = load i32, i32* %".11"
  %".47" = load i32, i32* %".8"
  %".48" = load i32, i32* %".11"
  %".49" = call i32 @"Arithmetic_mul"(i32 %".47", i32 %".48")
  %".50" = call i32 (i8*, ...) @"printf"(i8* %".44", i32 %".45", i32 %".46", i32 %".49")
  %".51" = alloca [14 x i8]
  store [14 x i8] [i8 37, i8 105, i8 32, i8 47, i8 32, i8 37, i8 105, i8 32, i8 61, i8 32, i8 37, i8 102, i8 10, i8 0], [14 x i8]* %".51"
  %".53" = getelementptr [14 x i8], [14 x i8]* %".51", i32 0
  %".54" = getelementptr [14 x i8], [14 x i8]* %".53", i32 0
  %".55" = bitcast [14 x i8]* %".54" to i8*
  %".56" = load i32, i32* %".8"
  %".57" = load i32, i32* %".11"
  %".58" = load i32, i32* %".8"
  %".59" = sitofp i32 %".58" to double
  %".60" = load i32, i32* %".11"
  %".61" = sitofp i32 %".60" to double
  %".62" = call double @"Arithmetic_div"(double %".59", double %".61")
  %".63" = call i32 (i8*, ...) @"printf"(i8* %".55", i32 %".56", i32 %".57", double %".62")
  %".64" = alloca [15 x i8]
  store [15 x i8] [i8 37, i8 105, i8 32, i8 37, i8 37, i8 32, i8 37, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 10, i8 0], [15 x i8]* %".64"
  %".66" = getelementptr [15 x i8], [15 x i8]* %".64", i32 0
  %".67" = getelementptr [15 x i8], [15 x i8]* %".66", i32 0
  %".68" = bitcast [15 x i8]* %".67" to i8*
  %".69" = load i32, i32* %".8"
  %".70" = load i32, i32* %".11"
  %".71" = load i32, i32* %".8"
  %".72" = load i32, i32* %".11"
  %".73" = call i32 @"Arithmetic_mod"(i32 %".71", i32 %".72")
  %".74" = call i32 (i8*, ...) @"printf"(i8* %".68", i32 %".69", i32 %".70", i32 %".73")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

define void @"Arithmetic__init_hook"() 
{
entry:
  ret void
}

define i32 @"Arithmetic_add"(i32 %".1", i32 %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".4"
  %".9" = load i32, i32* %".6"
  %".10" = add i32 %".8", %".9"
  ret i32 %".10"
}

define i32 @"Arithmetic_sub"(i32 %".1", i32 %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".4"
  %".9" = load i32, i32* %".6"
  %".10" = sub i32 %".8", %".9"
  ret i32 %".10"
}

define i32 @"Arithmetic_mul"(i32 %".1", i32 %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".4"
  %".9" = load i32, i32* %".6"
  %".10" = mul i32 %".8", %".9"
  ret i32 %".10"
}

define double @"Arithmetic_div"(double %".1", double %".2") 
{
entry:
  %".4" = alloca double
  store double %".1", double* %".4"
  %".6" = alloca double
  store double %".2", double* %".6"
  %".8" = load double, double* %".4"
  %".9" = load double, double* %".6"
  %".10" = fdiv double %".8", %".9"
  ret double %".10"
}

define i32 @"Arithmetic_mod"(i32 %".1", i32 %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".4"
  %".9" = load i32, i32* %".6"
  %".10" = srem i32 %".8", %".9"
  ret i32 %".10"
}

define void @"Foo__init_hook"(%"Foo"* %".1", i32 %".2") 
{
entry:
  %".4" = alloca %"Foo"*
  store %"Foo"* %".1", %"Foo"** %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".6"
  %".9" = load %"Foo"*, %"Foo"** %".4"
  %".10" = getelementptr %"Foo", %"Foo"* %".9", i32 0, i32 0
  store i32 %".8", i32* %".10"
  ret void
}

define void @"Foo_print"(%"Foo"* %".1") 
{
entry:
  %".3" = alloca %"Foo"*
  store %"Foo"* %".1", %"Foo"** %".3"
  %".5" = alloca [108 x i8]
  store [108 x i8] [i8 84, i8 104, i8 105, i8 115, i8 32, i8 105, i8 115, i8 32, i8 70, i8 111, i8 111, i8 44, i8 32, i8 114, i8 101, i8 112, i8 111, i8 114, i8 116, i8 105, i8 110, i8 39, i8 32, i8 105, i8 110, i8 33, i8 32, i8 87, i8 101, i8 32, i8 103, i8 111, i8 116, i8 32, i8 97, i8 32, i8 36, i8 97, i8 32, i8 119, i8 105, i8 116, i8 104, i8 32, i8 118, i8 97, i8 108, i8 117, i8 101, i8 32, i8 37, i8 105, i8 44, i8 32, i8 97, i8 110, i8 100, i8 32, i8 119, i8 101, i8 39, i8 114, i8 101, i8 32, i8 108, i8 111, i8 99, i8 97, i8 116, i8 101, i8 100, i8 32, i8 97, i8 116, i8 32, i8 108, i8 97, i8 116, i8 105, i8 116, i8 117, i8 100, i8 101, i8 32, i8 98, i8 121, i8 32, i8 108, i8 111, i8 110, i8 103, i8 105, i8 116, i8 117, i8 100, i8 101, i8 32, i8 37, i8 112, i8 46, i8 32, i8 79, i8 118, i8 101, i8 114, i8 46, i8 10, i8 0], [108 x i8]* %".5"
  %".7" = getelementptr [108 x i8], [108 x i8]* %".5", i32 0
  %".8" = getelementptr [108 x i8], [108 x i8]* %".7", i32 0
  %".9" = bitcast [108 x i8]* %".8" to i8*
  %".10" = load %"Foo"*, %"Foo"** %".3"
  %".11" = getelementptr %"Foo", %"Foo"* %".10", i32 0, i32 0
  %".12" = load i32, i32* %".11"
  %".13" = load %"Foo"*, %"Foo"** %".3"
  %".14" = call i32 (i8*, ...) @"printf"(i8* %".9", i32 %".12", %"Foo"* %".13")
  ret void
}

define void @"AddSub__init_hook"() 
{
entry:
  ret void
}

define i32 @"get_int"() 
{
entry:
  %".2" = alloca i8*
  %".3" = alloca i8
  %".4" = trunc i32 3 to i8
  store i8 %".4", i8* %".3"
  store i8* %".3", i8** %".2"
  %".7" = load i8*, i8** %".2"
  %".8" = call i8* @"get"(i8* %".7", i32 20)
  %".9" = call i32 @"atoi"(i8* %".3")
  ret i32 %".9"
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }