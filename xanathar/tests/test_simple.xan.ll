; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca i8
  %".7" = trunc i32 65 to i8
  store i8 %".7", i8* %".6"
  %".9" = load i8, i8* %".6"
  %".10" = call i32 (i8*, ...) @"printf"(i8* %".2", i8 %".9")
  %".11" = load i8, i8* %".6"
  %".12" = call i32 (i8*, ...) @"printf"(i8* %".3", i8 %".11")
  %".13" = load i8, i8* %".6"
  %".14" = call i32 (i8*, ...) @"printf"(i8* %".5", i8 %".13")
  %".15" = alloca [88 x i8]
  store [88 x i8] [i8 78, i8 101, i8 118, i8 101, i8 114, i8 32, i8 103, i8 111, i8 110, i8 110, i8 97, i8 32, i8 103, i8 105, i8 118, i8 101, i8 32, i8 121, i8 111, i8 117, i8 32, i8 117, i8 112, i8 10, i8 78, i8 101, i8 118, i8 101, i8 114, i8 32, i8 103, i8 111, i8 110, i8 110, i8 97, i8 32, i8 108, i8 101, i8 116, i8 32, i8 121, i8 111, i8 117, i8 32, i8 100, i8 111, i8 119, i8 110, i8 10, i8 78, i8 101, i8 118, i8 101, i8 114, i8 32, i8 103, i8 111, i8 110, i8 110, i8 97, i8 32, i8 114, i8 117, i8 110, i8 32, i8 97, i8 114, i8 111, i8 117, i8 110, i8 100, i8 32, i8 97, i8 110, i8 100, i8 32, i8 100, i8 101, i8 115, i8 101, i8 114, i8 116, i8 32, i8 121, i8 111, i8 117, i8 32, i8 0], [88 x i8]* %".15"
  %".17" = getelementptr [88 x i8], [88 x i8]* %".15", i32 0
  %".18" = call i32 (i8*, ...) @"printf"(i8* %".4", [88 x i8]* %".17")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"