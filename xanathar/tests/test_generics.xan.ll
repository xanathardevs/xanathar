; ModuleID = "tests/test_generics.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca [14 x i8]
  store [14 x i8] [i8 37, i8 105, i8 32, i8 43, i8 32, i8 37, i8 105, i8 32, i8 61, i8 32, i8 37, i8 105, i8 10, i8 0], [14 x i8]* %".8"
  %".10" = getelementptr [14 x i8], [14 x i8]* %".8", i32 0
  %".11" = getelementptr [14 x i8], [14 x i8]* %".10", i32 0
  %".12" = bitcast [14 x i8]* %".11" to i8*
  %".13" = call i32 @"f_int32int32"(i32 2, i32 3)
  %".14" = call i32 (i8*, ...) @"printf"(i8* %".12", i32 2, i32 3, i32 %".13")
  %".15" = alloca [14 x i8]
  store [14 x i8] [i8 37, i8 102, i8 32, i8 43, i8 32, i8 37, i8 102, i8 32, i8 61, i8 32, i8 37, i8 102, i8 10, i8 0], [14 x i8]* %".15"
  %".17" = getelementptr [14 x i8], [14 x i8]* %".15", i32 0
  %".18" = getelementptr [14 x i8], [14 x i8]* %".17", i32 0
  %".19" = bitcast [14 x i8]* %".18" to i8*
  %".20" = call double @"f_doubledouble"(double 0x4000000000000000, double 0x4008000000000000)
  %".21" = call i32 (i8*, ...) @"printf"(i8* %".19", double 0x4000000000000000, double 0x4008000000000000, double %".20")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

define i32 @"f_int32int32"(i32 %".1", i32 %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".4"
  %".9" = load i32, i32* %".6"
  %".10" = add i32 %".8", %".9"
  ret i32 %".10"
}

define double @"f_doubledouble"(double %".1", double %".2") 
{
entry:
  %".4" = alloca double
  store double %".1", double* %".4"
  %".6" = alloca double
  store double %".2", double* %".6"
  %".8" = load double, double* %".4"
  %".9" = load double, double* %".6"
  %".10" = fadd double %".8", %".9"
  ret double %".10"
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }