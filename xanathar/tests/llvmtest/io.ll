; ModuleID = io.ll
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%FILE = type opaque
declare %FILE* @fdopen(i32, i8*)
@r = constant [2 x i8] c"r\00"
declare i8* @"fgets"(i8*, i32, %FILE*)

define void @"main"()
{
  %".1" = bitcast [2 x i8]* @r to i8*
  %stdin = call %FILE* @fdopen(i32 0, i8* %".1")
  %".2" = alloca [20 x i8]
  %".3" = getelementptr [20 x i8], [20 x i8]* %".2", i32 0
  %".4" = bitcast [20 x i8]* %".3" to i8*
  call i8* @"fgets"(i8* %".4", i32 20, %FILE* %stdin)



  ret void
}