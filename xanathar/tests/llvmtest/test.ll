; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = trunc i64 214 to i32
  call void (i32) @"srand"(i32 %".2")
  %".3" = call i32 () @"rand"()
  %".4" = bitcast [5 x i8]* @"istr" to i8*
  %".5" = call i32 (i8*, ...) @"printf"(i8* %".4", i32 %".3")

  ; INPUT
  %".6" = alloca [20 x i8]
  %".7" = getelementptr [20 x i8], [20 x i8]* %".6", i32 0
  %".8" = bitcast [20 x i8]* %".7" to i8*
  call i8* (i8*) @"gets"(i8* %".8")

  %".9" = bitcast [3 x i8]* @"sstr" to i8*
  call i32 (i8*, ...) @"printf"(i8* %".9", i8* %".8")
  ret void
}

declare i32 @"printf"(i8* %".1", ...)
declare i8* @"gets"(i8*)
declare i32 @"rand"()
declare void @"srand"(i32)

@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"sstr" = internal constant [3 x i8] c"%s\00"