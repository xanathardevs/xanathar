; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca i32
  %".7" = call i32 @"time"(i32 0)
  store i32 %".7", i32* %".6"
  %".9" = load i32, i32* %".6"
  %".10" = call i32 (i8*, ...) @"printf"(i8* %".2", i32 %".9")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"fdopen_fmt" = internal constant [2 x i8] c"r\00"
@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
declare i32 @"time"(i32 %".1") 

declare i8* @"ctime"(i32* %".1") 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }