; ModuleID = "tests/test_ops.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"i" = type {i32}
%"i_vtable" = type {}
define void @"main"(i32 %".1", i8** %".2") !dbg !10
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %"$q" = alloca %"i"
  call void @"llvm.dbg.declare"(metadata %"i"* %"$q", metadata !43, metadata !25), !dbg !44
  call void @"i__init_hook"(%"i"* %"$q", i32 2), !dbg !45
  %"loaded_$q" = load %"i", %"i"* %"$q"
  %"i + i32" = call %"i" @"i_hook_+"(%"i"* %"$q", i32 2), !dbg !46
  %".7" = alloca %"i"
  store %"i" %"i + i32", %"i"* %".7"
  %".9" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 105, i8 10, i8 0], [4 x i8]* %".9"
  %".11" = getelementptr [4 x i8], [4 x i8]* %".9", i32 0
  %".12" = bitcast [4 x i8]* %".11" to i8*
  %".13" = getelementptr %"i", %"i"* %".7", i32 0, i32 0
  %".14" = load i32, i32* %".13"
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".12", i32 %".14"), !dbg !47
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"llvm.dbg.declare"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

declare void @"llvm.dbg.value"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
define void @"i__init_hook"(%"i"* %".1", i32 %".2") noinline!dbg !14
{
entry:
  %"$self" = alloca %"i"*
  store %"i"* %".1", %"i"** %"$self"
  %"$i" = alloca i32
  store i32 %".2", i32* %"$i"
  %"loaded_$self" = load %"i"*, %"i"** %"$self"
  %".6" = getelementptr %"i", %"i"* %"loaded_$self", i32 0, i32 0
  %"loaded_$i" = load i32, i32* %"$i"
  store i32 %"loaded_$i", i32* %".6"
  ret void
}

define %"i" @"i_hook_+"(%"i"* %".1", i32 %".2") noinline!dbg !20
{
entry:
  %"$self" = alloca %"i"*
  store %"i"* %".1", %"i"** %"$self"
  %"$i" = alloca i32
  store i32 %".2", i32* %"$i"
  %"$t" = alloca %"i"
  call void @"llvm.dbg.declare"(metadata %"i"* %"$t", metadata !24, metadata !25), !dbg !26
  %"loaded_$self" = load %"i"*, %"i"** %"$self"
  %".7" = getelementptr %"i", %"i"* %"loaded_$self", i32 0, i32 0
  %".8" = load i32, i32* %".7"
  %"loaded_$i" = load i32, i32* %"$i"
  %".9" = add i32 %".8", %"loaded_$i"
  call void @"i__init_hook"(%"i"* %"$t", i32 %".9"), !dbg !27
  %"loaded_$t" = load %"i", %"i"* %"$t"
  ret %"i" %"loaded_$t"
}

@"i_vtable_value" = internal constant %"i_vtable" {}
define %"i" @"i_hook_-"(%"i"* %".1", i32 %".2") noinline!dbg !33
{
entry:
  %"$self" = alloca %"i"*
  store %"i"* %".1", %"i"** %"$self"
  %"$i" = alloca i32
  store i32 %".2", i32* %"$i"
  %"$t" = alloca %"i"
  call void @"llvm.dbg.declare"(metadata %"i"* %"$t", metadata !37, metadata !25), !dbg !38
  %"loaded_$self" = load %"i"*, %"i"** %"$self"
  %".7" = getelementptr %"i", %"i"* %"loaded_$self", i32 0, i32 0
  %".8" = load i32, i32* %".7"
  %"loaded_$i" = load i32, i32* %"$i"
  %".9" = sub i32 %".8", %"loaded_$i"
  call void @"i__init_hook"(%"i"* %"$t", i32 %".9"), !dbg !39
  %"loaded_$t" = load %"i", %"i"* %"$t"
  ret %"i" %"loaded_$t"
}

!llvm.dbg.cu = !{ !2 }
!llvm.module.flags = !{ !3, !4, !5 }
!llvm.ident = !{ !6 }
!gen-by = !{ !48 }
!0 = !DIFile(directory: "/home/proc-daemon/Dropbox/Xanathar/xanathar/tests", filename: "test_ops.xan")
!1 = !{  }
!2 = distinct !DICompileUnit(emissionKind: FullDebug, enums: !1, file: !0, isOptimized: false, language: DW_LANG_Python, producer: "Xanathar v. a0.0.1", runtimeVersion: 0)
!3 = !{ i32 2, !"Dwarf Version", i32 4 }
!4 = !{ i32 2, !"Debug Info Version", i32 3 }
!5 = !{ i32 1, !"wchar_size", i32 4 }
!6 = !{ !"Xanathar a0.0.1" }
!7 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!8 = !{ !7 }
!9 = !DISubroutineType(types: !8)
!10 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !9, unit: !2, variables: !1)
!11 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!12 = !{ !11 }
!13 = !DISubroutineType(types: !12)
!14 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "i__init_hook", scope: !0, scopeLine: 1, type: !13, unit: !2, variables: !1)
!15 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!16 = !{ !15 }
!17 = !DICompositeType(elements: !16, file: !0, line: 1, size: 32, tag: DW_TAG_structure_type)
!18 = !{ !17 }
!19 = !DISubroutineType(types: !18)
!20 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "i_hook_+", scope: !0, scopeLine: 1, type: !19, unit: !2, variables: !1)
!21 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!22 = !{ !21 }
!23 = !DICompositeType(elements: !22, file: !0, line: 1, size: 32, tag: DW_TAG_structure_type)
!24 = !DILocalVariable(file: !0, line: 1, name: "t", scope: !20, type: !23)
!25 = !DIExpression()
!26 = !DILocation(column: 9, line: 8, scope: !20)
!27 = !DILocation(column: 9, line: 9, scope: !20)
!28 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!29 = !{ !28 }
!30 = !DICompositeType(elements: !29, file: !0, line: 1, size: 32, tag: DW_TAG_structure_type)
!31 = !{ !30 }
!32 = !DISubroutineType(types: !31)
!33 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "i_hook_-", scope: !0, scopeLine: 1, type: !32, unit: !2, variables: !1)
!34 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!35 = !{ !34 }
!36 = !DICompositeType(elements: !35, file: !0, line: 1, size: 32, tag: DW_TAG_structure_type)
!37 = !DILocalVariable(file: !0, line: 1, name: "t", scope: !33, type: !36)
!38 = !DILocation(column: 9, line: 16, scope: !33)
!39 = !DILocation(column: 9, line: 17, scope: !33)
!40 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 32)
!41 = !{ !40 }
!42 = !DICompositeType(elements: !41, file: !0, line: 1, size: 32, tag: DW_TAG_structure_type)
!43 = !DILocalVariable(file: !0, line: 1, name: "q", scope: !10, type: !42)
!44 = !DILocation(column: 1, line: 22, scope: !10)
!45 = !DILocation(column: 1, line: 23, scope: !10)
!46 = !DILocation(column: 10, line: 24, scope: !10)
!47 = !DILocation(column: 1, line: 25, scope: !10)
!48 = !{ !"Xanathar" }