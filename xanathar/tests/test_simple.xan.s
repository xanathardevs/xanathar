	.text
	.file	"test_simple.xan.ll"
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	movb	$41, 7(%rsp)
	leaq	7(%rsp), %rsi
	movl	$sstr, %edi
	xorl	%eax, %eax
	callq	printf
	popq	%rax
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
                                        # -- End function
	.type	istr,@object            # @istr
	.section	.rodata,"a",@progbits
istr:
	.asciz	"%i \n"
	.size	istr, 5

	.type	xstr,@object            # @xstr
xstr:
	.asciz	"%x \n"
	.size	xstr, 5

	.type	sstr,@object            # @sstr
sstr:
	.asciz	"%s \n"
	.size	sstr, 5


	.section	".note.GNU-stack","",@progbits
