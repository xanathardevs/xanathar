; ModuleID = "tests/gtkp.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"GtkApplication" = type opaque
%"GtkWidget" = type opaque
%"GtkWindow" = type opaque
%"gpointer" = type opaque
define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca %"GtkApplication"*
  %".9" = alloca i32
  %".10" = alloca [16 x i8]
  store [16 x i8] [i8 111, i8 114, i8 103, i8 46, i8 103, i8 116, i8 107, i8 46, i8 101, i8 120, i8 97, i8 109, i8 112, i8 108, i8 101, i8 0], [16 x i8]* %".10"
  %".12" = getelementptr [16 x i8], [16 x i8]* %".10", i32 0
  %".13" = getelementptr [16 x i8], [16 x i8]* %".12", i32 0
  %".14" = bitcast [16 x i8]* %".13" to i8*
  %".15" = load i32, i32* @"G_APPLICATION_FLAGS_NONE"
  %".16" = call %"GtkApplication"* @"xtk_application_new"(i8* %".14", i32 %".15")
  store %"GtkApplication"* %".16", %"GtkApplication"** %".8"
  %".18" = load %"GtkApplication"*, %"GtkApplication"** %".8"
  %".19" = alloca [9 x i8]
  store [9 x i8] [i8 97, i8 99, i8 116, i8 105, i8 118, i8 97, i8 116, i8 101, i8 0], [9 x i8]* %".19"
  %".21" = getelementptr [9 x i8], [9 x i8]* %".19", i32 0
  %".22" = getelementptr [9 x i8], [9 x i8]* %".21", i32 0
  %".23" = bitcast [9 x i8]* %".22" to i8*
  %".24" = alloca i32
  store i32 0, i32* %".24"
  call void (...) @"x_signal_connect"(%"GtkApplication"* %".18", i8* %".23", void (%"GtkApplication"*, i8*)* @"activate", i32* %".24")
  %".27" = load %"GtkApplication"*, %"GtkApplication"** %".8"
  %".28" = load i32, i32* %".4"
  %".29" = load i8**, i8*** %".6"
  %".30" = call i32 @"x_application_run"(%"GtkApplication"* %".27", i32 %".28", i8** %".29")
  store i32 %".30", i32* %".9"
  %".32" = load %"GtkApplication"*, %"GtkApplication"** %".8"
  call void (...) @"x_object_unref"(%"GtkApplication"* %".32")
  %".34" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 105, i8 10, i8 0], [4 x i8]* %".34"
  %".36" = getelementptr [4 x i8], [4 x i8]* %".34", i32 0
  %".37" = getelementptr [4 x i8], [4 x i8]* %".36", i32 0
  %".38" = bitcast [4 x i8]* %".37" to i8*
  %".39" = load i32, i32* %".9"
  %".40" = call i32 (i8*, ...) @"printf"(i8* %".38", i32 %".39")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare external %"GtkWidget"* @"xtk_application_window_new"(%"GtkApplication"* %".1") 

declare external void @"xtk_window_set_title"(%"GtkWindow"* %".1", i8* %".2") 

declare external void @"xtk_window_set_default_size"(%"GtkWindow"* %".1", i32 %".2", i32 %".3") 

declare external void @"xtk_widget_show_all"(%"GtkWidget"* %".1") 

declare external %"GtkApplication"* @"xtk_application_new"(i8* %".1", i32 %".2") 

declare external void @"x_signal_connect"(...) 

declare external i32 @"x_application_run"(%"GtkApplication"* %".1", i32 %".2", i8** %".3") 

declare external void @"x_object_unref"(...) 

@"G_APPLICATION_FLAGS_NONE" = internal constant i32 0
define void @"activate"(%"GtkApplication"* %".1", i8* %".2") 
{
entry:
  %".4" = alloca %"GtkApplication"*
  store %"GtkApplication"* %".1", %"GtkApplication"** %".4"
  %".6" = alloca i8*
  store i8* %".2", i8** %".6"
  %".8" = alloca %"GtkWidget"*
  %".9" = load %"GtkApplication"*, %"GtkApplication"** %".4"
  %".10" = call %"GtkWidget"* @"xtk_application_window_new"(%"GtkApplication"* %".9")
  store %"GtkWidget"* %".10", %"GtkWidget"** %".8"
  %".12" = load %"GtkWidget"*, %"GtkWidget"** %".8"
  %".13" = bitcast %"GtkWidget"* %".12" to %"GtkWindow"*
  %".14" = alloca [7 x i8]
  store [7 x i8] [i8 87, i8 105, i8 110, i8 100, i8 111, i8 119, i8 0], [7 x i8]* %".14"
  %".16" = getelementptr [7 x i8], [7 x i8]* %".14", i32 0
  %".17" = getelementptr [7 x i8], [7 x i8]* %".16", i32 0
  %".18" = bitcast [7 x i8]* %".17" to i8*
  call void @"xtk_window_set_title"(%"GtkWindow"* %".13", i8* %".18")
  %".20" = load %"GtkWidget"*, %"GtkWidget"** %".8"
  %".21" = bitcast %"GtkWidget"* %".20" to %"GtkWindow"*
  call void @"xtk_window_set_default_size"(%"GtkWindow"* %".21", i32 200, i32 200)
  %".23" = load %"GtkWidget"*, %"GtkWidget"** %".8"
  call void @"xtk_widget_show_all"(%"GtkWidget"* %".23")
  ret void
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }