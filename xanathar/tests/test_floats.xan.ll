; ModuleID = "tests/test_floats.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %"$x" = alloca double
  call void @"llvm.dbg.declare"(metadata double* %"$x", metadata !12, metadata !13), !dbg !14
  %"$y" = alloca double
  call void @"llvm.dbg.declare"(metadata double* %"$y", metadata !20, metadata !13), !dbg !21
  store double 0x4000000000000000, double* %"$x"
  store double 0x4000000000000000, double* %"$y"
  %".10" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 102, i8 10, i8 0], [4 x i8]* %".10"
  %".12" = getelementptr [4 x i8], [4 x i8]* %".10", i32 0
  %".13" = getelementptr [4 x i8], [4 x i8]* %".12", i32 0
  %".14" = bitcast [4 x i8]* %".13" to i8*
  %"loaded_$x" = load double, double* %"$x"
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".14", double %"loaded_$x")
  %".15" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 102, i8 10, i8 0], [4 x i8]* %".15"
  %".17" = getelementptr [4 x i8], [4 x i8]* %".15", i32 0
  %".18" = getelementptr [4 x i8], [4 x i8]* %".17", i32 0
  %".19" = bitcast [4 x i8]* %".18" to i8*
  %"loaded_$y" = load double, double* %"$y"
  %"called_printf.1" = call i32 (i8*, ...) @"printf"(i8* %".19", double %"loaded_$y")
  %".20" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 102, i8 10, i8 0], [4 x i8]* %".20"
  %".22" = getelementptr [4 x i8], [4 x i8]* %".20", i32 0
  %".23" = getelementptr [4 x i8], [4 x i8]* %".22", i32 0
  %".24" = bitcast [4 x i8]* %".23" to i8*
  %"loaded_$x.1" = load double, double* %"$x"
  %"loaded_$y.1" = load double, double* %"$y"
  %".25" = fadd double %"loaded_$x.1", %"loaded_$y.1"
  %"called_printf.2" = call i32 (i8*, ...) @"printf"(i8* %".24", double %".25")
  %".26" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 102, i8 10, i8 0], [4 x i8]* %".26"
  %".28" = getelementptr [4 x i8], [4 x i8]* %".26", i32 0
  %".29" = getelementptr [4 x i8], [4 x i8]* %".28", i32 0
  %".30" = bitcast [4 x i8]* %".29" to i8*
  %"loaded_$x.2" = load double, double* %"$x"
  %".31" = fadd double %"loaded_$x.2", 0x3ff0000000000000
  %"called_printf.3" = call i32 (i8*, ...) @"printf"(i8* %".30", double %".31")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"llvm.dbg.declare"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

declare void @"llvm.dbg.value"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
!llvm.dbg.cu = !{ !2 }
!llvm.module.flags = !{ !3, !4, !5 }
!llvm.ident = !{ !6 }
!gen-by = !{ !22 }
!0 = !DIFile(directory: "/home/proc-daemon/Dropbox/Xanathar/xanathar/tests", filename: "test_floats.xan")
!1 = !{  }
!2 = distinct !DICompileUnit(emissionKind: FullDebug, enums: !1, file: !0, isOptimized: false, language: DW_LANG_Python, producer: "Xanathar v. a0.0.1", runtimeVersion: 0)
!3 = !{ i32 2, !"Dwarf Version", i32 4 }
!4 = !{ i32 2, !"Debug Info Version", i32 3 }
!5 = !{ i32 1, !"wchar_size", i32 4 }
!6 = !{ !"Xanathar a0.0.1" }
!7 = !DIBasicType(encoding: DW_ATE_signed, name: "double", size: 8)
!8 = !DIBasicType(encoding: DW_ATE_signed, name: "void (i32, i8**)*", size: 8)
!9 = !{ !8 }
!10 = !DISubroutineType(types: !9)
!11 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !10, unit: !2, variables: !1)
!12 = !DILocalVariable(file: !0, line: 1, name: "x", scope: !11, type: !7)
!13 = !DIExpression()
!14 = !DILocation(column: 1, line: 1, scope: !11)
!15 = !DIBasicType(encoding: DW_ATE_signed, name: "double", size: 8)
!16 = !DIBasicType(encoding: DW_ATE_signed, name: "void (i32, i8**)*", size: 8)
!17 = !{ !16 }
!18 = !DISubroutineType(types: !17)
!19 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !18, unit: !2, variables: !1)
!20 = !DILocalVariable(file: !0, line: 1, name: "y", scope: !19, type: !15)
!21 = !DILocation(column: 1, line: 1, scope: !19)
!22 = !{ !"Xanathar" }