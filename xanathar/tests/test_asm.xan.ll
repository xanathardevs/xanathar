; ModuleID = "tests/test_asm.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  call void asm sideeffect "", "{rip}"
(i64 32)
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }