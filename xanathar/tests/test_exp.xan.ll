; ModuleID = "tests/test_exp.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca [3 x i8]
  store [3 x i8] [i8 37, i8 105, i8 0], [3 x i8]* %".8"
  %".10" = getelementptr [3 x i8], [3 x i8]* %".8", i32 0
  %".11" = getelementptr [3 x i8], [3 x i8]* %".10", i32 0
  %".12" = bitcast [3 x i8]* %".11" to i8*
  %".13" = add i32 2, 2
  %".14" = mul i32 3, 3
  %".15" = mul i32 %".13", %".14"
  %".16" = mul i32 4, 7
  %".17" = sdiv i32 %".15", %".16"
  %".18" = call i32 (i8*, ...) @"printf"(i8* %".12", i32 %".17")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }