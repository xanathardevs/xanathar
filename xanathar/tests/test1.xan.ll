; ModuleID = "tests/test1.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i32
  %".9" = alloca [64 x i64]
  %".10" = alloca i32
  %".11" = alloca [41 x i8]
  store [41 x i8] [i8 50, i8 52, i8 55, i8 49, i8 57, i8 50, i8 53, i8 52, i8 32, i8 119, i8 105, i8 108, i8 108, i8 32, i8 116, i8 97, i8 107, i8 101, i8 32, i8 37, i8 105, i8 32, i8 112, i8 111, i8 115, i8 116, i8 115, i8 32, i8 116, i8 111, i8 32, i8 99, i8 111, i8 109, i8 112, i8 108, i8 101, i8 116, i8 101, i8 10, i8 0], [41 x i8]* %".11"
  %".13" = getelementptr [41 x i8], [41 x i8]* %".11", i32 0
  %".14" = getelementptr [41 x i8], [41 x i8]* %".13", i32 0
  %".15" = bitcast [41 x i8]* %".14" to i8*
  %".16" = call i32 @"b"(i32 24719254)
  %".17" = call i32 (i8*, ...) @"printf"(i8* %".15", i32 %".16")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

declare i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

define i32 @"a"(i32 %".1") 
{
entry:
  %".3" = alloca i32
  store i32 %".1", i32* %".3"
  %".5" = load i32, i32* %".3"
  %".6" = icmp eq i32 %".5", 1
  br i1 %".6", label %"entry.if", label %"entry.else"
entry.if:
  ret i32 0
entry.else:
  %".9" = load i32, i32* %".3"
  %".10" = srem i32 %".9", 2
  %".11" = icmp eq i32 %".10", 0
  br i1 %".11", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %".20" = load i32, i32* %".3"
  %".21" = mul i32 3, %".20"
  %".22" = add i32 %".21", 1
  %".23" = call i32 @"a"(i32 %".22")
  %".24" = add i32 1, %".23"
  ret i32 %".24"
entry.else.if:
  %".13" = load i32, i32* %".3"
  %".14" = sdiv i32 %".13", 2
  %".15" = call i32 @"a"(i32 %".14")
  %".16" = add i32 1, %".15"
  ret i32 %".16"
entry.else.else:
  br label %"entry.else.endif"
entry.else.endif:
  br label %"entry.endif"
}

define i32 @"b"(i32 %".1") 
{
entry:
  %".3" = alloca i32
  store i32 %".1", i32* %".3"
  %".5" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 105, i8 10, i8 0], [4 x i8]* %".5"
  %".7" = getelementptr [4 x i8], [4 x i8]* %".5", i32 0
  %".8" = getelementptr [4 x i8], [4 x i8]* %".7", i32 0
  %".9" = bitcast [4 x i8]* %".8" to i8*
  %".10" = load i32, i32* %".3"
  %".11" = call i32 (i8*, ...) @"printf"(i8* %".9", i32 %".10")
  %".12" = load i32, i32* %".3"
  %".13" = icmp eq i32 %".12", 1
  br i1 %".13", label %"entry.if", label %"entry.else"
entry.if:
  ret i32 0
entry.else:
  %".16" = load i32, i32* %".3"
  %".17" = srem i32 %".16", 2
  %".18" = icmp eq i32 %".17", 0
  br i1 %".18", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %".27" = load i32, i32* %".3"
  %".28" = mul i32 3, %".27"
  %".29" = add i32 %".28", 1
  %".30" = call i32 @"b"(i32 %".29")
  %".31" = add i32 1, %".30"
  ret i32 %".31"
entry.else.if:
  %".20" = load i32, i32* %".3"
  %".21" = sdiv i32 %".20", 2
  %".22" = call i32 @"b"(i32 %".21")
  %".23" = add i32 1, %".22"
  ret i32 %".23"
entry.else.else:
  br label %"entry.else.endif"
entry.else.endif:
  br label %"entry.endif"
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }