; ModuleID = "tests/test_arrays.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca [3 x i32]
  %".9" = getelementptr [3 x i32], [3 x i32]* %".8", i32 0
  %".10" = bitcast [3 x i32]* %".9" to i32*
  store i32 4253, i32* %".10"
  %".12" = getelementptr [3 x i32], [3 x i32]* %".8", i32 1
  %".13" = bitcast [3 x i32]* %".12" to i32*
  store i32 1, i32* %".13"
  %".15" = getelementptr [3 x i32], [3 x i32]* %".8", i32 2
  %".16" = bitcast [3 x i32]* %".15" to i32*
  store i32 3, i32* %".16"
  %".18" = alloca [10 x i8]
  store [10 x i8] [i8 37, i8 105, i8 45, i8 37, i8 105, i8 45, i8 37, i8 105, i8 10, i8 0], [10 x i8]* %".18"
  %".20" = getelementptr [10 x i8], [10 x i8]* %".18", i32 0
  %".21" = getelementptr [10 x i8], [10 x i8]* %".20", i32 0
  %".22" = bitcast [10 x i8]* %".21" to i8*
  %".23" = getelementptr [3 x i32], [3 x i32]* %".8", i32 0
  %".24" = bitcast [3 x i32]* %".23" to i32*
  %".25" = load i32, i32* %".24"
  %".26" = getelementptr [3 x i32], [3 x i32]* %".8", i32 1
  %".27" = bitcast [3 x i32]* %".26" to i32*
  %".28" = load i32, i32* %".27"
  %".29" = getelementptr [3 x i32], [3 x i32]* %".8", i32 2
  %".30" = bitcast [3 x i32]* %".29" to i32*
  %".31" = load i32, i32* %".30"
  %".32" = call i32 (i8*, ...) @"printf"(i8* %".22", i32 %".25", i32 %".28", i32 %".31")
  %".33" = alloca [22 x i8]
  %".34" = getelementptr [22 x i8], [22 x i8]* %".33", i32 0
  %".35" = bitcast [22 x i8]* %".34" to i8*
  %".36" = call i32 (i8*, ...) @"printf"(i8* %".35")
  %".37" = alloca [22 x i8]
  store [22 x i8] [i8 116, i8 119, i8 101, i8 110, i8 116, i8 121, i8 32, i8 99, i8 104, i8 97, i8 114, i8 97, i8 99, i8 116, i8 101, i8 114, i8 32, i8 115, i8 116, i8 114, i8 10, i8 0], [22 x i8]* %".37"
  %".39" = getelementptr [22 x i8], [22 x i8]* %".37", i32 0
  %".40" = load [22 x i8], [22 x i8]* %".39"
  store [22 x i8] %".40", [22 x i8]* %".33"
  %".42" = getelementptr [22 x i8], [22 x i8]* %".33", i32 0
  %".43" = bitcast [22 x i8]* %".42" to i8*
  %".44" = call i32 (i8*, ...) @"printf"(i8* %".43")
  %".45" = alloca [32 x i8*]
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }