; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca i1024
  %".7" = load i1024, i1024* %".6"
  %".8" = call i32 (i8*, ...) @"printf"(i8* %".2", i1024 %".7")
  %".9" = alloca i1024
  %".10" = load i1024, i1024* %".6"
  store i1024 %".10", i1024* %".9"
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"fdopen_fmt" = internal constant [2 x i8] c"r\00"
@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
define void @"a"() 
{
entry:
  %".2" = alloca i1024
  store i1024 135066410865995223349603216278805969938881475605667027524485143851526510604859533833940287150571909441798207282164471551373680419703964191743046496589274256239341020864383202110372958725762358509643110564073501508187510676594629205563685529475213500852879416377328533906109750544334999811150056977236890927563, i1024* %".2"
  ret void
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }