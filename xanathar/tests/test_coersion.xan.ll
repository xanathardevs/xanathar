; ModuleID = "tests/test_coersion.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"t2" = type {}
%"t2_vtable" = type {}
define void @"main"(i32 %".1", i8** %".2") !dbg !10
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %"$x" = alloca i64
  call void @"llvm.dbg.declare"(metadata i64* %"$x", metadata !12, metadata !13), !dbg !14
  %".7" = zext i32 1 to i64
  call void @"llvm.dbg.value"(metadata i64 %".7", metadata !12, metadata !13), !dbg !15
  store i64 %".7", i64* %"$x"
  %"$y" = alloca i64*
  call void @"llvm.dbg.declare"(metadata i64** %"$y", metadata !18, metadata !13), !dbg !19
  call void @"llvm.dbg.value"(metadata i64* %"$x", metadata !18, metadata !13), !dbg !20
  store i64* %"$x", i64** %"$y"
  %"$z" = alloca i64
  call void @"llvm.dbg.declare"(metadata i64* %"$z", metadata !22, metadata !13), !dbg !23
  %".14" = zext i32 0 to i64
  call void @"llvm.dbg.value"(metadata i64 %".14", metadata !22, metadata !13), !dbg !24
  store i64 %".14", i64* %"$z"
  %"loaded_$z" = load i64, i64* %"$z"
  %".17" = zext i32 1 to i64
  %".18" = add i64 %"loaded_$z", %".17"
  call void @"llvm.dbg.value"(metadata i64 %".18", metadata !22, metadata !13), !dbg !25
  store i64 %".18", i64* %"$z"
  %"loaded_rip" = call i64 asm  "", "={rip}"
()
  %".21" = alloca i64
  store i64 %"loaded_rip", i64* %".21"
  %".23" = alloca [12 x i8]
  store [12 x i8] [i8 37, i8 112, i8 32, i8 40, i8 111, i8 102, i8 32, i8 37, i8 115, i8 41, i8 10, i8 0], [12 x i8]* %".23"
  %".25" = getelementptr [12 x i8], [12 x i8]* %".23", i32 0
  %".26" = getelementptr [12 x i8], [12 x i8]* %".25", i32 0
  %".27" = bitcast [12 x i8]* %".26" to i8*
  %"loaded_$q" = load i64, i64* %".21"
  %"loaded_$q.1" = load i64, i64* %".21"
  %".28" = alloca [4 x i8]
  store [4 x i8] [i8 105, i8 54, i8 52, i8 0], [4 x i8]* %".28"
  %".30" = getelementptr [4 x i8], [4 x i8]* %".28", i32 0
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".27", i64 %"loaded_$q", [4 x i8]* %".30"), !dbg !34
  %".31" = alloca [12 x i8]
  store [12 x i8] [i8 37, i8 112, i8 32, i8 40, i8 111, i8 102, i8 32, i8 37, i8 115, i8 41, i8 10, i8 0], [12 x i8]* %".31"
  %".33" = getelementptr [12 x i8], [12 x i8]* %".31", i32 0
  %".34" = getelementptr [12 x i8], [12 x i8]* %".33", i32 0
  %".35" = bitcast [12 x i8]* %".34" to i8*
  %".36" = alloca [9 x i8]
  store [9 x i8] [i8 118, i8 111, i8 105, i8 100, i8 32, i8 40, i8 41, i8 42, i8 0], [9 x i8]* %".36"
  %".38" = getelementptr [9 x i8], [9 x i8]* %".36", i32 0
  %"called_printf.1" = call i32 (i8*, ...) @"printf"(i8* %".35", void ()* @"test", [9 x i8]* %".38"), !dbg !35
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"llvm.dbg.declare"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

declare void @"llvm.dbg.value"(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
define void @"test"() noinline!dbg !29
{
entry:
  ret void
}

define void @"t2__init_hook"() noinline!dbg !33
{
entry:
  ret void
}

@"t2_vtable_value" = internal constant %"t2_vtable" {}
!llvm.dbg.cu = !{ !2 }
!llvm.module.flags = !{ !3, !4, !5 }
!llvm.ident = !{ !6 }
!gen-by = !{ !36 }
!0 = !DIFile(directory: "/home/proc-daemon/Dropbox/Xanathar/xanathar/tests", filename: "test_coersion.xan")
!1 = !{  }
!2 = distinct !DICompileUnit(emissionKind: FullDebug, enums: !1, file: !0, isOptimized: false, language: DW_LANG_Python, producer: "Xanathar v. a0.0.1", runtimeVersion: 0)
!3 = !{ i32 2, !"Dwarf Version", i32 4 }
!4 = !{ i32 2, !"Debug Info Version", i32 3 }
!5 = !{ i32 1, !"wchar_size", i32 4 }
!6 = !{ !"Xanathar a0.0.1" }
!7 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!8 = !{ !7 }
!9 = !DISubroutineType(types: !8)
!10 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !9, unit: !2, variables: !1)
!11 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!12 = !DILocalVariable(file: !0, line: 1, name: "x", scope: !10, type: !11)
!13 = !DIExpression()
!14 = !DILocation(column: 1, line: 1, scope: !10)
!15 = !DILocation(column: 1, line: 2, scope: !10)
!16 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!17 = !DIDerivedType(baseType: !16, size: 64, tag: DW_TAG_pointer_type)
!18 = !DILocalVariable(file: !0, line: 1, name: "y", scope: !10, type: !17)
!19 = !DILocation(column: 1, line: 4, scope: !10)
!20 = !DILocation(column: 1, line: 5, scope: !10)
!21 = !DIBasicType(encoding: DW_ATE_signed, name: "i64", size: 64)
!22 = !DILocalVariable(file: !0, line: 1, name: "z", scope: !10, type: !21)
!23 = !DILocation(column: 1, line: 7, scope: !10)
!24 = !DILocation(column: 1, line: 8, scope: !10)
!25 = !DILocation(column: 1, line: 9, scope: !10)
!26 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!27 = !{ !26 }
!28 = !DISubroutineType(types: !27)
!29 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "test", scope: !0, scopeLine: 1, type: !28, unit: !2, variables: !1)
!30 = !DIBasicType(encoding: DW_ATE_signed, name: "void", size: 8)
!31 = !{ !30 }
!32 = !DISubroutineType(types: !31)
!33 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "t2__init_hook", scope: !0, scopeLine: 1, type: !32, unit: !2, variables: !1)
!34 = !DILocation(column: 1, line: 15, scope: !10)
!35 = !DILocation(column: 1, line: 16, scope: !10)
!36 = !{ !"Xanathar" }