; ModuleID = "tests/test_io.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i8*
  %".9" = alloca i8
  %".10" = trunc i32 3 to i8
  store i8 %".10", i8* %".9"
  store i8* %".9", i8** %".8"
  %".13" = load i8*, i8** %".8"
  %".14" = call i8* @"get"(i8* %".13", i32 20)
  %".15" = alloca i32
  %".16" = call i32 @"atoi"(i8* %".9")
  store i32 %".16", i32* %".15"
  %".18" = alloca [3 x i8]
  store [3 x i8] [i8 37, i8 105, i8 0], [3 x i8]* %".18"
  %".20" = getelementptr [3 x i8], [3 x i8]* %".18", i32 0
  %".21" = getelementptr [3 x i8], [3 x i8]* %".20", i32 0
  %".22" = bitcast [3 x i8]* %".21" to i8*
  %".23" = load i32, i32* %".15"
  %".24" = add i32 %".23", 1
  %".25" = call i32 (i8*, ...) @"printf"(i8* %".22", i32 %".24")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare external i8* @"get"(i8* %".1", i32 %".2") 

declare external i32 @"atoi"(i8* %".1") 

declare external i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }