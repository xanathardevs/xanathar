; ModuleID = "tests/hello-gtk.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"GtkApplication" = type opaque
%"GtkWidget" = type opaque
%"GtkWindow" = type opaque
%"GtkContainer" = type opaque
%"GtkDialog" = type opaque
%"gpointer" = type opaque
define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %"$argc" = alloca i32
  store i32 %".1", i32* %"$argc"
  %"$argv" = alloca i8**
  store i8** %".2", i8*** %"$argv"
  %"$app" = alloca %"GtkApplication"*
  call void @llvm.dbg.declare(metadata %"GtkApplication"** %"$app", metadata !23, metadata !8)
  %"$status" = alloca i32
  call void @llvm.dbg.declare(metadata i32* %"$status", metadata !28, metadata !8)
  %".8" = alloca [19 x i8]
  store [19 x i8] [i8 120, i8 97, i8 110, i8 97, i8 116, i8 104, i8 97, i8 114, i8 46, i8 104, i8 101, i8 108, i8 108, i8 111, i8 45, i8 103, i8 116, i8 107, i8 0], [19 x i8]* %".8"
  %".10" = getelementptr [19 x i8], [19 x i8]* %".8", i32 0
  %".11" = getelementptr [19 x i8], [19 x i8]* %".10", i32 0
  %".12" = bitcast [19 x i8]* %".11" to i8*
  %"loaded_$G_APPLICATION_FLAGS_NONE" = load i32, i32* @"G_APPLICATION_FLAGS_NONE"
  %"called_xtk_application_new" = call %"GtkApplication"* @"xtk_application_new"(i8* %".12", i32 %"loaded_$G_APPLICATION_FLAGS_NONE")
  store %"GtkApplication"* %"called_xtk_application_new", %"GtkApplication"** %"$app"
  %"loaded_$app" = load %"GtkApplication"*, %"GtkApplication"** %"$app"
  %".14" = alloca [9 x i8]
  store [9 x i8] [i8 97, i8 99, i8 116, i8 105, i8 118, i8 97, i8 116, i8 101, i8 0], [9 x i8]* %".14"
  %".16" = getelementptr [9 x i8], [9 x i8]* %".14", i32 0
  %".17" = getelementptr [9 x i8], [9 x i8]* %".16", i32 0
  %".18" = bitcast [9 x i8]* %".17" to i8*
  %"ptr_to_" = alloca i32
  store i32 0, i32* %"ptr_to_"
  call void (...) @"x_signal_connect"(%"GtkApplication"* %"loaded_$app", i8* %".18", void (%"GtkApplication"*, i8*)* @"activate", i32* %"ptr_to_")
  %"loaded_$app.1" = load %"GtkApplication"*, %"GtkApplication"** %"$app"
  %"derefed_from_$argc" = load i32, i32* %"$argc"
  %"derefed_from_$argv" = load i8**, i8*** %"$argv"
  %"called_x_application_run" = call i32 @"x_application_run"(%"GtkApplication"* %"loaded_$app.1", i32 %"derefed_from_$argc", i8** %"derefed_from_$argv")
  store i32 %"called_x_application_run", i32* %"$status"
  %"loaded_$app.2" = load %"GtkApplication"*, %"GtkApplication"** %"$app"
  call void (...) @"x_object_unref"(%"GtkApplication"* %"loaded_$app.2")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @llvm.dbg.declare(metadata %".1", metadata %".2", metadata %".3") nounwind readnone

@"NULL" = internal constant i8* inttoptr (i32 0 to i8*)
declare external %"GtkWidget"* @"xtk_application_window_new"(%"GtkApplication"* %".1") 

declare external void @"xtk_window_set_title"(%"GtkWindow"* %".1", i8* %".2") 

declare external void @"xtk_window_set_default_size"(%"GtkWindow"* %".1", i32 %".2", i32 %".3") 

declare external void @"xtk_widget_show_all"(%"GtkWidget"* %".1") 

declare external %"GtkApplication"* @"xtk_application_new"(i8* %".1", i32 %".2") 

declare external void @"x_signal_connect"(...) 

declare external i32 @"x_application_run"(%"GtkApplication"* %".1", i32 %".2", i8** %".3") 

declare external void @"x_object_unref"(...) 

declare external %"GtkWidget"* @"xtk_button_box_new"(i32 %".1") 

declare external void @"xtk_container_add"(%"GtkContainer"* %".1", %"GtkWidget"* %".2") 

declare external %"GtkWidget"* @"xtk_button_new_with_label"(i8* %".1") 

declare external %"GtkWidget"* @"xtk_message_dialog_new"(...) 

declare external i32 @"xtk_dialog_run"(%"GtkDialog"* %".1") 

declare external void @"xtk_widget_destroy"(%"GtkWidget"* %".1") 

@"G_APPLICATION_FLAGS_NONE" = internal constant i32 0
@"GTK_ORIENTATION_HORIZONTAL" = internal constant i32 0
@"GTK_ORIENTATION_VERTICAL" = internal constant i32 1
@"GTK_DIALOG_MODAL" = internal constant i32 1
@"GTK_DIALOG_DESTROY_WITH_PARENT" = internal constant i32 2
@"GTK_DIALOG_USE_HEADER_BAR" = internal constant i32 4
@"GTK_MESSAGE_INFO" = internal constant i32 0
@"GTK_MESSAGE_WARNING" = internal constant i32 1
@"GTK_MESSAGE_QUESTION" = internal constant i32 2
@"GTK_MESSAGE_ERROR" = internal constant i32 3
@"GTK_MESSAGE_OTHER" = internal constant i32 4
@"GTK_BUTTONS_NONE" = internal constant i32 0
@"GTK_BUTTONS_OK" = internal constant i32 1
@"GTK_BUTTONS_CLOSE" = internal constant i32 2
@"GTK_BUTTONS_CANCEL" = internal constant i32 3
@"GTK_BUTTONS_YES_NO" = internal constant i32 4
@"GTK_BUTTONS_OK_CANCEL" = internal constant i32 5
define void @"print_hello"(%"GtkWidget"* %".1", i8* %".2") 
{
entry:
  %".4" = alloca %"GtkWidget"*
  store %"GtkWidget"* %".1", %"GtkWidget"** %".4"
  %".6" = alloca i8*
  store i8* %".2", i8** %".6"
  %"derefed_from_NULL" = load i8*, i8** @"NULL"
  %"loaded_$GTK_DIALOG_USE_HEADER_BAR" = load i32, i32* @"GTK_DIALOG_USE_HEADER_BAR"
  %"loaded_$GTK_MESSAGE_INFO" = load i32, i32* @"GTK_MESSAGE_INFO"
  %"loaded_$GTK_BUTTONS_OK" = load i32, i32* @"GTK_BUTTONS_OK"
  %".8" = alloca [12 x i8]
  store [12 x i8] [i8 72, i8 101, i8 108, i8 108, i8 111, i8 32, i8 71, i8 84, i8 75, i8 33, i8 10, i8 0], [12 x i8]* %".8"
  %".10" = getelementptr [12 x i8], [12 x i8]* %".8", i32 0
  %".11" = getelementptr [12 x i8], [12 x i8]* %".10", i32 0
  %".12" = bitcast [12 x i8]* %".11" to i8*
  %"called_xtk_message_dialog_new" = call %"GtkWidget"* (...) @"xtk_message_dialog_new"(i8* %"derefed_from_NULL", i32 %"loaded_$GTK_DIALOG_USE_HEADER_BAR", i32 %"loaded_$GTK_MESSAGE_INFO", i32 %"loaded_$GTK_BUTTONS_OK", i8* %".12")
  %".13" = alloca %"GtkWidget"*
  store %"GtkWidget"* %"called_xtk_message_dialog_new", %"GtkWidget"** %".13"
  %"loaded_$dialog" = load %"GtkWidget"*, %"GtkWidget"** %".13"
  %".15" = bitcast %"GtkWidget"* %"loaded_$dialog" to %"GtkDialog"*
  %"called_xtk_dialog_run" = call i32 @"xtk_dialog_run"(%"GtkDialog"* %".15")
  %".16" = alloca [8 x i8]
  store [8 x i8] [i8 71, i8 111, i8 116, i8 32, i8 37, i8 112, i8 10, i8 0], [8 x i8]* %".16"
  %".18" = getelementptr [8 x i8], [8 x i8]* %".16", i32 0
  %".19" = getelementptr [8 x i8], [8 x i8]* %".18", i32 0
  %".20" = bitcast [8 x i8]* %".19" to i8*
  %"loaded_$dialog.1" = load %"GtkWidget"*, %"GtkWidget"** %".13"
  %"called_printf" = call i32 (i8*, ...) @"printf"(i8* %".20", %"GtkWidget"* %"loaded_$dialog.1")
  %"loaded_$dialog.2" = load %"GtkWidget"*, %"GtkWidget"** %".13"
  call void @"xtk_widget_destroy"(%"GtkWidget"* %"loaded_$dialog.2")
  ret void
}

define void @"activate"(%"GtkApplication"* %".1", i8* %".2") 
{
entry:
  %".4" = alloca %"GtkApplication"*
  store %"GtkApplication"* %".1", %"GtkApplication"** %".4"
  %".6" = alloca i8*
  store i8* %".2", i8** %".6"
  %"$window" = alloca %"GtkWidget"*
  call void @llvm.dbg.declare(metadata %"GtkWidget"** %"$window", metadata !7, metadata !8)
  %"$button" = alloca %"GtkWidget"*
  call void @llvm.dbg.declare(metadata %"GtkWidget"** %"$button", metadata !13, metadata !8)
  %"$button_box" = alloca %"GtkWidget"*
  call void @llvm.dbg.declare(metadata %"GtkWidget"** %"$button_box", metadata !18, metadata !8)
  %"loaded_$app" = load %"GtkApplication"*, %"GtkApplication"** %".4"
  %"called_xtk_application_window_new" = call %"GtkWidget"* @"xtk_application_window_new"(%"GtkApplication"* %"loaded_$app")
  store %"GtkWidget"* %"called_xtk_application_window_new", %"GtkWidget"** %"$window"
  %"loaded_$window" = load %"GtkWidget"*, %"GtkWidget"** %"$window"
  %".12" = bitcast %"GtkWidget"* %"loaded_$window" to %"GtkWindow"*
  %".13" = alloca [15 x i8]
  store [15 x i8] [i8 72, i8 101, i8 108, i8 108, i8 111, i8 32, i8 116, i8 111, i8 32, i8 71, i8 84, i8 75, i8 43, i8 33, i8 0], [15 x i8]* %".13"
  %".15" = getelementptr [15 x i8], [15 x i8]* %".13", i32 0
  %".16" = getelementptr [15 x i8], [15 x i8]* %".15", i32 0
  %".17" = bitcast [15 x i8]* %".16" to i8*
  call void @"xtk_window_set_title"(%"GtkWindow"* %".12", i8* %".17")
  %"loaded_$window.1" = load %"GtkWidget"*, %"GtkWidget"** %"$window"
  %".18" = bitcast %"GtkWidget"* %"loaded_$window.1" to %"GtkWindow"*
  call void @"xtk_window_set_default_size"(%"GtkWindow"* %".18", i32 200, i32 200)
  %"loaded_$GTK_ORIENTATION_HORIZONTAL" = load i32, i32* @"GTK_ORIENTATION_HORIZONTAL"
  %"called_xtk_button_box_new" = call %"GtkWidget"* @"xtk_button_box_new"(i32 %"loaded_$GTK_ORIENTATION_HORIZONTAL")
  store %"GtkWidget"* %"called_xtk_button_box_new", %"GtkWidget"** %"$button_box"
  %"loaded_$window.2" = load %"GtkWidget"*, %"GtkWidget"** %"$window"
  %".20" = bitcast %"GtkWidget"* %"loaded_$window.2" to %"GtkContainer"*
  %"loaded_$button_box" = load %"GtkWidget"*, %"GtkWidget"** %"$button_box"
  call void @"xtk_container_add"(%"GtkContainer"* %".20", %"GtkWidget"* %"loaded_$button_box")
  %".21" = alloca [4 x i8]
  store [4 x i8] [i8 72, i8 105, i8 33, i8 0], [4 x i8]* %".21"
  %".23" = getelementptr [4 x i8], [4 x i8]* %".21", i32 0
  %".24" = getelementptr [4 x i8], [4 x i8]* %".23", i32 0
  %".25" = bitcast [4 x i8]* %".24" to i8*
  %"called_xtk_button_new_with_label" = call %"GtkWidget"* @"xtk_button_new_with_label"(i8* %".25")
  store %"GtkWidget"* %"called_xtk_button_new_with_label", %"GtkWidget"** %"$button"
  %"loaded_$button" = load %"GtkWidget"*, %"GtkWidget"** %"$button"
  %".27" = alloca [8 x i8]
  store [8 x i8] [i8 99, i8 108, i8 105, i8 99, i8 107, i8 101, i8 100, i8 0], [8 x i8]* %".27"
  %".29" = getelementptr [8 x i8], [8 x i8]* %".27", i32 0
  %".30" = getelementptr [8 x i8], [8 x i8]* %".29", i32 0
  %".31" = bitcast [8 x i8]* %".30" to i8*
  %"ptr_to_" = alloca i32
  store i32 0, i32* %"ptr_to_"
  call void (...) @"x_signal_connect"(%"GtkWidget"* %"loaded_$button", i8* %".31", void (%"GtkWidget"*, i8*)* @"print_hello", i32* %"ptr_to_")
  %"loaded_$button_box.1" = load %"GtkWidget"*, %"GtkWidget"** %"$button_box"
  %".33" = bitcast %"GtkWidget"* %"loaded_$button_box.1" to %"GtkContainer"*
  %"loaded_$button.1" = load %"GtkWidget"*, %"GtkWidget"** %"$button"
  call void @"xtk_container_add"(%"GtkContainer"* %".33", %"GtkWidget"* %"loaded_$button.1")
  %"loaded_$window.3" = load %"GtkWidget"*, %"GtkWidget"** %"$window"
  call void @"xtk_widget_show_all"(%"GtkWidget"* %"loaded_$window.3")
  ret void
}

!gen-by = !{ !29 }
!0 = !DIFile(directory: "tests", filename: "hello-gtk.xan")
!1 = !{  }
!2 = distinct !DICompileUnit(emissionKind: FullDebug, enums: !1, file: !0, isOptimized: false, language: DW_LANG_Python, producer: "Xanathar v. a0.0.1", runtimeVersion: 0)
!3 = !DIBasicType(encoding: DW_ATE_signed, name: "\22GtkWidget\22*", size: 8)
!4 = !DIBasicType(encoding: DW_ATE_signed, name: "void (%\22GtkApplication\22*, i8*)*", size: 8)
!5 = !DISubroutineType(types: !4)
!6 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "activate", scope: !0, scopeLine: 1, type: !5, unit: !2, variables: !1)
!7 = !DILocalVariable(file: !0, line: 1, name: "window", scope: !6, type: !3)
!8 = !DIExpression()
!9 = !DIBasicType(encoding: DW_ATE_signed, name: "\22GtkWidget\22*", size: 8)
!10 = !DIBasicType(encoding: DW_ATE_signed, name: "void (%\22GtkApplication\22*, i8*)*", size: 8)
!11 = !DISubroutineType(types: !10)
!12 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "activate", scope: !0, scopeLine: 1, type: !11, unit: !2, variables: !1)
!13 = !DILocalVariable(file: !0, line: 1, name: "button", scope: !12, type: !9)
!14 = !DIBasicType(encoding: DW_ATE_signed, name: "\22GtkWidget\22*", size: 8)
!15 = !DIBasicType(encoding: DW_ATE_signed, name: "void (%\22GtkApplication\22*, i8*)*", size: 8)
!16 = !DISubroutineType(types: !15)
!17 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "activate", scope: !0, scopeLine: 1, type: !16, unit: !2, variables: !1)
!18 = !DILocalVariable(file: !0, line: 1, name: "button_box", scope: !17, type: !14)
!19 = !DIBasicType(encoding: DW_ATE_signed, name: "\22GtkApplication\22*", size: 8)
!20 = !DIBasicType(encoding: DW_ATE_signed, name: "void (i32, i8**)*", size: 8)
!21 = !DISubroutineType(types: !20)
!22 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !21, unit: !2, variables: !1)
!23 = !DILocalVariable(file: !0, line: 1, name: "app", scope: !22, type: !19)
!24 = !DIBasicType(encoding: DW_ATE_signed, name: "i32", size: 4)
!25 = !DIBasicType(encoding: DW_ATE_signed, name: "void (i32, i8**)*", size: 8)
!26 = !DISubroutineType(types: !25)
!27 = distinct !DISubprogram(file: !0, isDefinition: true, isLocal: false, isOptimized: false, name: "main", scope: !0, scopeLine: 1, type: !26, unit: !2, variables: !1)
!28 = !DILocalVariable(file: !0, line: 1, name: "status", scope: !27, type: !24)
!29 = !{ !"Xanathar" }