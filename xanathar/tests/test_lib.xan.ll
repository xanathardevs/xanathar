; ModuleID = "tests/test_lib.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"fdopen_fmt" = internal constant [2 x i8] c"r\00"
@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
declare void @"nop"() 

declare i32* @"range"(i32 %".1", i32 %".2") 

declare i32 @"cmp"(i32 %".1", i32 %".2") 

declare void @"inc"(i32* %".1") 

declare void @"dec"(i32* %".1") 

define i32 @"fib"(i32 %".1") 
{
entry:
  %".3" = alloca i32
  store i32 %".1", i32* %".3"
  %".5" = load i32, i32* %".3"
  %".6" = icmp slt i32 %".5", 2
  br i1 %".6", label %"entry.if", label %"entry.else"
entry.if:
  ret i32 1
entry.else:
  %".9" = load i32, i32* %".3"
  %".10" = sub i32 %".9", 1
  %".11" = call i32 @"fib"(i32 %".10")
  %".12" = load i32, i32* %".3"
  %".13" = sub i32 %".12", 2
  %".14" = call i32 @"fib"(i32 %".13")
  %".15" = add i32 %".11", %".14"
  ret i32 %".15"
entry.endif:
  ret i32 -1
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }