; ModuleID = "tests/test_let.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i32
  store i32 32, i32* %".8"
  %".10" = alloca i32
  %".11" = alloca [3 x i8]
  store [3 x i8] [i8 37, i8 110, i8 0], [3 x i8]* %".11"
  %".13" = getelementptr [3 x i8], [3 x i8]* %".11", i32 0
  %".14" = getelementptr [3 x i8], [3 x i8]* %".13", i32 0
  %".15" = bitcast [3 x i8]* %".14" to i8*
  %".16" = call i32 (i8*, ...) @"printf"(i8* %".15", i32* %".10")
  %".17" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 105, i8 10, i8 0], [4 x i8]* %".17"
  %".19" = getelementptr [4 x i8], [4 x i8]* %".17", i32 0
  %".20" = getelementptr [4 x i8], [4 x i8]* %".19", i32 0
  %".21" = bitcast [4 x i8]* %".20" to i8*
  %".22" = load i32, i32* %".8"
  %".23" = call i32 (i8*, ...) @"printf"(i8* %".21", i32 %".22")
  %".24" = alloca [4 x i8]
  store [4 x i8] [i8 37, i8 105, i8 10, i8 0], [4 x i8]* %".24"
  %".26" = getelementptr [4 x i8], [4 x i8]* %".24", i32 0
  %".27" = getelementptr [4 x i8], [4 x i8]* %".26", i32 0
  %".28" = bitcast [4 x i8]* %".27" to i8*
  %".29" = load i32, i32* %".10"
  %".30" = call i32 (i8*, ...) @"printf"(i8* %".28", i32 %".29")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"nop"() 

declare i32* @"range"(i32 %".1", i32 %".2") 

declare i32 @"cmp"(i32 %".1", i32 %".2") 

declare void @"inc"(i32* %".1") 

declare void @"dec"(i32* %".1") 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }