; ModuleID = "tests/test_interface.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"IDisposable_vtable" = type {void ()*}
%"IDisposable" = type {}
%"ISerializable_vtable" = type {void (i8*)*, void (i8*)*}
%"ISerializable" = type {}
%"Test1" = type {i32}
%"Test1_vtable" = type {}
%"DisposableSerializable" = type {i32}
%"DisposableSerializable_vtable" = type {void (%"DisposableSerializable"*, i8*)*, void (%"DisposableSerializable"*, i8*)*, void (%"DisposableSerializable"*)*}
define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca %"DisposableSerializable"
  %".9" = alloca i8
  call void @"DisposableSerializable__init_hook"(%"DisposableSerializable"* %".8", i32 32)
  %".11" = alloca [11 x i8]
  store [11 x i8] [i8 79, i8 114, i8 105, i8 103, i8 105, i8 110, i8 97, i8 108, i8 58, i8 32, i8 0], [11 x i8]* %".11"
  %".13" = getelementptr [11 x i8], [11 x i8]* %".11", i32 0
  %".14" = getelementptr [11 x i8], [11 x i8]* %".13", i32 0
  %".15" = bitcast [11 x i8]* %".14" to i8*
  %".16" = call i32 (i8*, ...) @"printf"(i8* %".15")
  call void @"DisposableSerializable_print"(%"DisposableSerializable"* %".8")
  %".18" = alloca [32 x i8]
  store [32 x i8] [i8 83, i8 101, i8 114, i8 105, i8 97, i8 108, i8 105, i8 122, i8 105, i8 110, i8 103, i8 32, i8 97, i8 110, i8 100, i8 32, i8 68, i8 101, i8 115, i8 101, i8 114, i8 105, i8 97, i8 108, i8 105, i8 122, i8 105, i8 110, i8 103, i8 58, i8 32, i8 0], [32 x i8]* %".18"
  %".20" = getelementptr [32 x i8], [32 x i8]* %".18", i32 0
  %".21" = getelementptr [32 x i8], [32 x i8]* %".20", i32 0
  %".22" = bitcast [32 x i8]* %".21" to i8*
  %".23" = call i32 (i8*, ...) @"printf"(i8* %".22")
  call void @"DisposableSerializable_Serialize"(%"DisposableSerializable"* %".8", i8* %".9")
  call void @"DisposableSerializable_Deserialize"(%"DisposableSerializable"* %".8", i8* %".9")
  call void @"DisposableSerializable_print"(%"DisposableSerializable"* %".8")
  call void @"DisposableSerializable_Dispose"(%"DisposableSerializable"* %".8")
  %".28" = alloca [12 x i8]
  store [12 x i8] [i8 68, i8 105, i8 115, i8 112, i8 111, i8 115, i8 105, i8 110, i8 103, i8 58, i8 32, i8 0], [12 x i8]* %".28"
  %".30" = getelementptr [12 x i8], [12 x i8]* %".28", i32 0
  %".31" = getelementptr [12 x i8], [12 x i8]* %".30", i32 0
  %".32" = bitcast [12 x i8]* %".31" to i8*
  %".33" = call i32 (i8*, ...) @"printf"(i8* %".32")
  call void @"DisposableSerializable_print"(%"DisposableSerializable"* %".8")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

declare i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

@"Test1_vtable_value" = internal constant %"Test1_vtable" {}
define void @"DisposableSerializable_Serialize"(%"DisposableSerializable"* %".1", i8* %".2") 
{
entry:
  %".4" = alloca %"DisposableSerializable"*
  store %"DisposableSerializable"* %".1", %"DisposableSerializable"** %".4"
  %".6" = alloca i8*
  store i8* %".2", i8** %".6"
  %".8" = load i8*, i8** %".6"
  %".9" = alloca [3 x i8]
  store [3 x i8] [i8 37, i8 100, i8 0], [3 x i8]* %".9"
  %".11" = getelementptr [3 x i8], [3 x i8]* %".9", i32 0
  %".12" = getelementptr [3 x i8], [3 x i8]* %".11", i32 0
  %".13" = bitcast [3 x i8]* %".12" to i8*
  %".14" = load %"DisposableSerializable"*, %"DisposableSerializable"** %".4"
  %".15" = getelementptr %"DisposableSerializable", %"DisposableSerializable"* %".14", i32 0, i32 0
  %".16" = load i32, i32* %".15"
  %".17" = call i32 (i8*, i8*, ...) @"sprintf"(i8* %".8", i8* %".13", i32 %".16")
  ret void
}

define void @"DisposableSerializable_Deserialize"(%"DisposableSerializable"* %".1", i8* %".2") 
{
entry:
  %".4" = alloca %"DisposableSerializable"*
  store %"DisposableSerializable"* %".1", %"DisposableSerializable"** %".4"
  %".6" = alloca i8*
  store i8* %".2", i8** %".6"
  %".8" = load i8*, i8** %".6"
  %".9" = call i32 @"atoi"(i8* %".8")
  %".10" = load %"DisposableSerializable"*, %"DisposableSerializable"** %".4"
  %".11" = getelementptr %"DisposableSerializable", %"DisposableSerializable"* %".10", i32 0, i32 0
  store i32 %".9", i32* %".11"
  ret void
}

define void @"DisposableSerializable_Dispose"(%"DisposableSerializable"* %".1") 
{
entry:
  %".3" = alloca %"DisposableSerializable"*
  store %"DisposableSerializable"* %".1", %"DisposableSerializable"** %".3"
  %".5" = load %"DisposableSerializable"*, %"DisposableSerializable"** %".3"
  %".6" = getelementptr %"DisposableSerializable", %"DisposableSerializable"* %".5", i32 0, i32 0
  store i32 0, i32* %".6"
  ret void
}

define void @"DisposableSerializable_print"(%"DisposableSerializable"* %".1") 
{
entry:
  %".3" = alloca %"DisposableSerializable"*
  store %"DisposableSerializable"* %".1", %"DisposableSerializable"** %".3"
  %".5" = alloca [37 x i8]
  store [37 x i8] [i8 68, i8 105, i8 115, i8 112, i8 111, i8 115, i8 97, i8 98, i8 108, i8 101, i8 83, i8 101, i8 114, i8 105, i8 97, i8 108, i8 105, i8 122, i8 97, i8 98, i8 108, i8 101, i8 32, i8 119, i8 105, i8 116, i8 104, i8 32, i8 36, i8 97, i8 32, i8 61, i8 32, i8 37, i8 105, i8 10, i8 0], [37 x i8]* %".5"
  %".7" = getelementptr [37 x i8], [37 x i8]* %".5", i32 0
  %".8" = getelementptr [37 x i8], [37 x i8]* %".7", i32 0
  %".9" = bitcast [37 x i8]* %".8" to i8*
  %".10" = load %"DisposableSerializable"*, %"DisposableSerializable"** %".3"
  %".11" = getelementptr %"DisposableSerializable", %"DisposableSerializable"* %".10", i32 0, i32 0
  %".12" = load i32, i32* %".11"
  %".13" = call i32 (i8*, ...) @"printf"(i8* %".9", i32 %".12")
  ret void
}

define void @"DisposableSerializable__init_hook"(%"DisposableSerializable"* %".1", i32 %".2") 
{
entry:
  %".4" = alloca %"DisposableSerializable"*
  store %"DisposableSerializable"* %".1", %"DisposableSerializable"** %".4"
  %".6" = alloca i32
  store i32 %".2", i32* %".6"
  %".8" = load i32, i32* %".6"
  %".9" = load %"DisposableSerializable"*, %"DisposableSerializable"** %".4"
  %".10" = getelementptr %"DisposableSerializable", %"DisposableSerializable"* %".9", i32 0, i32 0
  store i32 %".8", i32* %".10"
  ret void
}

@"DisposableSerializable_vtable_value" = internal constant %"DisposableSerializable_vtable" {void (%"DisposableSerializable"*, i8*)* @"DisposableSerializable_Serialize", void (%"DisposableSerializable"*, i8*)* @"DisposableSerializable_Deserialize", void (%"DisposableSerializable"*)* @"DisposableSerializable_Dispose"}
!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }