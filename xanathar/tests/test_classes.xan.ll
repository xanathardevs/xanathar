; ModuleID = "tests/test_classes.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"Foo" = type {i8, i32}
%"Bar" = type {}
%"Baz" = type {%"Foo"*, %"Bar"*}
define void @"main"() 
{
entry:
  %".2" = alloca %"Foo"
  %".3" = trunc i32 32 to i8
  call void @"Foo__init_hook"(%"Foo"* %".2", i8 %".3", i32 64)
  %".5" = alloca [19 x i8]
  store [19 x i8] [i8 67, i8 97, i8 108, i8 108, i8 105, i8 110, i8 103, i8 32, i8 36, i8 120, i8 45, i8 62, i8 112, i8 114, i8 105, i8 110, i8 116, i8 10, i8 0], [19 x i8]* %".5"
  %".7" = getelementptr [19 x i8], [19 x i8]* %".5", i32 0
  %".8" = getelementptr [19 x i8], [19 x i8]* %".7", i32 0
  %".9" = bitcast [19 x i8]* %".8" to i8*
  %".10" = call i32 (i8*, ...) @"printf"(i8* %".9")
  call void @"Foo_print"(%"Foo"* %".2")
  %".12" = alloca %"Bar"
  %".13" = trunc i32 64 to i8
  call void @"Bar__init_hook"(%"Bar"* %".12", i8 %".13", i32 32)
  %".15" = alloca [19 x i8]
  store [19 x i8] [i8 67, i8 97, i8 108, i8 108, i8 105, i8 110, i8 103, i8 32, i8 36, i8 121, i8 45, i8 62, i8 112, i8 114, i8 105, i8 110, i8 116, i8 10, i8 0], [19 x i8]* %".15"
  %".17" = getelementptr [19 x i8], [19 x i8]* %".15", i32 0
  %".18" = getelementptr [19 x i8], [19 x i8]* %".17", i32 0
  %".19" = bitcast [19 x i8]* %".18" to i8*
  %".20" = call i32 (i8*, ...) @"printf"(i8* %".19")
  call void @"Bar_print"(%"Bar"* %".12")
  %".22" = alloca %"Baz"
  call void @"Baz__init_hook"(%"Baz"* %".22", %"Foo"* %".2", %"Bar"* %".12")
  %".24" = alloca [19 x i8]
  store [19 x i8] [i8 67, i8 97, i8 108, i8 108, i8 105, i8 110, i8 103, i8 32, i8 36, i8 122, i8 45, i8 62, i8 112, i8 114, i8 105, i8 110, i8 116, i8 10, i8 0], [19 x i8]* %".24"
  %".26" = getelementptr [19 x i8], [19 x i8]* %".24", i32 0
  %".27" = getelementptr [19 x i8], [19 x i8]* %".26", i32 0
  %".28" = bitcast [19 x i8]* %".27" to i8*
  %".29" = call i32 (i8*, ...) @"printf"(i8* %".28")
  call void @"Baz_print"(%"Baz"* %".22")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

define void @"Foo__init_hook"(%"Foo"* %".1", i8 %".2", i32 %".3") 
{
entry:
  %".5" = alloca %"Foo"*
  store %"Foo"* %".1", %"Foo"** %".5"
  %".7" = alloca i8
  store i8 %".2", i8* %".7"
  %".9" = alloca i32
  store i32 %".3", i32* %".9"
  %".11" = load i8, i8* %".7"
  %".12" = load %"Foo"*, %"Foo"** %".5"
  %".13" = getelementptr %"Foo", %"Foo"* %".12", i32 0, i32 0
  store i8 %".11", i8* %".13"
  %".15" = load i32, i32* %".9"
  %".16" = load %"Foo"*, %"Foo"** %".5"
  %".17" = getelementptr %"Foo", %"Foo"* %".16", i32 0, i32 1
  store i32 %".15", i32* %".17"
  %".19" = alloca [40 x i8]
  store [40 x i8] [i8 105, i8 110, i8 105, i8 116, i8 32, i8 104, i8 111, i8 111, i8 107, i8 32, i8 99, i8 97, i8 108, i8 108, i8 101, i8 100, i8 33, i8 32, i8 97, i8 114, i8 103, i8 115, i8 58, i8 32, i8 70, i8 111, i8 111, i8 64, i8 37, i8 112, i8 44, i8 32, i8 37, i8 105, i8 44, i8 32, i8 37, i8 105, i8 10, i8 0], [40 x i8]* %".19"
  %".21" = getelementptr [40 x i8], [40 x i8]* %".19", i32 0
  %".22" = getelementptr [40 x i8], [40 x i8]* %".21", i32 0
  %".23" = bitcast [40 x i8]* %".22" to i8*
  %".24" = load %"Foo"*, %"Foo"** %".5"
  %".25" = load i8, i8* %".7"
  %".26" = load i32, i32* %".9"
  %".27" = call i32 (i8*, ...) @"printf"(i8* %".23", %"Foo"* %".24", i8 %".25", i32 %".26")
  %".28" = alloca [57 x i8]
  store [57 x i8] [i8 112, i8 111, i8 115, i8 116, i8 32, i8 105, i8 110, i8 105, i8 116, i8 58, i8 32, i8 115, i8 101, i8 108, i8 102, i8 39, i8 115, i8 32, i8 36, i8 97, i8 32, i8 97, i8 110, i8 100, i8 32, i8 36, i8 99, i8 32, i8 97, i8 114, i8 101, i8 32, i8 37, i8 105, i8 32, i8 40, i8 48, i8 120, i8 37, i8 120, i8 41, i8 32, i8 97, i8 110, i8 100, i8 32, i8 37, i8 105, i8 32, i8 40, i8 48, i8 120, i8 37, i8 120, i8 41, i8 10, i8 0], [57 x i8]* %".28"
  %".30" = getelementptr [57 x i8], [57 x i8]* %".28", i32 0
  %".31" = getelementptr [57 x i8], [57 x i8]* %".30", i32 0
  %".32" = bitcast [57 x i8]* %".31" to i8*
  %".33" = load %"Foo"*, %"Foo"** %".5"
  %".34" = getelementptr %"Foo", %"Foo"* %".33", i32 0, i32 0
  %".35" = load i8, i8* %".34"
  %".36" = load %"Foo"*, %"Foo"** %".5"
  %".37" = getelementptr %"Foo", %"Foo"* %".36", i32 0, i32 0
  %".38" = load i8, i8* %".37"
  %".39" = load %"Foo"*, %"Foo"** %".5"
  %".40" = getelementptr %"Foo", %"Foo"* %".39", i32 0, i32 1
  %".41" = load i32, i32* %".40"
  %".42" = load %"Foo"*, %"Foo"** %".5"
  %".43" = getelementptr %"Foo", %"Foo"* %".42", i32 0, i32 1
  %".44" = load i32, i32* %".43"
  %".45" = call i32 (i8*, ...) @"printf"(i8* %".32", i8 %".35", i8 %".38", i32 %".41", i32 %".44")
  ret void
}

define void @"Foo_print"(%"Foo"* %".1") 
{
entry:
  %".3" = alloca %"Foo"*
  store %"Foo"* %".1", %"Foo"** %".3"
  %".5" = alloca [13 x i8]
  store [13 x i8] [i8 70, i8 111, i8 111, i8 60, i8 37, i8 105, i8 44, i8 32, i8 37, i8 105, i8 62, i8 10, i8 0], [13 x i8]* %".5"
  %".7" = getelementptr [13 x i8], [13 x i8]* %".5", i32 0
  %".8" = getelementptr [13 x i8], [13 x i8]* %".7", i32 0
  %".9" = bitcast [13 x i8]* %".8" to i8*
  %".10" = load %"Foo"*, %"Foo"** %".3"
  %".11" = getelementptr %"Foo", %"Foo"* %".10", i32 0, i32 0
  %".12" = load i8, i8* %".11"
  %".13" = load %"Foo"*, %"Foo"** %".3"
  %".14" = getelementptr %"Foo", %"Foo"* %".13", i32 0, i32 1
  %".15" = load i32, i32* %".14"
  %".16" = call i32 (i8*, ...) @"printf"(i8* %".9", i8 %".12", i32 %".15")
  ret void
}

define void @"Bar__init_hook"(%"Bar"* %".1", i8 %".2", i32 %".3") 
{
entry:
  %".5" = alloca %"Bar"*
  store %"Bar"* %".1", %"Bar"** %".5"
  %".7" = alloca i8
  store i8 %".2", i8* %".7"
  %".9" = alloca i32
  store i32 %".3", i32* %".9"
  %".11" = load %"Bar"*, %"Bar"** %".5"
  %".12" = bitcast %"Bar"* %".11" to %"Foo"*
  %".13" = load i8, i8* %".7"
  %".14" = load i32, i32* %".9"
  call void @"Foo__init_hook"(%"Foo"* %".12", i8 %".13", i32 %".14")
  ret void
}

define void @"Bar_print"(%"Bar"* %".1") 
{
entry:
  %".3" = alloca %"Bar"*
  store %"Bar"* %".1", %"Bar"** %".3"
  %".5" = load %"Bar"*, %"Bar"** %".3"
  %".6" = bitcast %"Bar"* %".5" to %"Foo"*
  call void @"Foo_print"(%"Foo"* %".6")
  ret void
}

define void @"Baz__init_hook"(%"Baz"* %".1", %"Foo"* %".2", %"Bar"* %".3") 
{
entry:
  %".5" = alloca %"Baz"*
  store %"Baz"* %".1", %"Baz"** %".5"
  %".7" = alloca %"Foo"*
  store %"Foo"* %".2", %"Foo"** %".7"
  %".9" = alloca %"Bar"*
  store %"Bar"* %".3", %"Bar"** %".9"
  %".11" = load %"Foo"*, %"Foo"** %".7"
  %".12" = load %"Baz"*, %"Baz"** %".5"
  %".13" = getelementptr %"Baz", %"Baz"* %".12", i32 0, i32 0
  store %"Foo"* %".11", %"Foo"** %".13"
  %".15" = load %"Bar"*, %"Bar"** %".9"
  %".16" = load %"Baz"*, %"Baz"** %".5"
  %".17" = getelementptr %"Baz", %"Baz"* %".16", i32 0, i32 1
  store %"Bar"* %".15", %"Bar"** %".17"
  ret void
}

define void @"Baz_print"(%"Baz"* %".1") 
{
entry:
  %".3" = alloca %"Baz"*
  store %"Baz"* %".1", %"Baz"** %".3"
  %".5" = load %"Baz"*, %"Baz"** %".3"
  %".6" = getelementptr %"Baz", %"Baz"* %".5", i32 0, i32 0
  %".7" = load %"Foo"*, %"Foo"** %".6"
  call void @"Foo_print"(%"Foo"* %".7")
  %".9" = load %"Baz"*, %"Baz"** %".3"
  %".10" = getelementptr %"Baz", %"Baz"* %".9", i32 0, i32 1
  %".11" = load %"Bar"*, %"Bar"** %".10"
  call void @"Bar_print"(%"Bar"* %".11")
  ret void
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }