; ModuleID = "tests/test_errors.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = alloca i512
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

define i32 @"a"() 
{
entry:
  ret i32 3
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }