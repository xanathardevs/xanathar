; ModuleID = "tests/test_match.xan"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"(i32 %".1", i8** %".2") 
{
entry:
  %".4" = alloca i32
  store i32 %".1", i32* %".4"
  %".6" = alloca i8**
  store i8** %".2", i8*** %".6"
  %".8" = call i32 @"time"(i32 0)
  call void @"srand"(i32 %".8")
  %".10" = alloca i32
  %".11" = call i32 @"rand"()
  %".12" = srem i32 %".11", 1000000
  store i32 %".12", i32* %".10"
  %".14" = alloca [61 x i8]
  store [61 x i8] [i8 73, i8 39, i8 109, i8 32, i8 116, i8 104, i8 105, i8 110, i8 107, i8 105, i8 110, i8 103, i8 32, i8 111, i8 102, i8 32, i8 97, i8 32, i8 110, i8 117, i8 109, i8 98, i8 101, i8 114, i8 32, i8 102, i8 114, i8 111, i8 109, i8 32, i8 48, i8 32, i8 116, i8 111, i8 32, i8 57, i8 57, i8 57, i8 57, i8 57, i8 57, i8 33, i8 10, i8 84, i8 114, i8 121, i8 32, i8 116, i8 111, i8 32, i8 103, i8 117, i8 101, i8 115, i8 115, i8 32, i8 105, i8 116, i8 33, i8 10, i8 0], [61 x i8]* %".14"
  %".16" = getelementptr [61 x i8], [61 x i8]* %".14", i32 0
  %".17" = getelementptr [61 x i8], [61 x i8]* %".16", i32 0
  %".18" = bitcast [61 x i8]* %".17" to i8*
  %".19" = call i32 (i8*, ...) @"printf"(i8* %".18")
  %".20" = alloca i8*
  %".21" = alloca i8
  %".22" = alloca i32
  %".23" = trunc i32 3 to i8
  store i8 %".23", i8* %".21"
  store i8* %".21", i8** %".20"
  %".26" = alloca i32
  store i32 0, i32* %".26"
  br label %"entry.whileloop0"
entry.whileloop0:
  %".29" = call i8* @"get"(i8* %".21", i32 20)
  %".30" = call i32 @"atoi"(i8* %".21")
  store i32 %".30", i32* %".22"
  %".32" = load i32, i32* %".22"
  %".33" = load i32, i32* %".10"
  %".34" = call i32 @"cmp"(i32 %".32", i32 %".33")
  switch i32 %".34", label %"default-match-0" [i32 0, label %"switchstmt-0-0" i32 1, label %"switchstmt-0-1" i32 -1, label %"switchstmt-0-2"]
entry.endwhile0:
  %".65" = alloca [24 x i8]
  store [24 x i8] [i8 89, i8 111, i8 117, i8 32, i8 119, i8 111, i8 110, i8 32, i8 105, i8 110, i8 32, i8 37, i8 105, i8 32, i8 103, i8 117, i8 101, i8 115, i8 115, i8 101, i8 115, i8 33, i8 10, i8 0], [24 x i8]* %".65"
  %".67" = getelementptr [24 x i8], [24 x i8]* %".65", i32 0
  %".68" = getelementptr [24 x i8], [24 x i8]* %".67", i32 0
  %".69" = bitcast [24 x i8]* %".68" to i8*
  %".70" = load i32, i32* %".26"
  %".71" = call i32 (i8*, ...) @"printf"(i8* %".69", i32 %".70")
  ret void
default-match-0:
  br label %"end-match-0"
end-match-0:
  %".58" = load i32, i32* %".26"
  %".59" = add i32 %".58", 1
  store i32 %".59", i32* %".26"
  %".61" = load i32, i32* %".22"
  %".62" = load i32, i32* %".10"
  %".63" = icmp ne i32 %".61", %".62"
  br i1 %".63", label %"entry.whileloop0", label %"entry.endwhile0"
switchstmt-0-0:
  %".37" = alloca [10 x i8]
  store [10 x i8] [i8 67, i8 111, i8 114, i8 114, i8 101, i8 99, i8 116, i8 33, i8 10, i8 0], [10 x i8]* %".37"
  %".39" = getelementptr [10 x i8], [10 x i8]* %".37", i32 0
  %".40" = getelementptr [10 x i8], [10 x i8]* %".39", i32 0
  %".41" = bitcast [10 x i8]* %".40" to i8*
  %".42" = call i32 (i8*, ...) @"printf"(i8* %".41")
  br label %"end-match-0"
switchstmt-0-1:
  %".44" = alloca [11 x i8]
  store [11 x i8] [i8 84, i8 111, i8 111, i8 32, i8 104, i8 105, i8 103, i8 104, i8 33, i8 10, i8 0], [11 x i8]* %".44"
  %".46" = getelementptr [11 x i8], [11 x i8]* %".44", i32 0
  %".47" = getelementptr [11 x i8], [11 x i8]* %".46", i32 0
  %".48" = bitcast [11 x i8]* %".47" to i8*
  %".49" = call i32 (i8*, ...) @"printf"(i8* %".48")
  br label %"end-match-0"
switchstmt-0-2:
  %".51" = alloca [10 x i8]
  store [10 x i8] [i8 84, i8 111, i8 111, i8 32, i8 108, i8 111, i8 119, i8 33, i8 10, i8 0], [10 x i8]* %".51"
  %".53" = getelementptr [10 x i8], [10 x i8]* %".51", i32 0
  %".54" = getelementptr [10 x i8], [10 x i8]* %".53", i32 0
  %".55" = bitcast [10 x i8]* %".54" to i8*
  %".56" = call i32 (i8*, ...) @"printf"(i8* %".55")
  br label %"end-match-0"
}

declare i32 @"printf"(i8* %".1", ...) 

declare void @"nop"() 

declare i32* @"range"(i32 %".1", i32 %".2") 

declare i32 @"cmp"(i32 %".1", i32 %".2") 

declare void @"inc"(i32* %".1") 

declare void @"dec"(i32* %".1") 

declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

declare i32 @"sprintf"(i8* %".1", i8* %".2", ...) 

declare void @"srand"(i32 %".1") 

declare i32 @"rand"() 

declare i32 @"time"(i32 %".1") 

declare i8* @"ctime"(i32* %".1") 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }