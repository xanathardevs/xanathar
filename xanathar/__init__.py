# LICENSE
# inspiration: https://blog.usejournal.com/writing-your-own-programming-language-and-compiler-with-python-a468970ae6df
__DEBUG__ = False

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

import lark
import lark.lark
import argparse
from xanathar import compiler
from xanathar import logs
from xanathar import strings
from xanathar import preprocessor

if __DEBUG__:
    sys.stdout = open('stdout', 'w')

cdir = os.path.dirname(__file__)
GRAMMAR_FILE = cdir + "/grammar/GRAMMAR.ebnf"
with open(GRAMMAR_FILE) as file:
    lark_parser = lark.lark.Lark(file.read(), propagate_positions=True)

VERSION = "a0.0.1"


def main():
    parser = argparse.ArgumentParser(description='Runs Xanathar.')
    parser.add_argument('-a', dest='ast', action='store_const',
                        const=True, default=False,
                        help='print the ast after parsing')
    parser.add_argument('-s', dest='so', action='store_const',
                        const=True, default=False,
                        help='compile to so')
    parser.add_argument("file")
    parser.add_argument('-o', nargs='?', help='out file for the generated LLVM')
    parser.add_argument('--static-int-alloc', dest='ia', nargs="?", default=32,
                        help="static allocation size (bits) for integers")
    parser.add_argument ('-g', dest='debug', action='store_const',
                         const=True, default=False,
                         help='add debugging info')

    args = parser.parse_args()

    try:
        with open(args.file) as in_file:
            in_file_txt = in_file.read()
            parsed = lark_parser.parse(in_file_txt)
            in_file_txt, flags = preprocessor.process(in_file_txt, parsed)
            parsed = lark_parser.parse (in_file_txt)

            if args.o:
                name = args.o
            else:
                name = in_file.name
            if args.ast:
                print(parsed.pretty())
        compiler.xcompile(parsed, name, int(args.ia), args.so, list(flags), args.file, args.debug)

    except (lark.exceptions.ParseError, lark.exceptions.LarkError) as e:
        logs.log(strings.LEVEL_ERROR, strings.PARSE_ERR)
        sys.stderr.write(str(e))
        exit(-1)


if __name__ == "__main__":
    main()
