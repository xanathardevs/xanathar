/*int* range(const int s, const int e){
    int r[e - s];
    for(int i = s; i < e; i++){
        r[i] = i;
    }
    return r;
}*/

void inc(int* c){
    c++;
}

void dec(int* c){
    c--;
}

int cmp(int a, int b){
    if(a > b){
        return 1;
    } else if(a < b){
        return -1;
    }

    return 0;
}

void nop(){}