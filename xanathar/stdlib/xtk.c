#include<gtk/gtk.h>

GtkWidget* xtk_application_window_new(GtkApplication* app){
    return gtk_application_window_new(app);
}

void xtk_window_set_title(GtkWindow* window, char* title){
    gtk_window_set_title(window, title);
}

void xtk_window_set_default_size(GtkWindow* window, int x, int y){
    gtk_window_set_default_size(window, x, y);
}

void xtk_widget_show_all(GtkWidget* widget){
    gtk_widget_show_all(widget);
}

GtkApplication* xtk_application_new(char* namespace_, int id){
    return gtk_application_new(namespace_, id);
}

void x_signal_connect(GtkApplication* app, char* code, void (*callback)(void), int* something) { // x_signal_connect[$app, "activate" >> 0, $activate, &0];
    g_signal_connect(app, code, G_CALLBACK(callback), something);
}

int x_application_run(GtkApplication* app, int argc, char** argv){
    return g_application_run(G_APPLICATION(app), argc, argv);
}

void x_object_unref(gpointer ptr){
    g_object_unref(ptr);
}

GtkWidget* xtk_button_box_new(int a){
    return gtk_button_box_new(a);
}

void xtk_container_add(GtkContainer* c, GtkWidget* w){
    gtk_container_add(c, w);
}

GtkWidget* xtk_button_new_with_label(char* l){
    return gtk_button_new_with_label(l);
}

GtkWidget* xtk_message_dialog_new(void* parent, int flags, int type, int buttons, char* message_format){
    return gtk_message_dialog_new(parent, flags, type, buttons, message_format);
}

int xtk_dialog_run(GtkDialog* g){
    return gtk_dialog_run(g);
}

void xtk_widget_destroy(GtkWidget* g){
    gtk_widget_destroy(g);
}