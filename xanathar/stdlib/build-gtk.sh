#!/usr/bin/env bash
gcc -shared -o xtk.so -fPIC xtk.c -g -Wall `pkg-config --cflags --libs gtk+-3.0`
