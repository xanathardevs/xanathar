from llvmlite import ir, binding
import sys

# borrowed from https://blog.usejournal.com/writing-your-own-programming-language-and-compiler-with-python-a468970ae6df
# :)


class CodeGen:
    def __init__(self, filename):
        self.binding = binding
        self.binding.initialize()
        self.binding.initialize_native_target()
        self.binding.initialize_native_asmprinter()
        self.binding.initialize_native_asmparser()
        self._config_llvm(filename)
        self._create_execution_engine()
        self._declare_print_function()

    def _config_llvm(self, filename):
        # Config LLVM
        self.module = ir.Module(name=filename)
        self.module.triple = self.binding.get_process_triple()
        parameters_to_main = [ir.IntType(32), ir.IntType(8).as_pointer().as_pointer()]  #
        func_type = ir.FunctionType(ir.VoidType(), parameters_to_main, False)
        base_func = ir.Function(self.module, func_type, name="main")
        self.main = base_func
        block = base_func.append_basic_block(name="entry")
        self.builder = ir.IRBuilder(block)

        argc = self.builder.alloca(ir.IntType(32), name='$argc')
        self.builder.store(base_func.args[0], argc)
        argv = self.builder.alloca(ir.IntType(8).as_pointer().as_pointer(), name='$argv')
        self.builder.store(base_func.args[1], argv)

        self.argc = argc
        self.argv = argv

    def _create_execution_engine(self):
        """
        Create an ExecutionEngine suitable for JIT code generation on
        the host CPU.  The engine is reusable for an arbitrary number of
        modules.
        """
        target = self.binding.Target.from_triple(self.module.triple)
        target_machine = target.create_target_machine()
        # And an execution engine with an empty backing module
        backing_mod = binding.parse_assembly("")
        engine = binding.create_mcjit_compiler(backing_mod, target_machine)
        self.engine = engine

    def _declare_print_function(self):
        # Declare Printf function
        voidptr_ty = ir.IntType(8).as_pointer()
        printf_ty = ir.FunctionType(ir.IntType(32), [voidptr_ty], var_arg=True)
        printf = ir.Function(self.module, printf_ty, name="printf")
        self.printf = printf

    def _compile_ir(self, builder, last_line, main_fn):
        """
        Compile the LLVM IR string with the given engine.
        The compiled module object is returned.
        """
        # Create a LLVM module object from the IR
        self.builder = builder
        # ret_instr = self.builder.ret_void()
        # if last_line and main_fn:
        #     ret_instr.set_metadata('dbg', self.module.add_debug_info('DILocation', {
        #         'line': last_line + 1,
        #         'column': 1,
        #         'scope': main_fn
        #     }))
        self.module.add_named_metadata("gen-by", ["Xanathar"])
        llvm_ir = str(self.module)
        try:
            mod = self.binding.parse_assembly(llvm_ir)
        except RuntimeError as e:
            sys.stderr.write("[ERR] LLVM parsing error!\n")
            sys.stderr.write(str(e))
            if "expected instruction opcode" in str(e):
                sys.stderr.write("\nDid you forget to return from a function?")
            exit(1)

            mod = 0  # Otherwise PyCharm complains about mod's usage below

        mod.verify()
        # Now add the module and make sure it is ready for execution
        self.engine.add_module(mod)
        self.engine.finalize_object()
        self.engine.run_static_constructors()
        return mod

    def create_ir(self, b, last_line=None, main_fn=None):
        pass
        # self._compile_ir(b, last_line, main_fn)

    def save_ir(self, filename):
        with open(filename, 'w') as output_file:
            output_file.write(str(self.module))
