import re

FLAGS_REGEX = re.compile(" *!=! *(.+?) *: *(.+?) *!=! *")
DEFINITIONS = []


def parse(tree):
    s = tree.children[0].value
    match = FLAGS_REGEX.match(s)  # if not match...
    if not match:
        return
    return match.groups()


def run(i):
    if hasattr(i, 'data'):
        if i.data == 'prep':
            yield parse(i)
    if hasattr(i, 'children'):
        for q in i.children:
            yield list(run(q))


def flatten(input_array):  # https://codereview.stackexchange.com/questions/201244/flatten-an-array-in-python
    result_array = []
    for element in input_array:
        if isinstance(element, list):
            result_array += flatten(element)
        else:
            result_array.append(element)
    return result_array


def process(text, parsed):
    # print(parsed)
    flags = flatten(run(parsed))
    flags = [f for f in flags if f]
    return text, flags
