import math
import platform
import random

from llvmlite import ir
from llvmlite import binding
import lark
import collections
import lark.tree
import json
import os
import sys

PREDEFINED_FUNCTIONS = {}
NAMESPACES = {}
VARIABLES = {}
VTABLES = {}
LOCAL_VARS = {}
# MODULES = {
#     'stdio': 'stdlib/stdio.xl',
#     'stdlib': 'stdlib/stdlib.xl'
# }

CLASSES = {}
GENERICS = {}
INSTANTIATED_GENERICS = []

with open(os.path.join(os.path.dirname(__file__), "stdlib/module_registry")) as mods:
    mod_txt = mods.read()
    MODULES = json.loads(mod_txt)


class IntDummy:
    pass


class FloatDummy:
    pass


class UIntDummy(IntDummy):
    pass


class Namespace:
    name = ""
    vars_ = {}
    fns = {}

    def __init__(self, name):
        self.name = name

    def add_var(self, var, res):
        self.vars_[var] = res

    def get_var(self, var):
        return self.vars_[var]

    def add_fn(self, fn, res):
        self.fns[fn] = res

    def get_fn(self, fn):
        return self.fns[fn]


class BasicAst:
    def __init__(self, builder: ir.IRBuilder, module: ir.Module):
        self.builder = builder
        self.module = module

    def eval(self): pass


class BasicAstValue(BasicAst):
    def __init__(self, builder, module, value):
        super().__init__(builder, module)
        self.value = value


class Float(BasicAstValue, FloatDummy):
    def eval(self):
        i = ir.Constant(ir.DoubleType(), float(self.value))
        return i


class String(BasicAstValue):
    def __init__(self, builder, module: ir.Module, value):
        value = value.encode('utf-8').decode('unicode_escape')
        super().__init__(builder, module, value)

        id_ = 'str'
        while 1:
            try:
                module.get_global(id_)
            except KeyError:
                break
            id_ += random.choice('0123456789abcdef')

        self.var_ = ir.GlobalVariable(module, ir.ArrayType(ir.IntType(8), len(self.value)), id_)
        self.var_.linkage = 'internal'
        self.var_.initializer = ir.Constant(ir.ArrayType(ir.IntType(8), len(self.value)),
                                            [ord(x) for x in self.value])

        # self.mem_addr = self.builder.alloca(ir.ArrayType(ir.IntType(8), len(self.value)))
        # self.builder.store(ir.Constant(ir.ArrayType(ir.IntType(8), len(self.value)),
        #                                [ord(x) for x in self.value]), self.mem_addr)
        self.i32_const = ir.Constant(ir.IntType(32), 0)
        self.lead_ptr = self.builder.gep(self.var_, [self.i32_const])
        # print(self.lead_ptr)

        # self.ptrs = []
        # chrs = [ord(x) for x in self.value]
        # for i in chrs:
        #     cptr = builder.alloca(ir.IntType(8))
        #     self.ptrs.append(cptr)
        #     value = ir.Constant(ir.IntType(8), i)
        #     builder.store(value, cptr)

        # self.v = ir.Constant(ir.ArrayType(ir.IntType(8), len(self.value)), [ord(x) for x in self.value])

    def eval(self):
        # i = ir.Constant(ir.ArrayType(ir.IntType(8), len(self.value)), [ord(x) for x in self.value])
        # voidptr_ty = ir.IntType(8).as_pointer()
        # value_ = ir.Constant(ir.IntType(32), 0)
        # value = self.builder.extract_value(self.v, 0)
        # value = self.builder.gep(self.v, [value_, value_])
        # print(value.type)
        return self.lead_ptr
            #self.builder.bitcast(self.lead_ptr, ir.IntType(8).as_pointer())
        # self.builder.bitcast(value, voidptr_ty)

    def __eq__(self, other):
        return self.value == other.value


TYPES = {
    'double': ir.DoubleType(),
    'void': ir.VoidType(),
}

for __bits in [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]:
    globals()["Int" + str(__bits)] = type("Int" + str(__bits), (BasicAstValue,IntDummy),
                                          {'eval': lambda self: ir.Constant(ir.IntType(__bits), int(self.value))})
    globals()["UInt" + str(__bits)] = type("UInt" + str(__bits), (BasicAstValue,UIntDummy),
                                           {'eval': lambda self: ir.Constant(ir.IntType(__bits), abs(int(self.value)))})

    TYPES["int%d" % __bits] = ir.IntType(__bits)
    TYPES["uint%d" % __bits] = ir.IntType(__bits)

    # such dynamic, much usefulness

TYPES['char'] = ir.IntType(8)
arch = platform.architecture()
TYPES['rune'] = ir.IntType(int(''.join(c for c in arch[0] if c.isdigit())))


class BinaryOp(BasicAst):
    def __init__(self, builder, module, left, right):
        super().__init__(builder, module)
        self.left = left
        self.right = right


class Sum(BinaryOp):
    def eval(self):
        l = self.left
        r = self.right
        if isinstance(l, IntDummy) and isinstance(r, IntDummy):
            return self.builder.add(l, r)
        elif isinstance(l, FloatDummy) and isinstance(r, FloatDummy):
            return self.builder.fadd(l, r)


class Sub(BinaryOp):
    def eval(self):
        l = self.left
        r = self.right
        if isinstance(l, IntDummy) and isinstance(r, IntDummy):
            return self.builder.sub(l, r)
        elif isinstance(l, FloatDummy) and isinstance(r, FloatDummy):
            return self.builder.fsub(l, r)


class Mul(BinaryOp):
    def eval(self):
        l = self.left
        r = self.right
        if isinstance(l, IntDummy) and isinstance(r, IntDummy):
            return self.builder.mul(l, r)
        elif isinstance(l, FloatDummy) and isinstance(r, FloatDummy):
            return self.builder.fmul(l, r)


class Div(BinaryOp):
    def eval(self):
        l = self.left
        r = self.right
        if isinstance(l, UIntDummy) and isinstance(r, UIntDummy):
            return self.builder.udiv(l, r)
        elif isinstance(l, FloatDummy) and isinstance(r, FloatDummy):
            return self.builder.fdiv(l, r)
        elif isinstance(l, IntDummy) and isinstance(r, IntDummy):
            return self.builder.sdiv(l, r)


class Mod(BinaryOp):
    def eval(self):
        l = self.left
        r = self.right
        if isinstance(l, UIntDummy) and isinstance(r, UIntDummy):
            return self.builder.urem(l, r)
        elif isinstance(l, FloatDummy) and isinstance(r, FloatDummy):
            return self.builder.frem(l, r)
        elif isinstance(l, IntDummy) and isinstance(r, IntDummy):
            return self.builder.srem(l, r)


class And(BinaryOp):
    def eval(self):
        l = self.left
        r = self.right
        if isinstance(l, IntDummy) and isinstance(r, IntDummy):
            return self.builder.and_(l, r)


class Rol(BasicAst):
    pass


class Ror(BasicAst):
    def __init__(self, build, mod, a1, a2):
        super().__init__(build, mod)
        self.a1 = a1
        self.a2 = a2

    def eval(self):
        if self.a1.type.__class__ not in [ir.IntType, ir.DoubleType]:
            gep = self.builder.gep(self.a1, [self.a2])
            return self.builder.bitcast(gep, ir.PointerType(ir.IntType(8)))
        return self.builder.ashr(self.a1, self.a2)


class Bor(BasicAst):
    pass


class Xor(BasicAst):
    pass


class Not(BasicAst):
    pass


class Rng(BasicAst):
    pass


class Ceq(BasicAst):
    def __init__(self, build, mod, a1, a2):
        super().__init__(build, mod)
        self.a1 = a1
        self.a2 = a2

    def eval(self):
        if self.a1.type.__class__ == ir.IntType:
            return self.builder.icmp_signed("==", self.a1, self.a2)
        elif self.a1.type.__class__ == ir.DoubleType:
            return self.builder.fcmp_ordered("==", self.a1, self.a2)
        else:
            return ir.Constant(ir.IntType(1), self.a1 == self.a2)


class Ois(BasicAst):
    pass


class cast(BasicAst):
    pass


class use_statement(BasicAst):
    pass


class declare_statement(BasicAst):
    def __init__(self, builder, module, variable, type_):
        super().__init__(builder, module)
        self.var = variable
        self.type = type_

    def eval(self):
        super().eval()
        ptr = self.builder.alloca(self.type, name='$' + self.var)
        subvar = Var(self.var, self.type, False, ptr)
        VARIABLES[self.var] = subvar

        return ptr


class variable_phrase(BasicAst):
    def __init__(self, builder, module, var, vap):
        super().__init__(builder, module)
        self.var = var
        self.vap = vap

    def eval(self):
        super().eval()
        var = VARIABLES[self.var]
        if var.lockedQ or self.vap:
            return var.irVar
        return self.builder.load(var.irVar, name='loaded_${0}'.format(self.var))


class as_phrase(BasicAst):
    pass


class variable_setting(BasicAst):
    def __init__(self, builder, module, var, val):
        super().__init__(builder, module)
        self.var = var
        self.val = val

    def eval(self):
        var = VARIABLES[self.var]
        assert not var.lockedQ, "Cannot set a locked variable!"
        try:
            self.builder.store(self.val, var.irVar)
        except TypeError as e:
            sys.stderr.write("[ERR] Invalid types!\n")
            sys.stderr.write(str(e))
            sys.stderr.write("\n\n")
            exit(1)


class fncall(BasicAst):
    def __init__(self, builder, module, fn: ir.Function, vars):
        super().__init__(builder, module)
        self.fn = fn
        self.vars = vars

    def eval(self):
        super().eval()
        return self.builder.call(self.fn, self.vars, name='called_{0}'.format(self.fn.name))


class ptr(BasicAst):
    def __init__(self, builder, module, var):
        super().__init__(builder, module)
        self.var = var

    def eval(self):
        try:
            name = self.var._name
        except:
            name = str(self.var)
        ptr = self.builder.alloca(self.var.type, name='ptr_to_{0}'.format(name))
        self.builder.store(self.var, ptr)
        return ptr


class Deref(BasicAst):
    def __init__(self, builder, module, var: ir.Constant):
        super().__init__(builder, module)
        self.var = var

    def eval(self):
        try:
            name = self.var._name
        except:
            name = ''
        return self.builder.load(self.var, name='derefed_from_{0}'.format(name))


class fn(BasicAst):
    pass


class if_(BasicAst):
    pass


class for_(BasicAst):
    pass


class while_(BasicAst):
    pass


class ret(BasicAst):
    pass


class varspec(BasicAst):
    pass


def printf_(value, builder, module, printf):
    voidptr_ty = ir.IntType(8).as_pointer()
    fmt = "%i \n\0"
    c_fmt = ir.Constant(ir.ArrayType(ir.IntType(8), len(fmt)),
                        bytearray(fmt.encode("utf8")))

    global_fmt = ir.GlobalVariable(module, c_fmt.type, name="fstr")
    global_fmt.linkage = 'internal'
    global_fmt.global_constant = True
    global_fmt.initializer = c_fmt

    fmt_arg = builder.bitcast(global_fmt, voidptr_ty)

    builder.call(printf, [fmt_arg, value], "printf")


class BuildTree(BasicAst):
    phrases = []

    def __init__(self, lark_tree: lark.tree.Tree, module, builder: ir.IRBuilder, main: ir.Function, printf, argc, argv, static_int_alloc, flags, file, debug, noinit=False):
        self.printf = printf
        super().__init__(builder, module)

        for i in lark_tree.children:
            self.phrases.append(i.children[0])

        self.ia = static_int_alloc

        self.ret = ir.VoidType()
        self.old_builder = ir.IRBuilder()
        # self.engine = engine

        self.is_void = True

        self.main = main

        self.cmpops = {
            'leq': '<=',
            'geq': '>=',
            'les': '<',
            'gre': '>',
            'neq': '!=',
        }

        self.LOADED_MODULES = []

        self.header = ''

        self.argc = argc
        self.argv = argv

        self.locks = []

        self.stack = []

        # TODO: parse flags
        flags = dict(flags)
        self.flags = flags["flags"] if "flags" in flags else ""

        self.file = file

        self.debug = debug

        # print(self.module.get_global('llvm.dbg.declare'))

        # print(self.module.declare_intrinsic('llvm.dbg.declare'))

        if debug:
            debug_fn_type = ir.FunctionType(ir.VoidType(), [ir.MetaDataType(), ir.MetaDataType(), ir.MetaDataType()])
            self.debug_fn = ir.Function(self.module, debug_fn_type, 'llvm.dbg.declare')
            self.debug_fn.attributes.add("nounwind")
            self.debug_fn.attributes.add("readnone")

            self.set_fn = ir.Function (self.module, debug_fn_type, 'llvm.dbg.value')
            self.set_fn.attributes.add ("nounwind")
            self.set_fn.attributes.add ("readnone")
        #  self.debug_fn.attributes.add("speculatable")

        # builder.call(printf, [fmt_arg, String(builder, module, "abcd").eval()], "printf")
        # printf_(ir.Constant(ir.IntType(8), 41), builder, module, printf)

        # PARSE lark_tree #

        if not noinit:
            self.setup()

        # POSTINIT

        if debug:
            subfntype = self.type_to_metadata (self.builder.function.type.pointee.return_type)
            #     self.module.add_debug_info ('DIBasicType', {
            #     'name': "int",  # BuildTree.canonicalize(str(self.builder.function.type.pointee.return_type)),
            #     'size': self.get_size (self.builder.function.type, py_int=True),
            #     'encoding': ir.DIToken ('DW_ATE_signed')
            # })
            fntype = self.module.add_debug_info ('DISubroutineType', {
                'types': [subfntype]
            })
            self.subprogram = self.module.add_debug_info ('DISubprogram', {
                'name': self.builder.function.name,
                'scope': self.file_d,
                'file': self.file_d,
                'type': fntype,
                'isLocal': False,
                'isDefinition': True,
                'scopeLine': 1,
                'isOptimized': False,
                'unit': self.compunit,
            }, is_distinct=True)

            self.builder.function.set_metadata('dbg', self.subprogram)

    def __setup_default_namespaces(self):
        #global NAMESPACES
        pass
        #std = Namespace("std")
        #std.add_fn("print", self.printf)
        #NAMESPACES["std"] = std

    def __setup_default_functions(self):
        global PREDEFINED_FUNCTIONS

        #PREDEFINED_FUNCTIONS["print_i"] = self.printf
        #PREDEFINED_FUNCTIONS["print_x"] = self.printf
        #PREDEFINED_FUNCTIONS["print_s"] = self.printf
        #PREDEFINED_FUNCTIONS["print_c"] = self.printf
        PREDEFINED_FUNCTIONS["printf"] = self.printf

        #FILE = self.module.context.get_identified_type("FILE")
        #i8_ptr = ir.IntType(8).as_pointer()
        #i32 = ir.IntType(32)
        #fgets_ty = ir.FunctionType(i8_ptr, [i8_ptr, i32, FILE.as_pointer()])
        #fgets = ir.Function(self.module, fgets_ty, name="fgets")
        #PREDEFINED_FUNCTIONS["fgets"] = fgets

        #fdopen_ty = ir.FunctionType(FILE.as_pointer(), [i32, i8_ptr])
        #fdopen = ir.Function(self.module, fdopen_ty, name="fdopen")
        #PREDEFINED_FUNCTIONS["fdopen"] = fdopen

    def __setup_default_variables(self):
        global VARIABLES

        argc = Var('argc', ir.IntType(32), True, self.argc)
        argv = Var('argv', ir.IntType(8).as_pointer(), True, self.argv)
        VARIABLES['argc'] = argc
        VARIABLES['argv'] = argv
        # FILE = self.module.context.get_identified_type("FILE")
        #fmt_val = ir.Constant(ir.ArrayType(ir.IntType(8), 2), bytearray("r\00".encode('utf-8')))
        #fmt = ir.GlobalVariable(self.module, fmt_val.type, "fdopen_fmt")
        #fmt.linkage = 'internal'
        #fmt.global_constant = True
        #fmt.initializer = fmt_val

        #local_fmt = self.builder.bitcast(fmt, ir.IntType(8).as_pointer())
        #stdin = self.builder.call(PREDEFINED_FUNCTIONS["fdopen"], [ir.IntType(32)(0), local_fmt]) # self.builder.alloca(FILE)
        # self.builder.store(self.builder.call(PREDEFINED_FUNCTIONS["fdopen"], [ir.IntType(32)(0), local_fmt]), stdin)
        #VARIABLES["stdin"] = stdin

        nullptr = ir.IntType(8).as_pointer()

        null_irvar = ir.GlobalVariable (self.module, nullptr, name="NULL")
        null_irvar.linkage = 'internal'
        null_irvar.global_constant = True
        null_irvar.initializer = ir.Constant(ir.IntType(32), 0).inttoptr(nullptr)

        null = Var('null', ir.IntType(8).as_pointer(), True, null_irvar)
        VARIABLES['NULL'] = null

    def __init_hook(self):
        self.voidptr_ty = ir.IntType(8).as_pointer()

        self.global_fmts = {}

        #for fmt_str in "ixsc":
        #    fmt = "%s%%%s \n\0" % ("0x" if fmt_str == 'x' else '', fmt_str)
        #    c_fmt = ir.Constant(ir.ArrayType(ir.IntType(8), len(fmt)),
        #                        bytearray(fmt.encode("utf8")))
        #
        #    global_fmt = ir.GlobalVariable(self.module, c_fmt.type, name="%sstr" % fmt_str)
        #    global_fmt.linkage = 'internal'
        #    global_fmt.global_constant = True
        #    global_fmt.initializer = c_fmt
        #
        #    self.global_fmts[fmt_str] = self.builder.bitcast(global_fmt, self.voidptr_ty)

    def setup(self):
        self.__setup_default_namespaces()
        self.__setup_default_functions()
        self.__setup_default_variables()
        self.__init_hook()
        self.debug_()

    def _parse_comp_op(self, op, i):
        v1 = self.parse_phrase(i.children[0])
        v2 = self.parse_phrase(i.children[1])

        if v1.type.__class__ == ir.IntType:
            return self.builder.icmp_signed(op, v1, v2, name='icmp_{0}'.format(op))
        elif v1.type.__class__ == ir.DoubleType:
            return self.builder.fcmp_ordered(op, v1, v2, name='fcmp_{0}'.format(op))

    def to_type(self, i: str):
        anatomy = ('*' in i, '[' in i)
        if anatomy[0] and anatomy[1]:
            base = TYPES[i.replace('*', '')[:i.index('[') - 1]]
            for q in range(i.count('*')):
                base = ir.PointerType(base)
            base = ir.ArrayType(base, int(i[i.index('['):i.index(']')][1:]))
            return base
        elif anatomy[0]:
            base = TYPES[i.replace ('*', '')]
            for i in range (i.count ('*')):
                base = ir.PointerType (base)
            return base
        elif anatomy[1]:
            base = TYPES[i[:i.index ('[')]]
            base = ir.ArrayType (base, int (i[i.index ('['):i.index (']')][1:]))
            return base
        else:
            return TYPES[i]

    def parse_type(self, t: lark.tree.Tree):
        bldr = ''
        to_process = list(t.children)
        for i in to_process:
            if hasattr(i, 'data'):
                if i.data == 'word':
                    bldr += ''.join(i.children)
                elif i.data == 'typespec':
                    bldr += ''.join(i.children[:-1] + [i.children[-1].children[0]])
                elif i.data == 'number':
                    bldr += '[' + i.children[0] + ']'
            #if isinstance(i, str):
            #    bldr += i
            elif hasattr(i, 'children'):
                to_process += i.children
            elif isinstance(i, str):
                bldr += i
        return self.to_type(bldr)

    def compile(self, name, so):
        sys.stderr.write('---COMPILE---\n')
        if not so:
            command = 'clang {0} -g -fstandalone-debug -fsanitize=address -O0 ' + name + '.ll '
            # command = 'clang {0} ' + name + '.ll '
        else:
            command = 'clang {0} -g -fstandalone-debug -fsanitize=address -O0 -shared -undefined dynamic_lookup ' +\
                      name + '.ll '

        command = command.format(self.flags)

        for i in self.LOADED_MODULES:
            if i["type"] == 'LINKED_SO':
                command += os.path.abspath(i["FILE"]) + ' '
        command = command + '-o ' + name + ('.so' if so else '.o')
        print(command)
        os.system(command)

    def debug_(self):
        if self.debug:
            self.file_d = self.module.add_debug_info ('DIFile', {
                'filename': os.path.split (self.file)[-1],
                'directory': os.sep.join(os.path.split(os.path.abspath (self.file))[:-1])
            })
            import xanathar
            self.compunit = self.module.add_debug_info('DICompileUnit', {
                'language': ir.DIToken('DW_LANG_Python'),
                'file': self.file_d,
                'producer': 'Xanathar v. %s' % xanathar.VERSION,
                'isOptimized': False,
                'runtimeVersion': 0,
                'emissionKind': ir.DIToken('FullDebug'),
                'enums': []
            }, is_distinct=True)

            self.module.add_named_metadata('llvm.dbg.cu', self.compunit)
            i32 = ir.IntType(32)
            a = self.module.add_metadata([i32(2), "Dwarf Version", i32(4)])
            b = self.module.add_metadata([i32 (2), "Debug Info Version", i32 (3)])
            c = self.module.add_metadata([i32(1), "wchar_size", i32(4)])
            self.module.add_named_metadata('llvm.module.flags', a)
            self.module.add_named_metadata('llvm.module.flags', b)
            self.module.add_named_metadata('llvm.module.flags', c)
            self.module.add_named_metadata('llvm.ident', ["Xanathar " + xanathar.VERSION])

    def add_module(self, mod):
        mod = MODULES[mod]
        with open(os.path.abspath(mod)) as mf:
            mod_txt = mf.read()
        module = json.loads(mod_txt)
        if module["type"] == "LINKED_SO":
            binding.load_library_permanently(module["FILE"])

        if module["type"] in ["LINKED_SO", "PURE_LIBC"]:
            # do types
            if "TYPES" in module:
                for ty in module["TYPES"]:
                    types = [self.to_type(q) for q in module["TYPES"][ty]]
                    ir_ty = self.module.context.get_identified_type(ty)

                    ty_ = Class(ty, types, [], False, False, [], self.module)
                    CLASSES[ty] = ty_
                    TYPES[ty] = ir_ty
                    if types:
                        ir_ty.set_body(*types)
                    #     def __init__(self, name, types, inheritance, sealed, static, vtable, module):
            if "FUNCTIONS" in module:
                for k in module["FUNCTIONS"]:
                    va_args = False
                    try:
                        types = [self.to_type(q) for q in module["FUNCTIONS"][k][1]]
                    except KeyError as e:
                        types = [self.to_type(q) for q in module["FUNCTIONS"][k][1][:-1]]
                        va_args = True

                    ftype = ir.FunctionType(self.to_type(module["FUNCTIONS"][k][0]), types, var_arg=va_args)
                    fxn = ir.Function(self.module, ftype, k)
                    fxn.linkage = 'external'
                    PREDEFINED_FUNCTIONS[k] = fxn

            if "VARIABLES" in module:
                for var in module["VARIABLES"]:
                    ty = self.to_type(module["VARIABLES"][var][0])
                    gvar = ir.GlobalVariable(self.module, ty, var)
                    #self.module.add_global(gvar)
                    v = Var(var, ty, False, gvar)
                    gvar.global_constant = True
                    gvar.linkage = 'internal'
                    gvar.initializer = ir.Constant(ty, int(module["VARIABLES"][var][1]))  # TODO: fix

                    VARIABLES[var] = v

        self.LOADED_MODULES.append(module)

    def get_line_no(self, source) -> (int, int):
        return source.line, source.column

    def call(self, tree: lark.tree.Tree, res):
        res.set_metadata('dbg', self.module.add_debug_info('DILocation', {
                'line': tree.meta.line,
                'column': tree.meta.column,
                'scope': self.subprogram
        }))

        return res

    def type_to_metadata(self, ty):
        if isinstance(ty, ir.IdentifiedStructType):
            return self.module.add_debug_info('DICompositeType', {
                'tag': ir.DIToken('DW_TAG_structure_type'),
                'file': self.file_d,
                'line': 1,  # TODO: get real line
                'size': self.get_size(ty, py_int=True) * 8,
                'elements': [
                    self.type_to_metadata(x) for x in ty.elements
                ]
            })
        elif isinstance(ty, ir.IntType):
            return self.module.add_debug_info('DIBasicType', {
                'name': str(ty),
                'size': self.get_size(ty, py_int=True) * 8,
                'encoding': ir.DIToken('DW_ATE_signed')
            })
        elif isinstance(ty, ir.VoidType):
            return self.module.add_debug_info('DIBasicType', {
                'name': 'void',
                'size': 8,
                'encoding': ir.DIToken('DW_ATE_signed')
            })
        elif isinstance(ty, ir.DoubleType):
            return self.module.add_debug_info ('DIBasicType', {
                'name': str (ty),
                'size': self.get_size (ty, py_int=True) * 8,
                'encoding': ir.DIToken ('DW_ATE_float')
            })
        elif isinstance(ty, ir.PointerType):
            return self.module.add_debug_info("DIDerivedType", {
                'tag': ir.DIToken('DW_TAG_pointer_type'),
                'baseType': self.type_to_metadata(ty.pointee),
                'size': self.get_size(ty, py_int=True) * 8
            })

    def get_size(self, ty, py_int=False):
        i32 = ir.IntType(32) if not py_int else lambda x: x
        return i32(ty.get_abi_size(binding.create_target_data(self.module.data_layout)))
        #
        #
        # i32 = (ir.IntType (32) if not py_int else lambda x: x)
        #
        # if isinstance (ty, ir.IntType):
        #     return i32 (math.ceil(ty.width / 8))
        # elif isinstance (ty, ir.DoubleType):
        #     return i32 (64)
        # elif isinstance (ty, ir.IdentifiedStructType):
        #     return i32(sum (map (lambda x: self.get_size(x, py_int=True), ty.elements)))
        # elif isinstance (ty, ir.PointerType):
        #     return i32 (32)

    @staticmethod
    def canonicalize(s):
        s = str(s)
        if s[0] == '%':
            s = s[1:]
        if (s[0] == '\'' and s[-1] == '\'') or (s[0] == '"' and s[-1] == '"'):
            s = s[1:-1]
        return s

    def parse_ref(self, i, as_ptr=False):
        v1 = self.parse_phrase (i.children[0])
        try:
            dict_ = CLASSES[v1.type.pointee].types
        except Exception as e:
            print (i.pretty ())
            raise e
        addr = ir.IntType (32) (0)
        idx = ir.IntType (32) (list (dict_.keys ()).index (i.children[1].children[0].children[0].value))
        elementptr = self.builder.gep (v1, [addr, idx])#, name='elementptr_of_{0}_at_{1}_struct_idx_{2}'.format(v1._name, addr, idx))
        if as_ptr:
            return elementptr
        loaded_ptr = self.builder.load (elementptr, name='loaded_${0}'.format(elementptr._name))
        return loaded_ptr

    def cast(self, val_to_cast, t1, t2):
        if t1.__class__ == ir.IntType and t2.__class__ == ir.DoubleType:
            return self.builder.sitofp (val_to_cast, ir.DoubleType ())
        elif t1.__class__ == ir.DoubleType and t2.__class__ == ir.IntType:
            return self.builder.fptosi (val_to_cast, t2)
        elif t1.__class__ == ir.IntType and t2.__class__ == ir.IntType:
            if t1.width > t2.width:
                return self.builder.trunc (val_to_cast, t2)
            elif t1.width < t2.width:
                return self.builder.zext (val_to_cast, t2)
            else:
                return val_to_cast
        elif t1.__class__ == ir.DoubleType and t2.__class__ == ir.DoubleType:
            return val_to_cast
        elif t1.__class__ == ir.PointerType and t2.__class__ == ir.PointerType:
            return self.builder.bitcast (val_to_cast, t2)

        return val_to_cast

    def parse_phrase(self, i, suggested_type=None, variable_as_ptr=False):
        global VARIABLES, TYPES, CLASSES, LOCAL_VARS

        if i.data == 'declare_statement':
            var = i.children[1].children[0].children[0].value
            try:
                ty = self.parse_type(i.children[2])

                # if isinstance(ty, ir.ArrayType):
                #     print('Array')
                #     ptr = ir.IRBuilder().call('fn', [], 'malloc')#self.builder.call()
                #     subvar = Var (var, ty, False, ptr)
                #     VARIABLES[var] = subvar
                #
                #     return

                alloca = declare_statement(self.builder, self.module, var, ty).eval()
            except AttributeError:
                ty = TYPES[i.children[1].children[0].children[1].children[0].children[0].value]
                for t in i.children[2].children[0].children[0].value:
                    if t == '*':
                        ty = ir.PointerType(ty)
                alloca = declare_statement(self.builder, self.module, var,
                                           ty).eval()

            if len(i.children) > 3:
                self.builder.store(self.parse_phrase(i.children[3]), VARIABLES[var].irVar)

            for q in i.children[0].children:
                if q.value == 'locked':
                    self.locks.append(var)

            # Debug Info

            if self.debug:
                ty = self.type_to_metadata(ty)
                metadata = self.module.add_debug_info ('DILocalVariable', {
                    "name": var,
                    "scope": self.subprogram,
                    "file": self.file_d,
                    "line": 1,
                    "type": ty
                })

                # TODO: fix all this
                # self.builder.function.set_metadata("dbg", self.subprogram)

                LOCAL_VARS[var] = metadata

                #self.declare_debug(alloca, metadata)
                null_expr = self.module.add_debug_info("DIExpression", {})
                null_expr.parent = alloca
                call = self.builder.call (self.debug_fn, [ir.MetaDataArgument(alloca),
                                                          metadata,
                                                          null_expr])

                #print(call)
                call.set_metadata("dbg", self.module.add_debug_info('DILocation', {
                    'line': i.meta.line,
                    'column': i.meta.column,
                    'scope': self.subprogram
                }))
            #print(call)

        elif i.data == 'fncall':
            if i.children[0].data == 'word':
                fnname = i.children[0].children[0]
                fn = PREDEFINED_FUNCTIONS[i.children[0].children[0].value]
            elif i.children[0].data == 'spec':
                v1 = self.parse_phrase(i.children[0].children[0])
                fnname = i.children[0].children[-1].children[0]
                cname = self.canonicalize(str(v1.type.pointee))
                try:
                    fn = PREDEFINED_FUNCTIONS[cname + '_' + fnname]
                except KeyError:
                    for x in CLASSES[cname].inheritance:
                        try:
                            fn = PREDEFINED_FUNCTIONS[self.canonicalize(str(x)) + '_' + fnname]
                            break
                        except KeyError:
                            continue
            else:
                return self.parse_phrase(i.children[0])

            # if 'print' in fnname and fnname[-1] in self.global_fmts:
            #     args = [self.global_fmts[fnname.value[-1]]] + [self.parse_phrase(x.children[0]) for x in i.children[1:]]
            # else:
            typs = [a.type for a in fn.args]
            if fn.type.pointee.var_arg:
                for q in range(len(i.children) + 1):
                    typs.append(None)

            args = [self.parse_phrase(i.children[x].children[0],
                                      suggested_type=typs[x - 1]
                                      )
                    for x in range(1, len(i.children))]

            if i.children[0].data == 'spec':
                args = [v1] + args

            res = fncall(self.builder, self.module, fn, args).eval()
            if self.debug:
                res.set_metadata('dbg', self.module.add_debug_info('DILocation', {
                    'line': i.meta.line,
                    'column': i.meta.column,
                    'scope': self.subprogram
                }))
            return res
        elif i.data == 'variable_phrase':
            res = variable_phrase(self.builder, self.module, i.children[0].children[0].value, variable_as_ptr).eval()
            if suggested_type:
                return self.cast(res, res.type, suggested_type)
            return res
        elif i.data == 'variable_setting':
            var = i.children[0]
            if len(i.children) == 2:
                if var.data == 'variable_phrase':
                    vname = var.children[0].children[0].value
                    assert vname not in self.locks, "Cannot set locked variable $%s!" % vname
                    val = self.parse_phrase(i.children[1], suggested_type=VARIABLES[vname].irVar.type.pointee)
                    if self.debug and vname in LOCAL_VARS:
                        self.builder.call (self.set_fn, [
                            ir.MetaDataArgument (val),
                            LOCAL_VARS[vname],
                            self.module.add_debug_info ("DIExpression", {})
                        ]).set_metadata ("dbg", self.module.add_debug_info ('DILocation', {
                            'line': i.meta.line,
                            'column': i.meta.column,
                            'scope': self.subprogram
                        }))
                    return variable_setting(self.builder, self.module, i.children[0].children[0].children[0].value,
                                            val).eval()
                elif var.data == 'ref':
                    var = self.parse_ref(var, as_ptr=True)
                    val = self.parse_phrase(i.children[1], suggested_type=var.type.pointee)
                    self.builder.store(val, var)
            elif len(i.children) == 3:
                if var.data == 'variable_phrase':
                    vname = var.children[0].children[0].value
                    assert vname not in self.locks, "Cannot set locked variable $%s!" % vname
                    val = self.parse_phrase(i.children[2])
                    bitcast_ty = val.type.as_pointer()
                    geped = self.builder.gep(VARIABLES[vname].irVar, [self.parse_phrase(i.children[1])],
                                             name='geped_from_{0}'.format(vname))
                    self.builder.store(val, self.builder.bitcast(geped, bitcast_ty))
                    #return variable_setting(self.builder, self.module, i.children[0].children[0].children[0].value,
                    #                        self.parse_phrase(i.children[2])).eval()
                elif var.data == 'ref':  # TODO: test
                    val = self.parse_phrase(i.children[2])
                    var = self.parse_ref(var, as_ptr=True)
                    var = self.builder.gep(var, [self.parse_phrase(i.children[1])])
                    self.builder.store(val, var)
        elif i.data == 'ptr':
            c = self.parse_phrase(i.children[0], suggested_type=None, variable_as_ptr=True)
            assert isinstance(c.type, ir.PointerType), "Unary operator '&' requires non-constant operand."
            if not suggested_type:
                return c
            return self.builder.bitcast(c, suggested_type)
        elif i.data == 'deref':
            return Deref(self.builder, self.module, self.parse_phrase(i.children[0])).eval()
        elif i.data == 'expression' or i.data == 'phrase':
            return self.parse_phrase(i.children[0], suggested_type=suggested_type, variable_as_ptr=variable_as_ptr)
        elif i.data == 'number':
            cnst = ir.Constant(ir.IntType(self.ia), int(i.children[0]))
            if suggested_type:
                return self.cast(cnst, cnst.type, suggested_type)
            return cnst
        elif i.data == 'float':
            cnst = ir.Constant(ir.DoubleType(), float(i.children[0]))
            if suggested_type:
                return self.cast(cnst, cnst.type, suggested_type)
            return cnst
        elif i.data == 'cast':
            val_to_cast = self.parse_phrase(i.children[0])
            t1 = val_to_cast.type
            t2 = self.parse_type(i.children[1].children[0])
            return self.cast(val_to_cast, t1, t2)
        elif i.data == 'str':
            s = String(self.builder, self.module, i.children[0].value[1:-1] + '\00').eval()
            if suggested_type:
                return self.builder.bitcast(s, suggested_type)
            return s
        elif i.data == 'ror':
            v1 = self.parse_phrase(i.children[0])
            return Ror(self.builder, self.module, v1,
                       self.parse_phrase(i.children[1])).eval()
        elif i.data == 'ceq':
            v1 = self.parse_phrase(i.children[0])
            return Ceq(self.builder, self.module, v1,
                       self.parse_phrase(i.children[1], suggested_type=v1.type)).eval()
        elif i.data == 'mod':
            v1 = self.parse_phrase(i.children[0])
            return self.builder.srem(v1, self.parse_phrase(i.children[1], suggested_type=v1.type))
        elif i.data == 'fn':
            # get type
            ret_str = i.children[2].children[0].children[0].value
            ret = self.parse_type(i.children[2].children[0])
            self.ret = ret
            self.is_void = (ret_str == 'void')
            args = []
            vars_ = []
            for j in i.children[1].children:
                if j.data == 'as_phrase':
                    t = j.children[0]
                    args.append(self.parse_type(t))
                else:
                    vars_.append(j.children[0].children[0].value)
            ftype = ir.FunctionType(ret, args)
            name = i.children[0].children[0].value
            f = ir.Function(self.module, ftype, self.header + name)
            f.attributes.add('noinline')
            PREDEFINED_FUNCTIONS[self.header + name] = f
            block = f.append_basic_block("entry")
            b = ir.IRBuilder(block)

            for q in range(len(f.args)):
                ptrl = b.alloca(f.args[q].type, name='$' + vars_[q])
                b.store(f.args[q], ptrl)
                VARIABLES[vars_[q]] = Var(vars_[q], ptrl.type.pointee, False, ptrl)

            old_builder = self.builder
            old_VARIABLES = VARIABLES
            if self.debug:
                old_subprogram = self.subprogram
            self.builder = b

            ty = self.builder.function.type
            while isinstance(ty, ir.PointerType):
                ty = ty.pointee
            if self.debug:
                subfntype = self.type_to_metadata (ty.return_type)
                fntype = self.module.add_debug_info ('DISubroutineType', {
                    'types': [subfntype]
                })
                self.subprogram = self.module.add_debug_info ('DISubprogram', {
                    'name': self.builder.function.name,
                    'scope': self.file_d,
                    'file': self.file_d,
                    'type': fntype,
                    'isLocal': False,
                    'isDefinition': True,
                    'scopeLine': 1,
                    'isOptimized': False,
                    'unit': self.compunit,
                }, is_distinct=True)

            for q in i.children[3:]:
                self.parse_phrase(q.children[0])

            self.builder = old_builder
            VARIABLES = old_VARIABLES

            if self.debug:
                f.set_metadata('dbg', self.subprogram)

                self.subprogram = old_subprogram

            if self.header == '':
                VARIABLES[name] = Var(name, f.type, True, f)

            return f
        elif i.data == 'if':
            ret_if = False
            ret_else = False
            with self.builder.if_else(self.parse_phrase(i.children[0])) as (ifs, other):
                if_statements = i.children[1:-1]
                else_statements = []
                if i.children[-1].data == 'else':
                    else_statements = i.children[-1].children
                else:
                    if_statements.append(i.children[-1])
                with ifs:
                    for stmt in if_statements:
                        if stmt.children[0].data == 'ret':
                            ret_if = True
                        self.parse_phrase(stmt.children[0])
                with other:
                    for stmt in else_statements:
                        if stmt.children[0].data == 'ret':
                            ret_else = True
                        self.parse_phrase(stmt.children[0])

            if ret_if and ret_else:
                self.builder.unreachable()
        elif i.data == 'ret':
            if self.is_void:
                self.builder.ret_void()
            else:
                val = self.parse_phrase(i.children[0])
                self.builder.ret(val)
        elif i.data == 'add':
            v1 = self.parse_phrase (i.children[0])
            v2 = self.parse_phrase (i.children[1], suggested_type=v1.type)

            v1_struct = isinstance(v1.type, ir.IdentifiedStructType)
            v2_struct = isinstance(v2.type, ir.IdentifiedStructType)

            if v1_struct:
                resu = self.builder.call(PREDEFINED_FUNCTIONS[self.canonicalize(str(v1.type)) + '_hook_+'],
                                         [self.parse_phrase (i.children[0], suggested_type=None,
                                                             variable_as_ptr=True), v2],
                                         '{0} + {1}'.format(self.canonicalize(v1.type), self.canonicalize(v2.type)))
                resu.set_metadata('dbg', self.module.add_debug_info('DILocation', {
                    'scope': self.subprogram,
                    'line': i.meta.line,
                    'column': i.meta.column
                }))
                return resu
            elif v2_struct:
                resu = self.builder.call (PREDEFINED_FUNCTIONS[self.canonicalize (str (v2.type)) + '_hook_+'],
                                          [self.parse_phrase (i.children[1], suggested_type=None,
                                                              variable_as_ptr=True), v1],
                                          '{0} + {1}'.format(self.canonicalize(v1.type), self.canonicalize(v2.type)))
                resu.set_metadata ('dbg', self.module.add_debug_info ('DILocation', {
                    'scope': self.subprogram,
                    'line': i.meta.line,
                    'column': i.meta.column
                }))
                return resu
            else:
                if isinstance(v1.type, ir.IntType):
                    return self.builder.add(v1, v2)
                elif isinstance(v1.type, ir.DoubleType):
                    return self.builder.fadd(v1, v2)
                elif isinstance(v1.type, ir.PointerType):
                    if isinstance(v2.type, ir.PointerType):
                        return self.builder.inttoptr(self.builder.add(self.builder.ptrtoint(v1, TYPES['rune']),
                                                     self.builder.ptrtoint(v2, TYPES['rune'])),
                                                     ir.IntType(8).as_pointer())
                    return self.builder.inttoptr(self.builder.add(self.builder.ptrtoint(v1, TYPES['rune']), v2),
                                                 ir.IntType(8).as_pointer())
        elif i.data == 'sub':  # TODO: to hook
            v1 = self.parse_phrase (i.children[0])
            v2 = self.parse_phrase (i.children[1], suggested_type=v1.type)

            if v1.type.__class__ == ir.IntType:
                return self.builder.sub(v1, v2)
            else:
                sub = self.builder.fsub(v1, v2)
                return sub
        elif i.data == 'mul':
            v1 = self.parse_phrase (i.children[0])
            v2 = self.parse_phrase (i.children[1], suggested_type=v1.type)

            if v1.type.__class__ == ir.IntType:
                return self.builder.mul(v1, v2)
            else:
                mul = self.builder.fmul(v1, v2)
                return mul
        elif i.data == 'div':
            v1 = self.parse_phrase(i.children[0])
            v2 = self.parse_phrase(i.children[1], suggested_type=v1.type)

            if v1.type.__class__ == ir.IntType:
                return self.builder.sdiv(v1, v2)  # TODO: for div and mod, include s/u/f
            else:
                division = self.builder.fdiv(v1, v2)
                return division
        elif i.data == 'and':
            v1 = self.parse_phrase (i.children[0])
            v2 = self.parse_phrase (i.children[1], suggested_type=v1.type)

            return self.builder.and_(v1, v2)
        elif i.data == 'for':
            p1 = i.children[0].children[0]
            e = i.children[1].children[0]
            p2 = i.children[2].children[0]

            try:
                phrases = i.children[3:]
            except IndexError:
                return

            id_ = 0
            for q in self.builder.function.basic_blocks:
                if q.name == 'forloop-' + str(id_):
                    id_ += 1

            curr_for = self.builder.append_basic_block("forloop-" + str(id_))
            end_for = self.builder.append_basic_block("endfor-" + str(id_))
            self.curr_loop = curr_for
            self.end_loop = end_for

            self.parse_phrase(p1)
            self.builder.branch(curr_for)
            self.builder = ir.IRBuilder(curr_for)

            for q in phrases:
                self.parse_phrase(q.children[0])

            self.parse_phrase(p2)
            self.builder.cbranch(self.parse_phrase(e), curr_for, end_for)

            self.builder = ir.IRBuilder(end_for)

        elif i.data == 'while':
            value = i.children[0]

            try:
                phrases = i.children[1:]
            except IndexError:
                return

            id_ = 0
            for q in self.builder.function.basic_blocks:
                if q.name == 'whileloop-' + str(id_):
                    id_ += 1

            curr_while = self.builder.function.append_basic_block("entry.whileloop" + str(id_))
            end_while = self.builder.function.append_basic_block("entry.endwhile" + str(id_))
            self.curr_loop = curr_while
            self.end_loop = end_while
            self.builder.branch(curr_while)
            self.builder = ir.IRBuilder(curr_while)

            for q in phrases:
                self.parse_phrase(q.children[0])

            self.builder.cbranch(self.parse_phrase(value), curr_while, end_while)

            self.builder = ir.IRBuilder(end_while)
        elif i.data == 'les' or i.data == 'gre' or i.data == 'leq' or i.data == 'geq' or i.data == 'neq':
            return self._parse_comp_op(self.cmpops[i.data], i)
        elif i.data == 'rol':
            return self.builder.lshr(self.parse_phrase(i.children[0]), self.parse_phrase(i.children[1]))
        elif i.data == 'bor':
            return self.builder.or_(self.parse_phrase(i.children[0]), self.parse_phrase(i.children[1]))
        elif i.data == 'xor':
            return self.builder.xor(self.parse_phrase(i.children[0]), self.parse_phrase(i.children[1]))
        elif i.data == 'not':
            return self.builder.not_(self.parse_phrase(i.children[0]))
        elif i.data == 'use_statement':
            self.add_module(i.children[0].children[0].value)
        elif i.data == 'raw_var':
            return VARIABLES[i.children[0].children[0].value].irVar
        elif i.data == 'match':
            exp = self.parse_phrase(i.children[0])
            arms = i.children[1:]

            id_ = 0
            for q in self.builder.function.basic_blocks:
                if q.name == 'default-match-' + str(id_):
                    id_ += 1

            default_block = self.builder.function.append_basic_block('default-match-' + str(id_))
            endmatch = self.builder.function.append_basic_block('end-match-' + str(id_))

            if arms[-1].data == 'default_arm':
                default_arm = arms[-1]
                arms = arms[:-1]

                self.old_builder = self.builder
                self.builder = ir.IRBuilder(default_block)

                self.parse_phrase(default_arm)
                self.builder.branch(endmatch)

            switch_stmt = self.old_builder.switch(exp, default_block)

            stmt_id = 0

            for q in arms:
                curr_arm_block = self.builder.function.append_basic_block('switchstmt-' + str(id_) + '-' + str(stmt_id))
                stmt_id += 1
                switch_stmt.add_case(self.parse_phrase(q.children[0]), curr_arm_block)
                self.builder = ir.IRBuilder(curr_arm_block)
                self.parse_phrase(q.children[1].children[0])
                self.builder.branch(endmatch)

            self.builder = ir.IRBuilder(endmatch)
        elif i.data == 'skip':
            self.builder.branch(self.curr_loop)
        elif i.data == 'break':
            self.builder.branch(self.end_loop)
        elif i.data == 'class':
            sealed = False
            static = False

            for token in i.children[0].children:
                if token.value == 'sealed':
                    sealed = True
                elif token.value == 'static':
                    static = True

            cname = i.children[1].children[0].value
            types = collections.OrderedDict()

            for q in i.children[2].children:
                types[q.children[0].children[0].children[0].value] = \
                    self.parse_type (q.children[1].children[0])
            inheritance = [x.children[0].value for x in i.children[3].children]
            vtable = []
            interface_inheritance = [x.children[0].value for x in i.children[4].children] \
                if i.children[4].data == 'inheritance' else []

            for interface in interface_inheritance:
                interface_value = CLASSES[interface]
                vtable.extend(interface_value.functions)

            id_ = 0
            for x in inheritance:
                assert CLASSES[x].can_be_inherited(), \
                    "Type %s is static or sealed and cannot be inherited" % CLASSES[x].name
                for t in CLASSES[x].types:
                    types[t] = CLASSES[x].types[t]
                    id_ += 1

            inheritance = [TYPES[x] for x in inheritance]# + [TYPES[x] for x in interface_inheritance]

            CLASS = self.module.context.get_identified_type (cname)
            CLASS.set_body (*types.values ())
            TYPES[cname] = CLASS
            class_ = Class(cname, types, inheritance, sealed, static, vtable, self.module)
            CLASSES[CLASS] = class_
            CLASSES[cname] = class_

            # Commit vtable

            vtable_types = list(map(InterfaceFn.get_fntype, vtable))
            for k in inheritance:
                vtable_types.extend(CLASSES[k].get_virt_fns())

            for q in range(len(vtable_types)):
                vtable_types[q] = ir.FunctionType(vtable_types[q].pointee.return_type,
                                                  [self.module.context.get_identified_type(cname).as_pointer()]
                                                  + list(vtable_types[q].pointee.args)).as_pointer()

            vtable_ir = self.module.context.get_identified_type(cname + '_vtable')
            vtable_ir.set_body(*vtable_types)

            self.header = cname + '_'

            for q in i.children[4:]:
                ret = self.parse_phrase(q.children[0])
                if q.children[0].data in ('override',):
                    CLASSES[cname].publish_function(ret)

            CLASSES[cname].publish_vtable()
            self.header = ''
            #VARIABLES[cname] = Var(cname, CLASS, True, CLASS)

        elif i.data == 'ref':
            v1 = self.parse_phrase(i.children[0])
            try:
                dict_ = CLASSES[v1.type.pointee].types
            except Exception as e:
                raise e
            addr = ir.IntType(32)(0)
            idx = ir.IntType(32)(list(dict_.keys()).index(i.children[1].children[0].children[0].value))
            elementptr = self.builder.gep(v1, [addr, idx])
            if i.children[1].data == 'raw_var':
                return elementptr
            loaded_ptr = self.builder.load(elementptr)
            return loaded_ptr
        elif i.data == 'new':
            variable = False
            var = i.children[0].children[0]#.children[0]
            if var.data == 'variable_phrase':
                var = var.children[0] # ??
                var = var.children[0]
                variable = True

            cname = i.children[1].children[0].children[0]
            assert not CLASSES[cname].sealed, "You cannot instantiate a sealed class!"
            fnname = str(cname) + "__init_hook"
            args = [self.parse_phrase(x.children[0]) for x in i.children[1].children[1:]]
            if variable:
                args = [VARIABLES[var].irVar] + args
            else:
                args = [self.parse_phrase(var)] + args
            fn = PREDEFINED_FUNCTIONS[fnname]

            res = fncall (self.builder, self.module, fn, args).eval ()
            if self.debug:
                res.set_metadata ('dbg', self.module.add_debug_info ('DILocation', {
                    'line': i.meta.line,
                    'column': i.meta.column,
                    'scope': self.subprogram
                }))
                #return res

                #res = self.builder.call(fn, args, name='new_{0}'.format(cname))

                step1 = {
                    'line': i.meta.line,
                    'column': i.meta.column,
                    'scope': self.subprogram
                }

                #step2 = self.module.add_debug_info('DILocation', step1)

                #print("Generating debugging info for " + str(res))  # If I remove these print statements, I get
                #print("Debugging info: " + str(step2))  # /root/miniconda3/conda-bld/llvmdev_1531160641630/work/include/llvm/Support/Casting.h:106: static bool llvm::isa_impl_cl<To, const From*>::doit(const From*) [with To = llvm::DIBasicType; From = llvm::Metadata]: Assertion `Val && "isa<> used on a null pointer"' failed.

                res.set_metadata('dbg', self.module.add_debug_info('DILocation', {
                    'line': i.meta.line,
                    'column': i.meta.column,
                    'scope': self.subprogram
                }))
            #print(res)
            return res
        elif i.data == 'call_sealed_class':
            j = i.children[-1]
            cname = i.children[0].children[0].value
            assert CLASSES[cname].sealed, "Calling a method from a non-sealed class in a sealed context is invalid."
            j.children[0].children[0].value = '_'.join([q.children[0].value for q in i.children[:-1]] +
                                                       [j.children[0].children[0].value])
            return self.parse_phrase(j)
        elif i.data == 'lock':
            self.locks.append(i.children[0].children[0].children[0].value)
        elif i.data == 'unlock':
            self.locks.remove(i.children[0].children[0].children[0].value)
        elif i.data == 'interface':
            iname = i.children[0].children[0].value
            vtable = []
            vtable_type = self.module.context.get_identified_type(iname + '_vtable')

            for q in i.children[1:]:
                vtable.append(InterfaceFn(q.children[0].children[0].value,
                                          list(map(self.parse_type, q.children[1:-1])),
                                          self.parse_type(q.children[-1])))

            vtable_type_body = list(map(InterfaceFn.get_fntype, vtable))
            vtable_type.set_body(*vtable_type_body)

            CLASSES[iname] = Interface(iname, vtable, self.module)
            it = self.module.context.get_identified_type(iname)
            it.set_body()
            TYPES[iname] = it

            # vtable_initializer = ir.Constant(vtable_type, i)
            #
            # global_vtable = ir.GlobalVariable(self.module, vtable_type, name="%s_vtable_value" % iname)
            # global_vtable.linkage = 'internal'
            # global_vtable.global_constant = True
            # global_vtable.initializer = vtable_initializer
        elif i.data == 'virtual_fn':
            #print(i.pretty())
            pass
        elif i.data == 'override':
            name = i.children[0].children[-1].children[0].value
            cname = i.children[0].children[0].children[0].value

            functions = CLASSES[cname].functions
            q = 0
            for q in range(len(functions)):
                if functions[q].name == name:
                    break

            ctype = self.module.context.get_identified_type(cname + '_vtable')
            proto = ctype.elements[q]
            nargs = tuple([self.module.context.get_identified_type(self.header[:-1]).as_pointer()] +
                          list(proto.pointee.args))
            proto = ir.FunctionType(proto.pointee.return_type, nargs).as_pointer()

            phrases = i.children[2:]

            ret = proto.pointee.return_type
            self.ret = ret
            self.is_void = ret.__class__ == ir.VoidType

            vars_ = ["self"] + [q.children[0].children[0].value for q in i.children[1].children]

            f = ir.Function (self.module, proto.pointee, self.header + name)
            PREDEFINED_FUNCTIONS[self.header + name] = f
            block = f.append_basic_block ("entry")
            b = ir.IRBuilder (block)

            self.old_builder = self.builder
            old_VARIABLES = VARIABLES
            for q in range (len (f.args)):
                ptrl = b.alloca (f.args[q].type)
                b.store (f.args[q], ptrl)
                VARIABLES[vars_[q]] = Var (vars_[q], ptrl.type.pointee, False, ptrl)
            self.builder = b

            for q in phrases:
                self.parse_phrase (q.children[0])

            self.builder = self.old_builder
            VARIABLES = old_VARIABLES

            return f

        # NEW to add: interface, interface_fn, override_fn, virtual_fn
        # NEW to fix: inheritance
        elif i.data == 'arr_idx':
            # var = VARIABLES[i.children[0].children[0].children[0].children[0].value].irVar
            try:
                var = self.parse_phrase (i.children[0].children[0], variable_as_ptr=True)
                ty = var.type.pointee.element.as_pointer()
            except AttributeError:
                var = self.parse_phrase (i.children[0].children[0], variable_as_ptr=False)
                ty = var.type
            r = self.builder.bitcast(self.builder.gep(var, [self.parse_phrase(i.children[1])]), ty)
            return r
        elif i.data == 'let':
            vname = i.children[0].children[0].children[0].value
            val = self.parse_phrase(i.children[1])
            var = self.builder.alloca(val.type)
            self.builder.store(val, var)
            VARIABLES[vname] = Var(vname, val.type, False, var)
        elif i.data == 'lambda':
            print(i.pretty())

            # TODO: implement
        elif i.data == 'template':
            classes = [x.children[0].children[0].value for x in i.children[0].children]
            name = i.children[1].children[0].value
            params = i.children[2].children
            ret = i.children[3]
            phrases = i.children[4:]

            GENERICS[name] = Generic(classes, name, params, ret, phrases)
        elif i.data == 'generic_call':
            name = i.children[0].children[0].value
            gp = i.children[1]
            args_ = i.children[2:]

            gp_str = ''.join([x.children[0].value for x in gp.children])
            gen_name = name + '_' + gp_str

            GENERIC = GENERICS[name]
            old_TYPES = TYPES
            for q in range(len(GENERIC.classes)):
                TYPES[GENERIC.classes[q]] = self.parse_type(gp.children[q])
            # UPDATE TYPES
            # COPIED FROM elif i.data == 'fn'

            # get type
            ret_str = GENERIC.ret
            ret = self.parse_type (GENERIC.ret)
            self.ret = ret
            self.is_void = (ret_str == 'void')
            args = []
            vars_ = []
            for j in GENERIC.params:
                if j.data == 'as_phrase':
                    t = j.children[0]
                    args.append (self.parse_type (t))
                else:
                    vars_.append (j.children[0].children[0].value)  #
            ftype = ir.FunctionType (ret, args)
            name = gen_name
            f = ir.Function (self.module, ftype, self.header + name)
            PREDEFINED_FUNCTIONS[self.header + name] = f
            block = f.append_basic_block ("entry")
            b = ir.IRBuilder (block)

            for q in range (len (f.args)):
                ptrl = b.alloca (f.args[q].type)
                b.store (f.args[q], ptrl)
                VARIABLES[vars_[q]] = Var (vars_[q], ptrl.type.pointee, False, ptrl)

            self.old_builder = self.builder
            old_VARIABLES = VARIABLES
            self.builder = b

            for q in GENERIC.phrases:
                self.parse_phrase (q.children[0])

            self.builder = self.old_builder
            VARIABLES = old_VARIABLES
            TYPES = old_TYPES

            return fncall(self.builder, self.module, f, [self.parse_phrase(x) for x in args_]).eval()
        elif i.data == 'sizeof':
            ty = self.parse_type (i.children[0])

            return self.get_size(ty)
        elif i.data == 'typeof':
            ty = self.parse_phrase (i.children[0])
            return String(self.builder, self.module, str(ty.type) + '\00').eval()
        elif i.data == 'asm':
            self.builder.asm(ir.FunctionType(ir.VoidType(), []), i.children[0], "", [], True)
        elif i.data == 'reg':
            reg = i.children[0].children[0].value
            return self.builder.load_reg(self.get_reg_type(reg),
                                         reg,
                                         name="loaded_{0}".format(reg))
        elif i.data == 'reg_set':
            reg = i.children[0].children[0].children[0].value
            val = self.parse_phrase(i.children[1])
            self.builder.store_reg(val, self.get_reg_type(reg), reg, name="stored_{0}".format (reg))

            #
            # targ = binding.Target.from_triple(binding.get_process_triple())
            # sz = ty.get_abi_size(targ)
            # return ir.IntType(self.ia)(sz)
        elif i.data == 'hook':
            # get type
            ret_str = i.children[2].children[0].children[0].value
            ret = self.parse_type(i.children[2].children[0])
            self.ret = ret
            oiv = self.is_void
            self.is_void = (ret_str == 'void')
            args = []
            vars_ = []
            for j in i.children[1].children:
                if j.data == 'as_phrase':
                    t = j.children[0]
                    args.append (self.parse_type (t))
                else:
                    vars_.append (j.children[0].children[0].value)
            ftype = ir.FunctionType (ret, args)
            name = 'hook_' + i.children[0]
            f = ir.Function (self.module, ftype, self.header + name)
            f.attributes.add ('noinline')
            PREDEFINED_FUNCTIONS[self.header + name] = f
            block = f.append_basic_block ("entry")
            b = ir.IRBuilder (block)

            for q in range (len (f.args)):
                ptrl = b.alloca (f.args[q].type, name='$' + vars_[q])
                b.store (f.args[q], ptrl)
                VARIABLES[vars_[q]] = Var (vars_[q], ptrl.type.pointee, False, ptrl)

            old_builder = self.builder
            old_VARIABLES = VARIABLES
            if self.debug:
                old_subprogram = self.subprogram
            self.builder = b

            ty = self.builder.function.type
            while isinstance (ty, ir.PointerType):
                ty = ty.pointee
            if self.debug:
                subfntype = self.type_to_metadata (ty.return_type)
                fntype = self.module.add_debug_info ('DISubroutineType', {
                    'types': [subfntype]
                })
                self.subprogram = self.module.add_debug_info ('DISubprogram', {
                    'name': self.builder.function.name,
                    'scope': self.file_d,
                    'file': self.file_d,
                    'type': fntype,
                    'isLocal': False,
                    'isDefinition': True,
                    'scopeLine': 1,
                    'isOptimized': False,
                    'unit': self.compunit,
                }, is_distinct=True)

            for q in i.children[3:]:
                self.parse_phrase (q.children[0])

            self.builder = old_builder
            VARIABLES = old_VARIABLES
            self.is_void = oiv

            if self.debug:
                f.set_metadata ('dbg', self.subprogram)

                self.subprogram = old_subprogram

            if self.header == '':
                VARIABLES[name] = Var (name, f.type, True, f)

            return f
        elif i.data == 'extends':
            h = self.header
            self.header = i.children[0].children[0] + '_'
            for q in i.children[1:]:
                self.parse_phrase(q)
            self.header = h
        #elif i.data == 'inc':
        #    return ir.IRBuilder()

    def get_reg_type(self, reg):
        if reg[0].upper() == 'R':
            return ir.IntType(64)
        elif reg[0].upper() == 'E':
            return ir.IntType(32)
        elif reg.upper() in ['AX', 'CX', 'DX', 'BX', 'SP', 'BP', 'SI', 'DI']:
            return ir.IntType(16)
        elif reg.upper() in ['AH', 'AL', 'CH', 'CL', 'DH', 'DL', 'BH', 'BL']:
            return ir.IntType(8)

        else:  # ??
            return TYPES['rune']

    def build(self):
        for i in self.phrases:
            self.parse_phrase(i)

        if self.debug:
            return i.meta.line, self.subprogram  # Might need to fix
        else:
            return


class Class:
    def __init__(self, name, types, inheritance, sealed, static, vtable, module):
        self.name = name
        self.types = collections.OrderedDict(types)
        self.inheritance = inheritance
        self.sealed = sealed
        self.static = static or sealed
        self.vtable = vtable
        self.functions = []
        self.module = module

    def __repr__(self):
        return str(self)

    def __str__(self):
        sealedQ = "Sealed " if self.sealed else ""
        staticQ = "Static " if self.static else ""
        return "%s%sClass %s<types %s> inheriting %s" % \
               (sealedQ, staticQ, self.name, self.types.values(), self.inheritance)

    def publish_function(self, q):
        self.functions.append(q)

    def publish_vtable(self):
        global VTABLES

        vtable_type = self.module.context.get_identified_type(self.name + '_vtable')
        vtable_initializer = ir.Constant(vtable_type, self.functions)

        global_vtable = ir.GlobalVariable(self.module, vtable_type, name="%s_vtable_value" % self.name)
        global_vtable.linkage = 'internal'
        global_vtable.global_constant = True
        global_vtable.initializer = vtable_initializer

        VTABLES[self.name] = global_vtable

    def can_be_inherited(self):
        return not self.static

    def get_virt_fns(self):
        return self.module.context.get_identified_type(self.name + '_vtable').elements


class Interface(Class):
    def __init__(self, name, functions, mod):
        super().__init__(name, [], [], False, True, functions, mod)
        self.functions = functions

    def can_be_inherited(self):
        return True

    def __str__(self):
        return "Interface %s (functions %s)" % (self.name, self.functions)


class InterfaceFn:
    def __init__(self, name, inp, out):
        self.name = name
        self.inp = inp
        self.out = out

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return "Interface fn (name %s, in %s, out %s)" % (self.name, self.inp, self.out)

    def get_fntype(self):
        return ir.FunctionType(self.out, self.inp).as_pointer()


class Generic:
    def __init__(self, classes, name, params, ret, phrases):
        self.classes = classes
        self.name = name
        self.params = params
        self.ret = ret
        self.phrases = phrases


class Var:
    def __init__(self, name, type_, lockedQ, irVar):
        self.name = name
        self.type = type_
        self.lockedQ = lockedQ
        self.irVar = irVar

    def __repr__(self):
        return str(self)

    def __str__(self):
        l = "Locked " if self.lockedQ else ""
        return "%sVariable %s of type %s, representing %s" % (l, self.name, self.type, self.irVar)


class PhraseList(BasicAst):
    def __init__(self, module, builder: ir.IRBuilder):
        super().__init__(builder, module)
