import argparse
import json
import os

parser = argparse.ArgumentParser(description='Runs Xanathar module editor.')
parser.add_argument('-i', dest='interactive', action='store_const',
                    const=False, default=True,
                    help='makes the editor non-interactive')
parser.add_argument('-n', nargs='?',
                    help='if using non-interactive mode, the name of the module to add to the module registry')
parser.add_argument('-l', nargs='?',
                    help='if using non-interactive mode, the path of the module to add to the module registry')
parser.add_argument('-a', nargs='?',
                    help='if using non-interactive mode, what action to perform ("1" = add, "2" = remove)')
parser.add_argument('-d', nargs='?', help='which directory to use for finding the module registry')
args = parser.parse_args()

REG_DIR = args.d if args.d else os.path.join(os.path.dirname(__file__), 'stdlib')
REG_FILE = os.path.join(REG_DIR, 'module_registry')

print("Registry: " + REG_FILE)


def add_module(name, mod, reg):
    with open(reg) as reg_line:
        reg_text = json.loads(reg_line.read())
    with open(reg, 'w') as reg_line:
        reg_text[name] = mod
        reg_line.write(json.dumps(reg_text))


def remove_module(name, reg):
    with open(reg) as reg_line:
        reg_text = json.loads(reg_line.read())
    with open(reg, 'w') as reg_line:
        del reg_text[name]
        reg_line.write(json.dumps(reg_text))


# https://stackoverflow.com/questions/19125237/a-text-table-writer-printer-for-python
def printTable (tbl, borderHorizontal = '-', borderVertical = '|', borderCross = '+'):
    cols = [list(x) for x in zip(*tbl)]
    lengths = [max(map(len, map(str, col))) for col in cols]
    f = borderVertical + borderVertical.join(' {:>%d} ' % l for l in lengths) + borderVertical
    s = borderCross + borderCross.join(borderHorizontal * (l+2) for l in lengths) + borderCross

    print(s)
    for row in tbl:
        print(f.format(*row))
        print(s)


def list_modules(reg):
    with open(reg) as reg_line:
        reg_text = json.loads(reg_line.read())
        t = [["Name", "Path"]]
        for i in reg_text:
            t.append([i, reg_text[i]])
        print(printTable(t))


if args.a and (args.a == "1" and args.l and args.n) or (args.a == "2" and args.n):
    if args.a == "1":
        add_module(args.n, args.l, REG_FILE)
    elif args.a == "2":
        remove_module(args.n, REG_FILE)
    exit(0)
else:
    while 1:
        print("1) Add a module")
        print("2) Remove a module")
        print("3) List modules")
        print("4) Exit")
        selection = input("> ").strip().rstrip()

        if selection == "1":
            add_module(input("Module name: "), input("Module path: "), REG_FILE)
        elif selection == "2":
            remove_module(input("Module name: "), REG_FILE)
        elif selection == "3":
            list_modules(REG_FILE)
        elif selection == "4":
            exit(0)
        else:
            print("Invalid selection.")
