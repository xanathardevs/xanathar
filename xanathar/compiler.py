from xanathar import codegen, interpreter

import os


def xcompile(parsed, name, static_int_alloc, so, flags, file, debug):
    cg = codegen.CodeGen(name)
    xp = interpreter.XanatharInterpreter(cg.module, cg.builder, cg.main, cg.printf, cg.argc, cg.argv, static_int_alloc, flags, file, debug)
    if debug:
        ll, m = xp.visit(parsed)
        cg.create_ir (xp.builder, ll, m)
    else:
        xp.visit (parsed)
        cg.create_ir (xp.builder)
    cg.save_ir(name + '.ll')

    xp.build(name, so)
