# How to create your own module

### Step 1: write some code

Any code that can be compiled to a .so/.dll works (e.g. C, C++, at some point Xanathar).

### Step 2: write your module definition

As of right now, there are 2 types of modules:

#### PURE_LIBC

Example: `{"type": "PURE_LIBC", "FUNCTIONS": {"srand": ["void", ["int32"]], "rand": ["int32", []]}}`

#### LINKED_SO

Example: `{"type": "LINKED_SO", "FUNCTIONS": {"nop": ["void", []], "range": ["int32*", ["int32", "int32"]], "cmp": ["int32", ["int32", "int32"]], "inc": ["void", ["int32*"]], "dec": ["void", ["int32*"]]}, "FILE": "stdlib/stdlib.so"}`

Both of these module types are represented as a JSON string. 

#### key: type

The type of the module

#### key: FUNCTIONS

A mapping of functions and their types

#### key: FILE

In LINKED_SO modules, the path to the .so file.

### Step 3: register it

Add it to the registry by running `python3 /path/to/xanathar/installation/module-edit.py`
 and following the instructions.
 
### Step 4: use it

Now, you can import the module in any Xanathar program with `use <name>`. 

Soon, there will be a package manager to make this easier.