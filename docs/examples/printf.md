```xanathar
declare $a as int8;
declare $x as int8;

$a = [2 as int8] >> [1 as int8];
print_i[$a];
print_s["a" >> 0];

declare $test as *int8;
$test = "%f" >> 0;
print_s[$test];

printf[$test, 256];
printf["
%p" >> 0, $test];
```

LLVM:

```llvm
; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca i8
  %".7" = alloca i8
  %".8" = trunc i32 2 to i8
  %".9" = trunc i32 1 to i8
  %".10" = ashr i8 %".8", %".9"
  store i8 %".10", i8* %".6"
  %".12" = load i8, i8* %".6"
  %".13" = call i32 (i8*, ...) @"printf"(i8* %".2", i8 %".12")
  %".14" = alloca [2 x i8]
  store [2 x i8] [i8 97, i8 0], [2 x i8]* %".14"
  %".16" = getelementptr [2 x i8], [2 x i8]* %".14", i32 0
  %".17" = getelementptr [2 x i8], [2 x i8]* %".16", i32 0
  %".18" = bitcast [2 x i8]* %".17" to i8*
  %".19" = call i32 (i8*, ...) @"printf"(i8* %".4", i8* %".18")
  %".20" = alloca i8*
  %".21" = alloca [3 x i8]
  store [3 x i8] [i8 37, i8 102, i8 0], [3 x i8]* %".21"
  %".23" = getelementptr [3 x i8], [3 x i8]* %".21", i32 0
  %".24" = getelementptr [3 x i8], [3 x i8]* %".23", i32 0
  %".25" = bitcast [3 x i8]* %".24" to i8*
  store i8* %".25", i8** %".20"
  %".27" = load i8*, i8** %".20"
  %".28" = call i32 (i8*, ...) @"printf"(i8* %".4", i8* %".27")
  %".29" = load i8*, i8** %".20"
  %".30" = call i32 (i8*, ...) @"printf"(i8* %".29", i32 256)
  %".31" = alloca [4 x i8]
  store [4 x i8] [i8 10, i8 37, i8 112, i8 0], [4 x i8]* %".31"
  %".33" = getelementptr [4 x i8], [4 x i8]* %".31", i32 0
  %".34" = getelementptr [4 x i8], [4 x i8]* %".33", i32 0
  %".35" = bitcast [4 x i8]* %".34" to i8*
  %".36" = load i8*, i8** %".20"
  %".37" = call i32 (i8*, ...) @"printf"(i8* %".35", i8* %".36")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
```