```xanathar
use stdio;

declare $x as *int8;
declare $y as int8;
$y = [3 as int8];
$x = @y;
get[$x, 20];

declare $z as int32;
$z = atoi[@y];
print_i[$z + 1];
```

LLVM:

```llvm
; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

%"FILE" = type opaque
define void @"main"() 
{
entry:
  %".2" = bitcast [2 x i8]* @"fdopen_fmt" to i8*
  %".3" = call %"FILE"* @"fdopen"(i32 0, i8* %".2")
  %".4" = bitcast [5 x i8]* @"istr" to i8*
  %".5" = bitcast [7 x i8]* @"xstr" to i8*
  %".6" = bitcast [5 x i8]* @"sstr" to i8*
  %".7" = bitcast [5 x i8]* @"cstr" to i8*
  %".8" = alloca i8*
  %".9" = alloca i8
  %".10" = trunc i32 3 to i8
  store i8 %".10", i8* %".9"
  store i8* %".9", i8** %".8"
  %".13" = load i8*, i8** %".8"
  %".14" = call i8* @"get"(i8* %".13", i32 20)
  %".15" = alloca i32
  %".16" = call i32 @"atoi"(i8* %".9")
  store i32 %".16", i32* %".15"
  %".18" = load i32, i32* %".15"
  %".19" = add i32 %".18", 1
  %".20" = call i32 (i8*, ...) @"printf"(i8* %".4", i32 %".19")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

declare i8* @"fgets"(i8* %".1", i32 %".2", %"FILE"* %".3") 

declare %"FILE"* @"fdopen"(i32 %".1", i8* %".2") 

@"fdopen_fmt" = internal constant [2 x i8] c"r\00"
@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

define void @"a"() 
{
entry:
  ret void
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }

```