```xanathar
print_s["Hello, world!"];
```
LLVM:
```llvm
; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca [14 x i8]
  store [14 x i8] [i8 72, i8 101, i8 108, i8 108, i8 111, i8 44, i8 32, i8 119, i8 111, i8 114, i8 108, i8 100, i8 33, i8 0], [14 x i8]* %".6"
  %".8" = getelementptr [14 x i8], [14 x i8]* %".6", i32 0
  %".9" = call i32 (i8*, ...) @"printf"(i8* %".4", [14 x i8]* %".8")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
```