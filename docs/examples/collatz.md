```xanathar
use stdio;

declare $x as int32;
declare $y as int64[64];

fn a[$c as int32] as int32 {
    if [$c == 1] {
        ret 0;
    } else {
        if [$c % 2 == 0] {
            ret 1 + a[$c / 2];
        }
    }

    ret 1 + a[3 * $c + 1];
}

declare $i as int32;

for [$i = 1; $i <= 10; $i = $i + 1;] {
    printf["$i is %i, " >> 0, $i];
    printf["collatz gives %i\n" >> 0, a[$i]];
}

printf["collatz of 77,031 is %i\n" >> 0, a[77031]];
```
    
Generates this LLVM:
```llvm
; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = alloca i32
  %".7" = alloca i64
  %".8" = alloca i32
  store i32 1, i32* %".8"
  br label %"forloop-0"
forloop-0:
  %".11" = alloca [11 x i8]
  store [11 x i8] [i8 36, i8 105, i8 32, i8 105, i8 115, i8 32, i8 37, i8 105, i8 44, i8 32, i8 0], [11 x i8]* %".11"
  %".13" = getelementptr [11 x i8], [11 x i8]* %".11", i32 0
  %".14" = getelementptr [11 x i8], [11 x i8]* %".13", i32 0
  %".15" = bitcast [11 x i8]* %".14" to i8*
  %".16" = load i32, i32* %".8"
  %".17" = call i32 (i8*, ...) @"printf"(i8* %".15", i32 %".16")
  %".18" = alloca [18 x i8]
  store [18 x i8] [i8 99, i8 111, i8 108, i8 108, i8 97, i8 116, i8 122, i8 32, i8 103, i8 105, i8 118, i8 101, i8 115, i8 32, i8 37, i8 105, i8 10, i8 0], [18 x i8]* %".18"
  %".20" = getelementptr [18 x i8], [18 x i8]* %".18", i32 0
  %".21" = getelementptr [18 x i8], [18 x i8]* %".20", i32 0
  %".22" = bitcast [18 x i8]* %".21" to i8*
  %".23" = load i32, i32* %".8"
  %".24" = call i32 @"a"(i32 %".23")
  %".25" = call i32 (i8*, ...) @"printf"(i8* %".22", i32 %".24")
  %".26" = load i32, i32* %".8"
  %".27" = add i32 %".26", 1
  store i32 %".27", i32* %".8"
  %".29" = load i32, i32* %".8"
  %".30" = icmp sle i32 %".29", 10
  br i1 %".30", label %"forloop-0", label %"endfor-0"
endfor-0:
  %".32" = alloca [25 x i8]
  store [25 x i8] [i8 99, i8 111, i8 108, i8 108, i8 97, i8 116, i8 122, i8 32, i8 111, i8 102, i8 32, i8 55, i8 55, i8 44, i8 48, i8 51, i8 49, i8 32, i8 105, i8 115, i8 32, i8 37, i8 105, i8 10, i8 0], [25 x i8]* %".32"
  %".34" = getelementptr [25 x i8], [25 x i8]* %".32", i32 0
  %".35" = getelementptr [25 x i8], [25 x i8]* %".34", i32 0
  %".36" = bitcast [25 x i8]* %".35" to i8*
  %".37" = call i32 @"a"(i32 77031)
  %".38" = call i32 (i8*, ...) @"printf"(i8* %".36", i32 %".37")
  ret void
}

declare i32 @"printf"(i8* %".1", ...) 

@"fdopen_fmt" = internal constant [2 x i8] c"r\00"
@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

define i32 @"a"(i32 %".1") 
{
entry:
  %".3" = alloca i32
  store i32 %".1", i32* %".3"
  %".5" = load i32, i32* %".3"
  %".6" = icmp eq i32 %".5", 1
  br i1 %".6", label %"entry.if", label %"entry.else"
entry.if:
  ret i32 0
entry.else:
  %".9" = load i32, i32* %".3"
  %".10" = srem i32 %".9", 2
  %".11" = icmp eq i32 %".10", 0
  br i1 %".11", label %"entry.else.if", label %"entry.else.else"
entry.endif:
  %".20" = load i32, i32* %".3"
  %".21" = mul i32 3, %".20"
  %".22" = add i32 %".21", 1
  %".23" = call i32 @"a"(i32 %".22")
  %".24" = add i32 1, %".23"
  ret i32 %".24"
entry.else.if:
  %".13" = load i32, i32* %".3"
  %".14" = sdiv i32 %".13", 2
  %".15" = call i32 @"a"(i32 %".14")
  %".16" = add i32 1, %".15"
  ret i32 %".16"
entry.else.else:
  br label %"entry.else.endif"
entry.else.endif:
  br label %"entry.endif"
}

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }
```
