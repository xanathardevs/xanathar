```xanathar
use stdlib;
use stdio;
use random;
use time;

srand[time[0]];

declare $secret as int32;
$secret = rand[] % 100;
printf["I'm thinking of a number from 0 to 99!\nTry to guess it!\n" >> 0];

declare $x as *int8;
declare $y as int8;
declare $z as int32;
$y = [3 as int8];
$x = @y;

declare $guesses as int32;
$guesses = 0;


while [$z != $secret] {
    get[@y, 20];

    $z = atoi[@y];

    match [cmp[$z, $secret]] {
        0 => print_s["Correct!"],
        1 => print_s["Too high!"],
        -1 => print_s["Too low!"],
        default => print_s["Error!"];
    }

    $guesses = $guesses + 1;
}

printf["You won in %i guesses!\n" >> 0, $guesses];
```

LLVM:

```llvm
; ModuleID = "/home/proc-daemon/Dropbox/Xanathar/xanathar/codegen.py"
target triple = "x86_64-unknown-linux-gnu"
target datalayout = ""

define void @"main"() 
{
entry:
  %".2" = bitcast [5 x i8]* @"istr" to i8*
  %".3" = bitcast [7 x i8]* @"xstr" to i8*
  %".4" = bitcast [5 x i8]* @"sstr" to i8*
  %".5" = bitcast [5 x i8]* @"cstr" to i8*
  %".6" = call i32 @"time"(i32 0)
  call void @"srand"(i32 %".6")
  %".8" = alloca i32
  %".9" = call i32 @"rand"()
  %".10" = srem i32 %".9", 100
  store i32 %".10", i32* %".8"
  %".12" = alloca [57 x i8]
  store [57 x i8] [i8 73, i8 39, i8 109, i8 32, i8 116, i8 104, i8 105, i8 110, i8 107, i8 105, i8 110, i8 103, i8 32, i8 111, i8 102, i8 32, i8 97, i8 32, i8 110, i8 117, i8 109, i8 98, i8 101, i8 114, i8 32, i8 102, i8 114, i8 111, i8 109, i8 32, i8 48, i8 32, i8 116, i8 111, i8 32, i8 57, i8 57, i8 33, i8 10, i8 84, i8 114, i8 121, i8 32, i8 116, i8 111, i8 32, i8 103, i8 117, i8 101, i8 115, i8 115, i8 32, i8 105, i8 116, i8 33, i8 10, i8 0], [57 x i8]* %".12"
  %".14" = getelementptr [57 x i8], [57 x i8]* %".12", i32 0
  %".15" = getelementptr [57 x i8], [57 x i8]* %".14", i32 0
  %".16" = bitcast [57 x i8]* %".15" to i8*
  %".17" = call i32 (i8*, ...) @"printf"(i8* %".16")
  %".18" = alloca i8*
  %".19" = alloca i8
  %".20" = alloca i32
  %".21" = trunc i32 3 to i8
  store i8 %".21", i8* %".19"
  store i8* %".19", i8** %".18"
  %".24" = alloca i32
  store i32 0, i32* %".24"
  br label %"entry.whileloop0"
entry.whileloop0:
  %".27" = call i8* @"get"(i8* %".19", i32 20)
  %".28" = call i32 @"atoi"(i8* %".19")
  store i32 %".28", i32* %".20"
  %".30" = load i32, i32* %".20"
  %".31" = load i32, i32* %".8"
  %".32" = call i32 @"cmp"(i32 %".30", i32 %".31")
  switch i32 %".32", label %"default-match-0" [i32 0, label %"switchstmt-0-0" i32 1, label %"switchstmt-0-1" i32 -1, label %"switchstmt-0-2"]
entry.endwhile0:
  %".57" = alloca [24 x i8]
  store [24 x i8] [i8 89, i8 111, i8 117, i8 32, i8 119, i8 111, i8 110, i8 32, i8 105, i8 110, i8 32, i8 37, i8 105, i8 32, i8 103, i8 117, i8 101, i8 115, i8 115, i8 101, i8 115, i8 33, i8 10, i8 0], [24 x i8]* %".57"
  %".59" = getelementptr [24 x i8], [24 x i8]* %".57", i32 0
  %".60" = getelementptr [24 x i8], [24 x i8]* %".59", i32 0
  %".61" = bitcast [24 x i8]* %".60" to i8*
  %".62" = load i32, i32* %".24"
  %".63" = call i32 (i8*, ...) @"printf"(i8* %".61", i32 %".62")
  ret void
default-match-0:
  br label %"end-match-0"
end-match-0:
  %".50" = load i32, i32* %".24"
  %".51" = add i32 %".50", 1
  store i32 %".51", i32* %".24"
  %".53" = load i32, i32* %".20"
  %".54" = load i32, i32* %".8"
  %".55" = icmp ne i32 %".53", %".54"
  br i1 %".55", label %"entry.whileloop0", label %"entry.endwhile0"
switchstmt-0-0:
  %".35" = alloca [9 x i8]
  store [9 x i8] [i8 67, i8 111, i8 114, i8 114, i8 101, i8 99, i8 116, i8 33, i8 0], [9 x i8]* %".35"
  %".37" = getelementptr [9 x i8], [9 x i8]* %".35", i32 0
  %".38" = call i32 (i8*, ...) @"printf"(i8* %".4", [9 x i8]* %".37")
  br label %"end-match-0"
switchstmt-0-1:
  %".40" = alloca [10 x i8]
  store [10 x i8] [i8 84, i8 111, i8 111, i8 32, i8 104, i8 105, i8 103, i8 104, i8 33, i8 0], [10 x i8]* %".40"
  %".42" = getelementptr [10 x i8], [10 x i8]* %".40", i32 0
  %".43" = call i32 (i8*, ...) @"printf"(i8* %".4", [10 x i8]* %".42")
  br label %"end-match-0"
switchstmt-0-2:
  %".45" = alloca [9 x i8]
  store [9 x i8] [i8 84, i8 111, i8 111, i8 32, i8 108, i8 111, i8 119, i8 33, i8 0], [9 x i8]* %".45"
  %".47" = getelementptr [9 x i8], [9 x i8]* %".45", i32 0
  %".48" = call i32 (i8*, ...) @"printf"(i8* %".4", [9 x i8]* %".47")
  br label %"end-match-0"
}

declare i32 @"printf"(i8* %".1", ...) 

@"fdopen_fmt" = internal constant [2 x i8] c"r\00"
@"istr" = internal constant [5 x i8] c"%i \0a\00"
@"xstr" = internal constant [7 x i8] c"0x%x \0a\00"
@"sstr" = internal constant [5 x i8] c"%s \0a\00"
@"cstr" = internal constant [5 x i8] c"%c \0a\00"
declare void @"nop"() 

declare i32* @"range"(i32 %".1", i32 %".2") 

declare i32 @"cmp"(i32 %".1", i32 %".2") 

declare void @"inc"(i32* %".1") 

declare void @"dec"(i32* %".1") 

declare i8* @"get"(i8* %".1", i32 %".2") 

declare i32 @"atoi"(i8* %".1") 

declare void @"srand"(i32 %".1") 

declare i32 @"rand"() 

declare i32 @"time"(i32 %".1") 

!gen-by = !{ !0 }
!0 = !{ !"Xanathar" }
```